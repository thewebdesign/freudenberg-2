﻿<!DOCTYPE html>
<html class="no-js">
    <?php include 'includes/header.php'; ?> 

    <body >

        <header id="header" role="banner">
            <?php include 'includes/navigation.php'; ?> 

            <style>
                .ctatext-text{margin-top:31%;}
                @media screen and (max-width:768px){
                    .ctatext-text{margin-top:42%;}
                }
                @media screen and (max-width:420px){
                    .ctatext-text{margin-top:115%;}
                }
            </style>

        </header><!--  #header  -->
        <?php include 'includes/booking.php'; ?> 


        <div class="blur">  
            <div class="node--page_basic mode--full">    
                <aside role="complementary">
                    <?php include 'includes/slider.php'; ?>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>MICE and Travel</li>
                    </breadcrumb>
                </div>      

                <div id="main" role="main">
                    <div class="wrapper">
                        <article role="article" style="background-image:url(assets/images/travel.png); background-repeat: no-repeat;">
                            <div class="ctatext-wrapper">
                                <div class="ctatext-text">
                                    <div class="hdr-two">MICE and Travel</div>     
                                    <p style="text-align:justify; font-size:16px;">The Meetings, Incentives, Conventions and Exhibitions tourism (MICE) is a business-oriented segment, involving non-discretionary travel. The Incentives part of MICE is the odd one out – though it is related to business, as it is usually provided to employees or dealers/distributors as a reward, it tends to be leisure based.

                                        <br><br>Sri Lanka’s strategic location, excellent connectivity and cultural heritage make it an ideal MICE destination and we deliver prompt and within budget services. Reputed to be the Perl in the Indian Ocean with bright sunshine and tropical climate, the island nation is ideal to visit at any time of the year.

                                        <br><br>Our first class accommodations, meeting facilities, cultural attractions, pristine beaches, ancient cities, mountains, and wild life etc offer limitless possibilities. Come stage a truly unforgettable experience for any business meetings and incentive programmes.

                                        <br><br>Fredudenberg Leisure with three way outs in Colombo, Kandy and Nuwara Eliya will ensure well executed events are professionally delivered according to your needs together with a personal touch.

                                        <br><br>We provides special customized convention and   travel packages for the providing of an unique all-in-one meeting solution tailored to your specific needs, incentive travel programs that enhance your team’s motivation and  productivity while fulfilling your business objectives.

                                        this island is now a popular destination for new attractions viz whale watching, bird watching, surf championships, elephant gathering, underwater activities, leopard safaris etc. Incentive travelers have increased over the last few years with industry offering many creative programs to offer excitement to participants to have memorable experiences when they return.</p>            
                                </div><!--  .ctatext-text  -->          
                            </div><!--  .ctatext-wrapper  -->

                        </article>
                    </div><!--  .wrapper  -->
                </div><!--  #main  -->


            </div><!--  #node-details  -->

            <div style="clear:both"></div>.


            <footer id="footer" role="contentinfo">    

                <?php /* ?> <aside role="complementary">
                  <div class="ctatext-buildadventure ctatext-wrapper">
                  <div class="ctatext-text">
                  <h1 class="hdr-seven">Build your own Adventure</h1>
                  <div class="hdr-two">Your Perfect Getaway Awaits</div>
                  <p>Whatever your lifestyle or pace, Freudenberg Leisure offers something unique for everyone.</p>
                  <a class="btn-arrow" href="#">Book Now</a>
                  </div><!--  .ctatext-text  -->
                  </div><!--  .ctatext-wrapper  -->
                  </aside><?php */ ?>


                <?php include 'includes/footer.php'; ?>

                </body>
                </html>