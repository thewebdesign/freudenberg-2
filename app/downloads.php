﻿<?php 
$pg = ['property' => 'freudenberg', 'page' => 'downloads'];
include 'includes/header.php'; 
?>
    <style>
        .hdr-seven{text-align:left;color: #484848;}
        .ctatext-wrapper{padding-top:0px !important;}
    </style>

    <body class="node-type-accommodation-list">
        <header id="header" role="banner"> 
            <?php include 'includes/navigation.php'; ?> 
        </header><!--  #header  -->

        <?php include 'includes/booking.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">
                    <?php include 'includes/slider.php'; ?>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Downloads</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text pad_top">         
                                    <?php require 'includes/showdescription.php'; ?>

                                    <?php require 'includes/getdownloaditems.php'; ?>

                                    <br>

                                </div>
                        <!--			<p style="text-align:left; font-size:16px;">* Note : We will be retaining your email address for future followup.</p>
                                -->          </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->


            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include 'includes/footer.php'; ?> 
            </footer>    
    </body>
</html>
