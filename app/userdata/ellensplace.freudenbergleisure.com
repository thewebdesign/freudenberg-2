--- 
customlog: 
  - 
    format: combined
    target: /etc/apache2/logs/domlogs/ellensplace.freudenbergleisure.com
documentroot: /home2/fyfxamvcmvss/public_html/hotels/ellens
group: fyfxamvcmvss
hascgi: 1
homedir: /home2/fyfxamvcmvss
ifmodulealiasmodule: 
  scriptalias: 
    - 
      path: /home2/fyfxamvcmvss/public_html/hotels/ellens/cgi-bin/
      url: /cgi-bin/
ifmoduleconcurrentphpc: {}

ifmoduleincludemodule: 
  directoryhomefyfxamvcmvsspublichtmlhotelsellens: 
    ssilegacyexprparser: 
      - 
        value: " On"
ifmodulelogconfigmodule: 
  ifmodulelogiomodule: {}

ifmodulemodincludec: 
  directoryhomefyfxamvcmvsspublichtmlhotelsellens: 
    ssilegacyexprparser: 
      - 
        value: " On"
ifmodulemodsuphpc: 
  group: fyfxamvcmvss
ifmoduleuserdirmodule: 
  ifmodulempmitkc: 
    ifmoduleruidmodule: {}

ip: 128.199.220.102
ipv6: ~
is_addon: 1
no_cache_update: 1
owner: root
phpopenbasedirprotect: 1
port: 82
scriptalias: 
  - 
    path: /home/fyfxamvcmvss/public_html/hotels/ellens/cgi-bin/
    url: /cgi-bin/
serveradmin: webmaster@ellensplace.freudenbergleisure.com
serveralias: ellens-place.com ellensplace.lk mail.ellens-place.com mail.ellensplace.lk www.ellens-place.com www.ellensplace.freudenbergleisure.com www.ellensplace.lk
servername: ellensplace.freudenbergleisure.com
ssl: 1
usecanonicalname: 'Off'
user: fyfxamvcmvss
userdirprotect: ''
