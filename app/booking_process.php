<?php
require_once('top.php');
?>
<html>
    <head>
        <title>Processing..</title>
        <script language="javascript">
            function onLoadSubmit() {
	            document._resBBBox.submit();
            }
        </script>
    </head>
    <body onload="onLoadSubmit();">
<?php
if (isset($_POST['submit_fsaleisure']) && $_POST['site'] === 'fsaleisure') {
    
    if($_POST['hotels'] && $_POST['hotels'] == 'randholee')
    {
        ?>
        <form name="_resBBBox" action="https://live.ipms247.com/booking/book-rooms-197353" method="POST" target="_self">
            <input name="eZ_chkin" value="<?=$_POST['eZ_chkin']?>" type="text" style="visibility: hidden;">
            <input name="eZ_chkout" value="<?=$_POST['eZ_chkout']?>" type="text" style="visibility: hidden;">
            <input name="eZ_Nights" value="2" type="hidden">
            <input name="eZ_adult" value="1" type="hidden">
            <input name="eZ_child" value="0" type="hidden">
            <input name="eZ_room" value="1" type="hidden">
            <input name="hidBodyLanguage" value="en" type="hidden">
            <input name="calformat" value="yy-mm-dd" type="hidden">
            <input name="ArDt" value="<?=date('Y-m-d')?>" type="hidden">
            <input name="acturl" value="https://live.ipms247.com/booking/book-rooms-197353" type="hidden">            
        </form>
        <?php
    }
    
    if($_POST['hotels'] && $_POST['hotels'] == 'ellens')
    {
        ?>
        <form name="_resBBBox" action="https://live.ipms247.com/booking/book-rooms-018704" method="POST" target="_self">
            <input name="eZ_chkin" value="<?=$_POST['eZ_chkin']?>" type="text" style="visibility: hidden;">
            <input name="eZ_chkout" value="<?=$_POST['eZ_chkout']?>" type="text" style="visibility: hidden;">
            <input name="eZ_Nights" value="2" type="hidden">
            <input name="eZ_adult" value="1" type="hidden">
            <input name="eZ_child" value="0" type="hidden">
            <input name="eZ_room" value="1" type="hidden">
            <input name="hidBodyLanguage" value="en" type="hidden">
            <input name="calformat" value="yy-mm-dd" type="hidden">
            <input name="ArDt" value="<?=date('Y-m-d')?>" type="hidden">
            <input name="acturl" value="https://live.ipms247.com/booking/book-rooms-018704" type="hidden">
        </form>
        <?php
    }    
 
     if($_POST['hotels'] && $_POST['hotels'] == 'firs')
    {
        ?>
        <form name="_resBBBox" action="https://live.ipms247.com/booking/book-rooms-thefirs" method="POST" target="_self">
            <input name="eZ_chkin" value="<?=$_POST['eZ_chkin']?>" type="text" style="visibility: hidden;">
            <input name="eZ_chkout" value="<?=$_POST['eZ_chkout']?>" type="text" style="visibility: hidden;">
            <input name="eZ_Nights" value="2" type="hidden">
            <input name="eZ_adult" value="1" type="hidden">
            <input name="eZ_child" value="0" type="hidden">
            <input name="eZ_room" value="1" type="hidden">
            <input name="promotioncode" value="" type="hidden">
            <input name="hidBodyLanguage" value="en" type="hidden">
            <input name="calformat" value="yy-mm-dd" type="hidden">
            <input name="ArDt" value="<?=date('Y-m-d')?>" type="hidden">
            <input name="acturl" value="https://live.ipms247.com/booking/book-rooms-thefirs" type="hidden">
        </form>
        <?php
    }    
    
}
?>
    </body>
</html>