<?php

require_once('PHPMailer/class.phpmailer.php');

/**
 * @author Mifas
 */
class mailing {

    private static $username = '';
    private static $password = '';
    private static $host = 'localhost';
    private static $enable_debug = false;
    private static $use_smpt = false;
    private static $reply = 'reply@example.com';
    private static $from = 'from@example.com';

    private static function create_object() {
        if (self::$enable_debug) {
            $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
            $mail->SMTPDebug = 2;                     // debugging: 1 = errors and messages, 2 = messages only
        } else {
            $mail = new PHPMailer(false); // the true param means it will throw exceptions on errors, which we need to catch
            $mail->SMTPDebug = false;                    // debugging: 1 = errors and messages, 2 = messages only
        }

        if (self::$use_smpt) {
            $mail->IsSMTP(); // telling the class to use SMTP

            $mail->SMTPAuth = false;                  // enable SMTP authentication
            // $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
            $mail->Host = self::$host;      // sets as the SMTP server
            $mail->Port = 5555;                   // set the SMTP port for the server

            $mail->Username = self::$username;
            $mail->Password = self::$password;
        } else {
            $mail->IsMAIL(); //Sets Mailer to send message using PHP mail() function.
        }

        return $mail;
    }

    static function html_mail($to, $subject, $body, $from, $reply, $attachments = array()) {

        $to = trim($to);
        if (empty($to) || is_null($to) || count($to) < 0 || !filter_var($to, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        $message = " ";

        if (!is_null($from) && !empty($from) && filter_var($from, FILTER_VALIDATE_EMAIL)) {
            $f = $from;
        } else {
            $f = self::$from;
        }

        if (!is_null($reply) && !empty($reply) && filter_var($reply, FILTER_VALIDATE_EMAIL)) {
            $r = $reply;
        } else {
            $r = self::$reply;
        }

        if (empty($subject)) {
            $subject = ' ';
        }

        if (empty($body)) {
            $body = ' ';
        }

        $message .= $body;

        try {
            $mail = self::create_object();

            $mail->AddReplyTo($r, $r);
            $mail->SetFrom($f, $f);

            $mail->Subject = $subject;
            $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
            $mail->Body = $message;

            if (count($attachments) > 0 && isset($attachments[0]['path'])) {
                foreach ($attachments as $attachment) {
                    $attachment_name = ( isset($attachment['name']) ) ? $attachment['name'] : false;
                    $file_ext = pathinfo($_FILES['cv_file']['name'], PATHINFO_EXTENSION);

                    $mime_type = false;

                    if (self::get_mime_type($file_ext)) {
                        $mime_type = self::get_mime_type($file_ext);
                    }
                    $mail->AddAttachment($attachment['path'], $attachment_name, 'base64', $mime_type);
                }
            }

            $mail->AddAddress($to, $to);

            if ($mail->Send()) {
                $mail->ClearAllRecipients();

                return true;
            } else {
                if (self::$enable_debug)
                    echo $mail->ErrorInfo;

                return false;
            }
        } catch (phpmailerException $e) {
            if (self::$enable_debug)
                echo $e->errorMessage(); //Pretty error messages from PHPMailer

            return false;
        } catch (Exception $e) {
            if (self::$enable_debug)
                echo $e->getMessage(); //Boring error messages from anything else!

            return false;
        }
    }

    static public function get_mime_type($extension) {

        // our list of mime types
        $mime_types = array(
            "pdf" => "application/pdf"
            , "exe" => "application/octet-stream"
            , "zip" => "application/zip"
            , "docx" => "application/msword"
            , "doc" => "application/msword"
            , "xls" => "application/vnd.ms-excel"
            , "ppt" => "application/vnd.ms-powerpoint"
            , "gif" => "image/gif"
            , "png" => "image/png"
            , "jpeg" => "image/jpg"
            , "jpg" => "image/jpg"
            , "mp3" => "audio/mpeg"
            , "wav" => "audio/x-wav"
            , "mpeg" => "video/mpeg"
            , "mpg" => "video/mpeg"
            , "mpe" => "video/mpeg"
            , "mov" => "video/quicktime"
            , "avi" => "video/x-msvideo"
            , "3gp" => "video/3gpp"
            , "css" => "text/css"
            , "jsc" => "application/javascript"
            , "js" => "application/javascript"
            , "php" => "text/html"
            , "htm" => "text/html"
            , "html" => "text/html"
            , "odf" => "application/vnd.oasis.opendocument.formula"
        );

        //$extension = strtolower(end(explode('.',$file)));

        return $mime_types[$extension];
    }

}

?>