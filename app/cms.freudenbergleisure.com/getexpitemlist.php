<?php
require '../appdata/cms/bootstrap.php';

	// get navigation item list
	//$pgid = (int)$_GET['pgid'];

	$expdata = $db->getRows('SELECT * FROM tblexperiences');

	$destinations = array(1 => 'Colombo', 2 => 'Kandy', 3 => 'Nuwara Eliya');

	//echo json_encode($navdata);


	ob_start();
	foreach($expdata as $expitem):

	?>
	<div class="expitem-wrap" style="/*clear: left;*/ padding-bottom: 20px; padding-top: 20px;">
		<div class="expitem-image" style="width: 200px; height: 200px; float: left;">
			<img src="<?=LIVE_SITE_URL.'/images/experiences/'.$expitem->image?>" style="width: 100%; /*height: 100%;*/"/>
		</div>
		<div class="expitem-details" style="float: left; margin-left: 20px;">
			<table style="width: 200px;">
				<tr>
					<td>Tagline:</td>
					<td><?=$expitem->tagline?></td>
				</tr>	
				<tr>
					<td>Description:</td>
					<td><?=$expitem->description?></td>
				</tr>	
				<tr>
					<td>Destination:</td>
					<td><?=$destinations[$expitem->destination_id]?></td>
				</tr>										
			</table>
			<p><a href="#" class="editexpitem" data-exp-item-id="<?=$expitem->id?>">Edit item</a></p>
		</div>	
		<br style="clear: left;"/>	
	</div>

	<?php
	endforeach;
	$out = ob_get_clean();

	echo $out;
	exit;
	?>