<?php
require '../appdata/cms/bootstrap.php';
header('Content-Type: application/javascript');

switch($_GET['tab'])
{
	case 'Description':
	//$descdata = $db->getRow('SELECT * FROM tbldescriptions WHERE `page_id` = ?', array($_GET['pg']));
		?>
		console.log('Description JS loaded');
		//var scriptname = 'description';

		// populate form here
		//$('#frmpgdesc input[name=subtitle]').val('<?php echo (isset($descdata->sub_title) ? $descdata->sub_title : '') ?>');
		//$('#frmpgdesc input[name=maintitle]').val('<?php echo (isset($descdata->main_title) ? $descdata->main_title : '') ?>');
		//$('#frmpgdesc textarea[name=bodytext]').val('<?php echo (isset($descdata->body_text) ? $descdata->body_text : '') ?>');
		//$('#frmpgdesc input[name=desc-id]').val('<?php echo (isset($descdata->id) ? $descdata->id : 0) ?>');
		//$('#frmpgdesc input[name=desc-page-id]').val('<?php echo (isset($descdata->page_id) ? $descdata->page_id : '') ?>');

		// remove (Required) on focus
		$('#frmpgdesc :input').on('focus', function(){
			//console.log('focused');
			$(this).siblings('label').children('.requiredmsg').css('display', 'none');
		})

		$('#frmpgdesc').on('submit', function(e){
			console.log('submit clicked');
			$('#actionfeedback').removeClass().hide();
			e.preventDefault();

			var form = $(this);

			// take contents from tinymce editor and populate textarea#bodytext
			//form.find('input[name=bodytext]').val(tinymce.get('bodytext').getContent());

			var formData = new FormData();

			var errors = false;
			var errmsg;

			var descFrmEl = [
					$('#frmpgdesc input[name=subtitle]'),
					$('#frmpgdesc input[name=maintitle]'),
					$('#frmpgdesc textarea[name=bodytext]'),
					$('#frmpgdesc input[name=desc-id]'),
					$('#frmpgdesc input[name=desc-page-id]'),
					$('#frmpgdesc input[name=submit]')
				];


			$.each(descFrmEl, function(index, element){
				//element = $(this);
				console.log('validating '+element.attr('name')+' ...');

				// skip submit

				if(element.attr('name') == 'undefined')
				{
					console.log('undefined skipped');
					return true;
				}

				if(element.attr('name') == 'submit')
				{
					// skip iteration
					return true;
				}

				if(element.attr('name') == 'subtitle')
				{
					console.log('subtitle skipped');
					formData.append(element.attr('name'), element.val());
				}
				else
				{
					if(element.val() == '')
					{
						errors = true;
						console.log(element.attr('name')+' is empty!');
						element.siblings('label').children('.requiredmsg').css('display','inline');
					}
					else
					{
						formData.append(element.attr('name'), element.val());
					}
				}
			});

			console.log(formData);

			if(errors)
			{
				console.log('Errors Found!');
				//console.log(errmsg);
				return false;
			}
			else
			{
				// no errors

					var urltopost = form.attr('action');

					console.log(urltopost);
					console.log('no errors');

					$.ajax({
						method: 'POST',
						url: urltopost,
						data: formData,
						dataType: 'json',
						processData: false,
						contentType: false,

					})
					.done(function(res){
						console.log(res); 

						$("html, body").animate({
					        scrollTop: 0
					    });

						if(res.action == 'add' && res.status == true)
						{
							form.find('input[name=desc-id]').val(res.id);
							$('#actionfeedback').html('Page Contents Inserted!').addClass('actionfeedback success').show().fadeOut(5000);
							//$('#actionfeedback').html('<div class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> Page Contents Inserted!.</div>').addClass('actionfeedback');
						}
						else if(res.action == 'update' && res.status == true)
						{
							$('#actionfeedback').html('Page Contents Updated!').addClass('actionfeedback success').show().fadeOut(5000);
							//$('#actionfeedback').html('<div class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> Page Contents Updated!.</div>').addClass('actionfeedback');
						}
						else
						{
							$('#actionfeedback').html('Operation Failed!').addClass('actionfeedback failed').show().fadeOut(5000);
							//$('#actionfeedback').html('<div class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Operation Failed!.</div>').addClass('actionfeedback');
						}

						//response = JSON.parse(res);	// when we set the expected data type to json and when we send with json_encode, no need to parse it with JSON.parse(res)

						//console.log(res.status);
						/*
						if(res.status == true)
						{
							$.get('http://freudenberg.cms/getnavitemlist.php', { pgid: res.page_id }, function(navitemlist){
								$('.nav-item-list').html(navitemlist);
								//console.log(navitemlist);
							})

							navform.trigger('reset');	// reset form

							// if action == 'update' remove navitem-id hidden element
							if(res.action == 'update')
							{
								// remove navitem-id hidden element
								navform.find('input[name=navitem-id]').remove();
							}

							$('#image-preview').html('Image not selected!');		// reset preview

							$('#myModal').modal('hide');

							//console.log('Success');
						}*/
					});				

			}


		});

		<?php
	break;

	case 'Navigation':
		?>

			console.log('Navigation JS Loaded');
			//var scriptname = 'navigation';

			/*
			var pg = '<?php echo $_GET['pg']; ?>';

			$.get('http://freudenberg.cms/getnavitemlist.php', { pgid: pg }, function(navitemlist){
				$('.nav-item-list').html(navitemlist);
				//console.log(navitemlist);
			})*/

			$('#popme').on('click', function(e){
				e.preventDefault();
				console.log('clicked');

				// reset form
				$('#frmnavigation').trigger('reset');

				getPropertyList('<?=SITE_URL?>', $('#frmnavigation select[name=navitem-target-pg-property]'), 1, 0);

				$('#frmnavigation select[name=navitem-target]').html('').append('<option value="">Select a Property First ...</option>');

				$('#frmnavigation input[name=navitem-action]').val("add");
				$('#frmnavigation input[name=navitemsave]').val('Add Item!');
				//$('#frmnavigation').append('<input type="hidden" name="navtype" value="1"/>');
				//$('#frmnavigation input[name=navitem-layout]').val("1");  -> not using
				$('#myModal').modal('show');
			});

			

			$('#frmnavigation select[name=navitem-target-pg-property]').change(function(){
			

				// populate properties select list
				var selpropval = $(this).val();
				console.log('PRID :'+selpropval);

				getPagesByProperty('<?=SITE_URL?>', selpropval, 0);

				
			})




			/* not using anymore
			$('#popme2').on('click', function(e){
				e.preventDefault();
				console.log('clicked');
				$('#frmnavigation input[name=navitem-layout]').val("2");
				$('#myModal').modal('show');
			});*/

			// remove (Required) on focus
			$('#frmnavigation :input').on('focus', function(){
				//console.log('focused');
				$(this).siblings('label').children('.requiredmsg').css('display', 'none');
			})

			$('#frmnavigation').on('submit', function(e){
				e.preventDefault();

				var navform = $(this);

				var errors = false;
				var formData = new FormData();

				var needfile = true;

				//console.log(formData);

				//formData.append('hello', 'ami');		-> test append (works)

				//console.log(formData.getAll('hello')); 	-> debug (works)

				// validate each input element
				$.each($(this).find(':input'), function(){
					//console.log($(this).attr('name'));

					element = $(this);


					if(element.val() == '')
					{
						// skip checking image file for upadtes
						if(element.attr('name') == 'navitem-image')
						{
							if(navform.find('input[name=navitem-action]').val() == 'update')
							{
								// skip iteration
								return true;
							}
						}

						errors = true;
						element.siblings('label').children('.requiredmsg').css('display','inline');
					}
					else
					{
						// append to form data. exclude navitem-image
						if(element.attr('name') != 'navitem-image')
						{
							//payload[element.attr('name')] = element.val();
							formData.append(element.attr('name'), element.val());
						}
						
					}
				})

				/* this also does what above does.
				$(this).find(':input').each(function(index, element){

					console.log(element.val());
				})*/


				/* this is not good coding. so disabled it
				var navitemname = $(this).find('input[name=navitem-name]');
				var navitemtargetpg = $(this).find('select[name=navitem-target]');
				var navitemtagline = $(this).find('input[name=navitem-tagline]');
				var navitemdesc = $(this).find('textarea[name=navitem-desc]');
				var navitemimage = $(this).find('input[name=navitem-image]');
				var navitemlayouttype = $(this).find('input[name=navitem-layout]');
				var navitempageid = $(this).find('input[name=navitem-page-id]');

				//console.log(navitemtargetpg.val());

				// validate items
				var itemstovalidate = [navitemname,navitemtargetpg,navitemtagline,navitemdesc,navitemimage];

				var errors = false;

				$.each(itemstovalidate, function(index, value){

					if(value.val() == '')
					{
						errors = true;
						value.siblings('label').children('.requiredmsg').css('display','inline');
					}
					
				});*/

				
				if(errors)
				{
					console.log('Errors found!');
					return false;
				}
				else
				{

					//var file = document.getElementById('navitem-image').files;

					// if action == 'update' && there is some file selected 
					// or if action == 'add'
					if((navform.find('input[name=navitem-action]').val() == 'update' && navform.find('input[name=navitem-image]').val() != '') || navform.find('input[name=navitem-action]').val() == 'add')
					{
						var addfile = true;
						formData.append('imageupadated', 'true');
					}

					if(addfile == true)
					{
						var fileinputelement = $(this).find('input[name=navitem-image]');

						var file = fileinputelement[0].files[0];

						console.log(file);

						// validate file type
						if(!file.type.match('image.*'))
						{
							fileinputelement.siblings('label').children('.requiredmsg').html('Please Select an Image').css('display','inline');
							return false;
						}

						console.log('Good');

						formData.append(fileinputelement.attr('name'), file, file.name);			
					}


					/* debugging - works
					for(var pair of formData.entries()) {
			   			console.log(pair[0]+ ', '+ pair[1]); 
					}*/

					//console.log(formData);

					/* this is how you append to formData object
						// Files
						formData.append(name, file, filename);

						// Blobs
						formData.append(name, blob, filename);

						// Strings
						formData.append(name, value); 

					*/

					/*
						$.post(url,data,callback,datatype);
					*/

					//console.log($(this).attr('action'));		// get form's action url

					/* this causes an error (illegal invocation when it tries to serialize data. we have to set content type)
					$.post($(this).attr('action'), formData, function(res){

							console.log(res);

					}, "json");*/

					var urltopost = $(this).attr('action');

					$.ajax({
						method: 'POST',
						url: urltopost,
						data: formData,
						contentType: false,
						dataType: 'json',
						processData: false,

					})
					.done(function(res){
						console.log(res); 
						//response = JSON.parse(res);	// when we set the expected data type to json and when we send with json_encode, no need to parse it with JSON.parse(res)

						//console.log(res.status);
						
						if(res.status == true)
						{
							$.get('<?=SITE_URL?>/getnavitemlist.php', { pgid: res.page_id }, function(navitemlist){
								$('.nav-item-list').html(navitemlist);
								//console.log(navitemlist);
							})

							navform.trigger('reset');	// reset form

							// if action == 'update' remove navitem-id hidden element
							if(res.action == 'update')
							{
								// remove navitem-id hidden element
								navform.find('input[name=navitem-id]').remove();
							}

							$('#image-preview').html('Image not selected!');		// reset preview

							$('#myModal').modal('hide');

							//console.log('Success');
						}
					});


				}

			})

			// handle edit
			$('.nav-item-list').on('click', '.editnavitem', function(e){
				e.preventDefault();
				//console.log($(this).attr('data-nav-item-id'));
				console.log('hello click');
				$('#myModal').modal('show');

				var navitemid = $(this).attr('data-nav-item-id');
				console.log(navitemid);

				$.get('<?=SITE_URL?>/getnavitemdetails.php', { navitem: navitemid }, function(navitemlist){
					//$('.nav-item-list').html(navitemlist);
					console.log(navitemlist);
					itemdata = JSON.parse(navitemlist);		// parse json_encoded response from php. this is required as we didn't tell $.get that we expect to get json
					// {"id":"28","page_id":"20","title":"fdsa","tagline":"fdsffds","description":"fdsa","target_pg":"1","image":"d1d3c92262fc555b604c7ac70b5e0d2f_555007896.png"}

					console.log(itemdata.title);

					// set form data
					$('#frmnavigation input[name=navitem-name]').val(itemdata.title);
					//$('#frmnavigation input[name=navitem-tagline]').val(itemdata.tagline);
					$('#frmnavigation textarea[name=navitem-desc]').val(itemdata.description);
					$('#frmnavigation input[name=navitemsave]').val('Update Item!');
					$('#nav-image-preview').html('<img src="<?=LIVE_SITE_URL?>/images/navigation/'+itemdata.page_id+'/'+itemdata.image+'" style="max-width: 100%; max-height: 100%;">');
					
					getPropertyList('<?=SITE_URL?>', $('#frmnavigation select[name=navitem-target-pg-property]'), 1, itemdata.property_id);  	// this populates the property list 

					getPagesByProperty('<?=SITE_URL?>', itemdata.property_id, itemdata.target_pg);

					//$('#frmnavigation select[name=navitem-target]').val(itemdata.target_pg).change();

					// set the action
					$('#frmnavigation input[name=navitem-action]').val("update");

					// set the nav item id
					$('#frmnavigation div.hiddenstuff').prepend('<input type="hidden" name="navitem-id" value="'+itemdata.id+'"/>');

				})	

			});

			// execute on model hide
			$('#myModal').on('hidden.bs.modal', function(e){
				$('#frmnavigation').trigger('reset');		// reset form
				$('.requiredmsg').css('display','none');	// hide Required messages if visible
				$('#nav-image-preview').html('<p>Image not selected!</p>');

				if($('#frmnavigation input[name=navitem-id]').length)
				{
					// remove navitem-id hidden element
					$('#frmnavigation input[name=navitem-id]').remove();
					//navform.find('input[name=navitem-id]').remove();
				}
			});
	

		<?php
	break;

	case 'Experiences':
		?>

			console.log('Experiences JS Loaded');
			//var scriptname = 'navigation';

			/*
			var pg = '<?php echo $_GET['pg']; ?>';

			$.get('http://freudenberg.cms/getnavitemlist.php', { pgid: pg }, function(navitemlist){
				$('.nav-item-list').html(navitemlist);
				//console.log(navitemlist);
			})*/

			$('#popexpmodel').on('click', function(e){
				e.preventDefault();
				console.log('clicked');

				// reset form
				$('#frmexperience').trigger('reset');

				$('#frmexperience input[name=expitem-action]').val("add");
				$('#frmexperience input[name=expitemsave]').val('Add Item!');
				//$('#frmnavigation').append('<input type="hidden" name="navtype" value="1"/>');
				//$('#frmnavigation input[name=navitem-layout]').val("1");  -> not using
				$('#myExperienceModal').modal('show');
			});

			/* not using anymore
			$('#popme2').on('click', function(e){
				e.preventDefault();
				console.log('clicked');
				$('#frmnavigation input[name=navitem-layout]').val("2");
				$('#myModal').modal('show');
			});*/

			// remove (Required) on focus
			$('#frmexperience :input').on('focus', function(){
				//console.log('focused');
				$(this).siblings('label').children('.requiredmsg').css('display', 'none');
			})

			$('#frmexperience').on('submit', function(e){
				e.preventDefault();

				var expform = $(this);

				var errors = false;
				var formData = new FormData();

				var needfile = true;

				//console.log(formData);

				//formData.append('hello', 'ami');		-> test append (works)

				//console.log(formData.getAll('hello')); 	-> debug (works)

				// validate each input element
				$.each($(this).find(':input'), function(){
					//console.log($(this).attr('name'));

					element = $(this);


					if(element.val() == '')
					{
						// skip checking image file for upadtes
						if(element.attr('name') == 'expitem-image')
						{
							if(expform.find('input[name=expitem-action]').val() == 'update')
							{
								// skip iteration
								return true;
							}
						}

						errors = true;
						element.siblings('label').children('.requiredmsg').css('display','inline');
					}
					else
					{
						// append to form data. exclude navitem-image
						if(element.attr('name') != 'expitem-image')
						{
							//payload[element.attr('name')] = element.val();
							formData.append(element.attr('name'), element.val());
						}
						
					}
				})

				/* this also does what above does.
				$(this).find(':input').each(function(index, element){

					console.log(element.val());
				})*/


				/* this is not good coding. so disabled it
				var navitemname = $(this).find('input[name=navitem-name]');
				var navitemtargetpg = $(this).find('select[name=navitem-target]');
				var navitemtagline = $(this).find('input[name=navitem-tagline]');
				var navitemdesc = $(this).find('textarea[name=navitem-desc]');
				var navitemimage = $(this).find('input[name=navitem-image]');
				var navitemlayouttype = $(this).find('input[name=navitem-layout]');
				var navitempageid = $(this).find('input[name=navitem-page-id]');

				//console.log(navitemtargetpg.val());

				// validate items
				var itemstovalidate = [navitemname,navitemtargetpg,navitemtagline,navitemdesc,navitemimage];

				var errors = false;

				$.each(itemstovalidate, function(index, value){

					if(value.val() == '')
					{
						errors = true;
						value.siblings('label').children('.requiredmsg').css('display','inline');
					}
					
				});*/

				
				if(errors)
				{
					console.log('Errors found!');
					return false;
				}
				else
				{

					//var file = document.getElementById('navitem-image').files;

					// if action == 'update' && there is some file selected 
					// or if action == 'add'
					if((expform.find('input[name=expitem-action]').val() == 'update' && expform.find('input[name=expitem-image]').val() != '') || expform.find('input[name=expitem-action]').val() == 'add')
					{
						var addfile = true;
						formData.append('imageupadated', 'true');
					}

					if(addfile == true)
					{
						var fileinputelement = $(this).find('input[name=expitem-image]');

						var file = fileinputelement[0].files[0];

						console.log(file);

						// validate file type
						if(!file.type.match('image.*'))
						{
							fileinputelement.siblings('label').children('.requiredmsg').html('Please Select an Image').css('display','inline');
							return false;
						}

						console.log('Good');

						formData.append(fileinputelement.attr('name'), file, file.name);			
					}


					/* debugging - works
					for(var pair of formData.entries()) {
			   			console.log(pair[0]+ ', '+ pair[1]); 
					}*/

					//console.log(formData);

					/* this is how you append to formData object
						// Files
						formData.append(name, file, filename);

						// Blobs
						formData.append(name, blob, filename);

						// Strings
						formData.append(name, value); 

					*/

					/*
						$.post(url,data,callback,datatype);
					*/

					//console.log($(this).attr('action'));		// get form's action url

					/* this causes an error (illegal invocation when it tries to serialize data. we have to set content type)
					$.post($(this).attr('action'), formData, function(res){

							console.log(res);

					}, "json");*/

					var urltopost = $(this).attr('action');

					$.ajax({
						method: 'POST',
						url: urltopost,
						data: formData,
						contentType: false,
						dataType: 'json',
						processData: false,

					})
					.done(function(res){
						console.log(res); 
						//response = JSON.parse(res);	// when we set the expected data type to json and when we send with json_encode, no need to parse it with JSON.parse(res)

						//console.log(res.status);
						
						if(res.status == true)
						{
							$.get('<?=SITE_URL?>/getexpitemlist.php', function(expitemlist){
								$('.exp-item-list').html(expitemlist);
								//console.log(navitemlist);
							})

							expform.trigger('reset');	// reset form

							// if action == 'update' remove navitem-id hidden element
							if(res.action == 'update')
							{
								// remove navitem-id hidden element
								expform.find('input[name=expitem-id]').remove();
							}

							$('#exp-image-preview').html('<p>Image not selected!</p>');		// reset preview

							$('#myExperienceModal').modal('hide');

							//console.log('Success');
						}
					});


				}

			})

			// handle edit
			$('.exp-item-list').on('click', '.editexpitem', function(e){
				e.preventDefault();
				//console.log($(this).attr('data-nav-item-id'));
				console.log('hello click');
				$('#myExperienceModal').modal('show');

				var expitemid = $(this).attr('data-exp-item-id');
				console.log(expitemid);

				$.get('<?=SITE_URL?>/getexpitemdetails.php', { expitem: expitemid }, function(expitemlist){
					//$('.nav-item-list').html(navitemlist);
					console.log(expitemlist);
					itemdata = JSON.parse(expitemlist);		// parse json_encoded response from php. this is required as we didn't tell $.get that we expect to get json
					// {"id":"28","page_id":"20","title":"fdsa","tagline":"fdsffds","description":"fdsa","target_pg":"1","image":"d1d3c92262fc555b604c7ac70b5e0d2f_555007896.png"}

					//console.log(itemdata.title);

					// set form data
					$('#frmexperience input[name=expitem-tagline]').val(itemdata.tagline);
					$('#frmexperience textarea[name=expitem-desc]').val(itemdata.description);
					$('#frmexperience input[name=expitemsave]').val('Update Item!');
					$('#exp-image-preview').html('<img src="<?=LIVE_SITE_URL?>/images/experiences/'+itemdata.image+'" style="max-width: 100%; max-height: 100%;">');
					$('#frmexperience select[name=expitem-destination]').val(itemdata.destination_id).change();

					// set the action
					$('#frmexperience input[name=expitem-action]').val("update");

					// set the nav item id
					$('#frmexperience div.exphiddenstuff').prepend('<input type="hidden" name="expitem-id" value="'+itemdata.id+'"/>');

				})	

			});

			// execute on model hide
			$('#myExperienceModal').on('hidden.bs.modal', function(e){
				$('#frmexperience').trigger('reset');		// reset form
				$('.requiredmsg').css('display','none');	// hide Required messages if visible
				$('#exp-image-preview').html('<p>Image not selected!</p>');

				if($('#frmexperience input[name=expitem-id]').length)
				{
					// remove expitem-id hidden element
					$('#frmexperience input[name=expitem-id]').remove();
					//navform.find('input[name=navitem-id]').remove();
				}
			});
			
		<?php
	break;

	case 'Gallery':
	?>

		// initially set this to $_GET['data']
		section = '<?=$_GET['data']?>';
		pg = '<?=$_GET['pg']?>';


		// on subsequent clicks set this to that value
		$('.tablinks').on('click', function(){

			var extra = $(this).attr('data-extra');

			section = extra;

			if(extra == 'gallery')
			{
				doUploadify('<?=SITE_URL?>', 'gallery', pg, 'uploadify1', 'uploadify2');
			}
			else if(extra == 'slider')
			{
				doUploadify('<?=SITE_URL?>', 'slider', pg, 'uploadify2', 'uploadify1');
			}

		});


		if(section == 'gallery')
		{
			doUploadify('<?=SITE_URL?>', 'gallery', pg, 'uploadify1', 'uploadify2');
		}
		else if(section == 'slider')
		{
			doUploadify('<?=SITE_URL?>', 'slider', pg, 'uploadify2', 'uploadify1');
		}

		//doUploadify('<?=SITE_URL?>', section, pg, 'uploadify');

		/** uploadify functionality */


		function doUploadify(baseurl, cat, pg, elmid, remove)
		{

			// hide stuff
  			$('#QueueUploadStatus').hide();
  			$('#QueueUploadStatus1').hide();

			// set some vars
			var filecount = 0;
			var totalfiles = 0;
			var uploaded = 0;

			// remove existing elements
			if($('#'+elmid).length)
			{
				$('#'+elmid).remove();

				if($('#'+elmid+'Uploader').length)
				{
					$('#'+elmid+'Uploader').remove();
				}
			}

			// remove other element
			if($('#'+remove).length)
			{
				$('#'+remove).remove();

				if($('#'+remove+'Uploader').length)
				{
					$('#'+remove+'Uploader').remove();
				}
			}	

			// create uploadify element
			$('#uploadify-container').prepend('<input type="file" name="'+elmid+'" id="'+elmid+'"/>');	

			// set element to apply uploadify
			var el = $('#'+elmid);

			// initialize uploadify with el
			el.uploadify({
				'uploader'       : baseurl+'/assets/uploadify.swf',
				'script'         : baseurl+'/uploadify.php?cat='+cat,
				'cancelImg'      : baseurl+'/assets/cancel.png',
				'folder'         : pg,
				'queueID'        : 'fileQueue',
				'auto'           : true,
				'multi'          : true,
				'queueSizeLimit' : 5,
				'fileDesc'		 : 'jpg, gif',
				'fileExt'		 : '*.jpg;*.gif',
				'sizeLimit'      : '5120000',//max size bytes - 5 MB
				'checkScript'    : baseurl+'/check.php', //if we take this out, it will never replace files, otherwise asks if we want to replace
				'onAllComplete'  : function() {
									//$('#switch-effect').unbind('change');
									//$('#toggle-slideshow').unbind('click');
									//galleries[0].slideshow.stop();
									//start();
									//$('#fileQueueCounter').html('<span style="color: green;">Finished Uploading '+totalfiles+' Files!</span>');
									$('#QueueUploadStatus').hide();

									$('#QueueUploadStatus1').html('Finished Uploading '+totalfiles+' Files!').show('fast').fadeOut(3000);

									console.log('gallery js loaded section: '+cat);

									//$('#fileQueue').html('');
									
									filecount = 0;
									totalfiles = 0;
									uploaded = 0;

									},

				onSelect       	 : function() {

									filecount++;
									totalfiles = filecount;

									//console.log(filecount);
									//$('#fileQueueCounter').html(totalfiles+' Files Selected');

									},

				onCancel       	 : function() {

									filecount--;
									totalfiles = filecount;
									$('#fileQueueCounter').html(totalfiles+' Files Selected');

									if(totalfiles == 0)
									{
										$('#fileQueueCounter').html('');
									}

									},

				onComplete     	 : function() {

										filecount--;
										outof = totalfiles - filecount;
										$('#fileQueueRunningCounter').html(outof);
										$('#fileQueueTotal').html(totalfiles);
										$('#QueueUploadStatus').show();

										// call populate()
										//populatePgSlider('<?=SITE_URL?>', <?=$_GET['pg']?>);
										console.log('oncomplete section: '+cat);
										getPageGallery(baseurl, pg, cat);
									}					
			});		
		}
					
		

		// have chained magnificPopup('open') as it required two clicks initiatially
		(function(){ 	

			/** magnific popup */			


			$('.slider-image-list').on('click', '.slider-item-image', function(e){
				
				e.preventDefault();
				//$(this).trigger('focus');
				$(this).magnificPopup({
					type: 'ajax'
				}).magnificPopup('open');

				/* inline popup
				e.preventDefault();
				var imagepath = $(this).attr('href');

				$(this).magnificPopup({
					items: {
						src: '<div class="image-detail-view white-popup"><div style="float: left; width: 330px;"><img src="'+imagepath+'" style="width: 100%;"/></div><div class="" style="margin-left: 330px; padding-left: 10px;"><form id="frmsliderimgdetailupdate"><div class="field"><label for="alttext">Alt Tag Text</label><br><input type="text" name="alttext" id="alttext"/></div><div class="field"><label for="desctext">Short Caption</label><br><textarea name="desctext" id="desctext" rows="3" cols="50"></textarea></div><div class="field"><input type="submit" name="submit" value="Update"/></div></form></div><br style="clear: left;"/></div>',
						type: 'inline'
						//type: 'ajax'
					}
				}).magnificPopup('open');*/
			})	


			/* update slider image alt and desc
			$('#frmsliderimgdetailupdate').on('submit', function(e){
				e.preventDefault();
				console.log('clicked to update image details');
			})*/

			$('body').on('click', '#frmsliderimgdetailupdate input[name=submit]', function(e){
				e.preventDefault();
				console.log('clicked to update image details');
				//var form = $(this);

				var submitaction = $(this).attr('data-action');
				console.log(submitaction);

				// if fields are empty return false
				if(submitaction == 'update')
				{
					if($('#frmsliderimgdetailupdate input[name=alttext]').val() == '' &&  $('#frmsliderimgdetailupdate textarea[name=desctext]').val() == '')
					{
						return false;
					} 						
				}

				console.log('laaaaa')
				// we are good. prepare form data
				var formData = new FormData();

				formData.append('alttext', $('#frmsliderimgdetailupdate input[name=alttext]').val());
				formData.append('desctext', $('#frmsliderimgdetailupdate textarea[name=desctext]').val());
				formData.append('frmaction', submitaction);
				formData.append('imagename', $('#frmsliderimgdetailupdate input[name=imagename]').val());
				formData.append('imageid', $('#frmsliderimgdetailupdate input[name=imageid]').val());


				var urltopost = $('#frmsliderimgdetailupdate').attr('action');

				$.ajax({
					method: 'POST',
					url: urltopost,
					data: formData,
					contentType: false,
					dataType: 'json',
					processData: false,

				})
				.done(function(res){
					console.log(res); 

					if(res.status == true)
					{
						$.magnificPopup.close();

						//populatePgSlider('<?=SITE_URL?>', <?=$_GET['pg']?>);
						getPageGallery('<?=SITE_URL?>', pg, section);
					}
					//response = JSON.parse(res);	// when we set the expected data type to json and when we send with json_encode, no need to parse it with JSON.parse(res)

					//console.log(res.status);
					/*
					if(res.status == true)
					{
						$.get('http://freudenberg.cms/getexpitemlist.php', function(expitemlist){
							$('.exp-item-list').html(expitemlist);
							//console.log(navitemlist);
						})

						expform.trigger('reset');	// reset form

						// if action == 'update' remove navitem-id hidden element
						if(res.action == 'update')
						{
							// remove navitem-id hidden element
							expform.find('input[name=expitem-id]').remove();
						}

						$('#exp-image-preview').html('<p>Image not selected!</p>');		// reset preview

						$('#myExperienceModal').modal('hide');

						//console.log('Success');
					}*/
				});					
			})


		})();			


	<?php
	break;

	case 'Properties':
	?>
			console.log('Properties JS Loaded');
			//var scriptname = 'navigation';

			/*
			var pg = '<?php echo $_GET['pg']; ?>';

			$.get('http://freudenberg.cms/getnavitemlist.php', { pgid: pg }, function(navitemlist){
				$('.nav-item-list').html(navitemlist);
				//console.log(navitemlist);
			})*/

			$('#poppropmodel').on('click', function(e){
				e.preventDefault();
				console.log('clicked');

				// reset form
				$('#frmproperties').trigger('reset');

				$('#frmproperties input[name=propitem-action]').val("add");
				//$('#frmnavigation').append('<input type="hidden" name="navtype" value="1"/>');
				//$('#frmnavigation input[name=navitem-layout]').val("1");  -> not using
				$('#myPropertiesModal').modal('show');
			});

			/* not using anymore
			$('#popme2').on('click', function(e){
				e.preventDefault();
				console.log('clicked');
				$('#frmnavigation input[name=navitem-layout]').val("2");
				$('#myModal').modal('show');
			});*/

			// remove (Required) on focus
			$('#frmproperties :input').on('focus', function(){
				//console.log('focused');
				$(this).siblings('label').children('.requiredmsg').css('display', 'none');
			})

			$('#frmproperties').on('submit', function(e){
				e.preventDefault();

				var propform = $(this);

				var errors = false;
				var formData = new FormData();

				var needfile = true;

				//console.log(formData);

				//formData.append('hello', 'ami');		-> test append (works)

				//console.log(formData.getAll('hello')); 	-> debug (works)

				// validate each input element
				$.each($(this).find(':input'), function(){
					//console.log($(this).attr('name'));

					element = $(this);


					if(element.val() == '')
					{
						// skip checking image file for upadtes
						if(element.attr('name') == 'propitem-image')
						{
							if(propform.find('input[name=propitem-action]').val() == 'update')
							{
								// skip iteration
								return true;
							}
						}

						errors = true;
						errel = element.siblings('label').children('.requiredmsg');

						if(errel.length)
						{
							errel.css('display','inline');
						}
						//element.siblings('label').children('.requiredmsg').css('display','inline');
					}
					else
					{
						// append to form data. exclude navitem-image
						if(element.attr('name') != 'propitem-image')
						{
							//payload[element.attr('name')] = element.val();
							formData.append(element.attr('name'), element.val());
						}
						
					}
				})

				/* this also does what above does.
				$(this).find(':input').each(function(index, element){

					console.log(element.val());
				})*/


				/* this is not good coding. so disabled it
				var navitemname = $(this).find('input[name=navitem-name]');
				var navitemtargetpg = $(this).find('select[name=navitem-target]');
				var navitemtagline = $(this).find('input[name=navitem-tagline]');
				var navitemdesc = $(this).find('textarea[name=navitem-desc]');
				var navitemimage = $(this).find('input[name=navitem-image]');
				var navitemlayouttype = $(this).find('input[name=navitem-layout]');
				var navitempageid = $(this).find('input[name=navitem-page-id]');

				//console.log(navitemtargetpg.val());

				// validate items
				var itemstovalidate = [navitemname,navitemtargetpg,navitemtagline,navitemdesc,navitemimage];

				var errors = false;

				$.each(itemstovalidate, function(index, value){

					if(value.val() == '')
					{
						errors = true;
						value.siblings('label').children('.requiredmsg').css('display','inline');
					}
					
				});*/

				
				if(errors)
				{
					console.log('Errors found!');
					return false;
				}
				else
				{

					//var file = document.getElementById('navitem-image').files;

					// if action == 'update' && there is some file selected 
					// or if action == 'add'
					if((propform.find('input[name=propitem-action]').val() == 'update' && propform.find('input[name=propitem-image]').val() != '') || propform.find('input[name=propitem-action]').val() == 'add')
					{
						var addfile = true;
						formData.append('imageupadated', 'true');
					}

					if(addfile == true)
					{
						var fileinputelement = $(this).find('input[name=propitem-image]');

						var file = fileinputelement[0].files[0];

						console.log(file);

						// validate file type
						if(!file.type.match('image.*'))
						{
							fileinputelement.siblings('label').children('.requiredmsg').html('Please Select an Image').css('display','inline');
							return false;
						}

						console.log('Good');

						formData.append(fileinputelement.attr('name'), file, file.name);			
					}


					/* debugging - works
					for(var pair of formData.entries()) {
			   			console.log(pair[0]+ ', '+ pair[1]); 
					}*/

					//console.log(formData);

					/* this is how you append to formData object
						// Files
						formData.append(name, file, filename);

						// Blobs
						formData.append(name, blob, filename);

						// Strings
						formData.append(name, value); 

					*/

					/*
						$.post(url,data,callback,datatype);
					*/

					//console.log($(this).attr('action'));		// get form's action url

					/* this causes an error (illegal invocation when it tries to serialize data. we have to set content type)
					$.post($(this).attr('action'), formData, function(res){

							console.log(res);

					}, "json");*/

					var urltopost = $(this).attr('action');

					$.ajax({
						method: 'POST',
						url: urltopost,
						data: formData,
						contentType: false,
						dataType: 'json',
						processData: false,

					})
					.done(function(res){
						console.log(res); 
						//response = JSON.parse(res);	// when we set the expected data type to json and when we send with json_encode, no need to parse it with JSON.parse(res)

						//console.log(res.status);
						
						if(res.status == true)
						{
							$.get('<?=SITE_URL?>/getpropitemlist.php', function(propitemlist){
								$('.prop-item-list').html(propitemlist);
								//console.log(navitemlist);
							})

							propform.trigger('reset');	// reset form

							// if action == 'update' remove navitem-id hidden element
							if(res.action == 'update')
							{
								// remove navitem-id hidden element
								propform.find('input[name=propitem-id]').remove();
							}

							$('#prop-image-preview').html('<p>Image not selected!</p>');		// reset preview

							$('#myPropertiesModal').modal('hide');

							//console.log('Success');
						}
					});


				}

			})

			// handle edit
			$('.prop-item-list').on('click', '.editpropitem', function(e){
				e.preventDefault();
				//console.log($(this).attr('data-nav-item-id'));
				console.log('hello click');
				$('#myPropertiesModal').modal('show');

				var propitemid = $(this).attr('data-prop-item-id');
				console.log(propitemid);

				$.get('<?=SITE_URL?>/getpropitemdetails.php', { propitem: propitemid }, function(propitemlist){
					//$('.nav-item-list').html(navitemlist);
					console.log(propitemlist);
					itemdata = JSON.parse(propitemlist);		// parse json_encoded response from php. this is required as we didn't tell $.get that we expect to get json
					// {"id":"28","page_id":"20","title":"fdsa","tagline":"fdsffds","description":"fdsa","target_pg":"1","image":"d1d3c92262fc555b604c7ac70b5e0d2f_555007896.png"}

					//console.log(itemdata.title);

					// set form data
					$('#frmproperties input[name=propitem-name]').val(itemdata.prop_name);
					$('#frmproperties textarea[name=propitem-desc]').val(itemdata.prop_description);
					$('#frmproperties input[name=propitem-tlink]').val(itemdata.prop_link);
					$('#frmproperties input[name=propitemsave]').val('Update Item!');
					$('#prop-image-preview').html('<img src="<?=LIVE_SITE_URL?>/images/properties/'+itemdata.prop_image+'" style="max-width: 100%; max-height: 100%;">');
					$('#frmproperties select[name=propitem-browsertab]').val(itemdata.browser_window).change();

					// set the action
					$('#frmproperties input[name=propitem-action]').val("update");

					// set the nav item id
					$('#frmproperties div.prophiddenstuff').prepend('<input type="hidden" name="propitem-id" value="'+itemdata.id+'"/>');

				})	

			});

			// execute on model hide
			$('#myPropertiesModal').on('hidden.bs.modal', function(e){
				$('#frmproperties').trigger('reset');		// reset form
				$('.requiredmsg').css('display','none');	// hide Required messages if visible

				if($('#frmproperties input[name=propitem-id]').length)
				{
					// remove propitem-id hidden element
					$('#frmproperties input[name=propitem-id]').remove();
					//navform.find('input[name=navitem-id]').remove();
				}

			});
			


	<?php
	break;

	case 'Promotions':
	?>

			console.log('Promotions JS Loaded');
			//var scriptname = 'navigation';

			passedpropid = '<?=$_GET["data"]?>';
			console.log('passed propid :'+passedpropid);

			// hide trigger2 file upload div
			$('#frmpromotion div.trigger2').hide();			

			//formData = new FormData();
			promoImage = {};

			popupImage = {};

			formData = new FormData();

			toSubmit = {};			

			function validator1()
			{

				var promoDataObj = {};

				// define form elements for promotion details
				var elements = [
						$('#frmpromotion select[name=promoitem-property]'),
						$('#frmpromotion input[name=promoitem-title]'),
						$('#frmpromotion textarea[name=promoitem-desc]'),
						$('#frmpromotion input[name=promoitem-image]'),
						//$('#frmpromotion input[name=promoitem-trigger]'),
						$('#frmpromotion input[name=promoitem-enabled]'),
						$('#frmpromotion input[name=promoitem-action]')
					];

				var form = $('#frmpromotion');				

				var errors = false;

				// validate each input element
				$.each(elements, function(index, element){
					//console.log($(this).attr('name'));

					//element = $(this);
					console.log(element.val());


					if(element.val() == '')
					{

						// skip checking for 'enabled' check box
						if(element.attr('name') == 'promoitem-enabled')
						{
							return true;
						}

						// skip checking image file for upadtes
						if(element.attr('name') == 'promoitem-image')
						{
							if(form.find('input[name=promoitem-action]').val() == 'update')
							{
								// skip iteration
								return true;
							}
						}


						errors = true;
						errel = element.siblings('label').children('.requiredmsg');

						if(errel.length)
						{
							errel.css('display','inline');
						}
						//element.siblings('label').children('.requiredmsg').css('display','inline');
					}
					else
					{

						if(element.attr('name') == 'promoitem-enabled')
						{
							if(element.is(':checked'))
							{
								promoDataObj['promoitem-enabled'] = element.val();
							}
						}
						else if(element.attr('name') == 'promoitem-action' && element.val() == 'update')
						{
							promoDataObj['promoitem-action'] = 'update';
							promoDataObj['promoitem-id'] = form.find('input[name=promoitem-id]').val();
						}
						else
						{
							if(element.attr('name') != 'promoitem-image')
							{
								promoDataObj[element.attr('name')] = element.val();
							}
							
						}
						
					}
					
				});

				console.log('in validation ....');
				console.log(promoDataObj);

				if(errors)
				{
					return false;
				}
				else
				{

					if(($('#frmpromotion').find('input[name=promoitem-action]').val() == 'update' && $('#frmpromotion').find('input[name=promoitem-image]').val() != '') || $('#frmpromotion').find('input[name=promoitem-action]').val() == 'add')
					{
						var addfile = true;
						//sformData.append('imageupadated', 'true');
					}

					if(addfile == true)
					{
						var fileinputelement = $('#frmpromotion').find('input[name=promoitem-image]');

						var file = fileinputelement[0].files[0];

						console.log(file);

						// validate file type
						if(!file.type.match('image.*'))
						{
							fileinputelement.siblings('label').children('.requiredmsg').html('Please Select an Image').css('display','inline');
							return false;
						}

						console.log('Good');

						//formData.append(fileinputelement.attr('name'), file, file.name);
						// keep this in a separate object to avoid losing file data when serialized 
						promoImage['el'] = fileinputelement;
						promoImage['name'] = file.name;
						promoImage['src'] = file;			
					}


					promoDataObj['promoitem-trigger'] = $('#frmpromotion input[name=promoitem-trigger]:checked').val(),

					console.log('no errors -> check object before returning..');
					console.log(promoDataObj);
					return promoDataObj;
				}
			}

			/*
			function getPropertyList()
			{
				$.get('<?=SITE_URL?>/getpropertylist.php', {propid: passedpropid}, function(propertylist){

					list = JSON.parse(propertylist);

					console.log(list);

					el = $('#frmpromotion select[name=promoitem-property]');

					$.each(list, function(index, value){
						el.prepend('<option value="'+value.id+'" '+(value.id == passedpropid ? "selected=\"selected\"" : "")+'>'+value.property_name+'</option>');
					})

					//el.prepend('<option value="">Select A Property</option>');
				})				
			}*/



			$('#poppromomodel').on('click', function(e){
				e.preventDefault();
				console.log('clicked');

				// reset form
				$('#frmpromotion').trigger('reset');		// this we can do when modal hides

				//$('#frmpromotion div.formbottom').html('');	// no need

				/* add submit btn - no need
				$('#frmpromotion div#promo-details').append('<div class="field formbottom" style="margin-top: 20px;"><input type="submit" class="btn btn-submit" name="promoformbtn" id="promoformbtn" value="Add Item!"/></div>');
				*/

				$('#promo-image-preview').html('<p>Image not selected!</p>');		// reset preview

				$('#frmpromotion input[name=promoitem-action]').val("add");
					
				/* no need
				$('#frmpromotion div.promohiddenstuff').append('<input type="hidden" name="promoitem-action" value="add"/>');
				*/

				//$('#frmnavigation').append('<input type="hidden" name="navtype" value="1"/>');
				//$('#frmnavigation input[name=navitem-layout]').val("1");  -> not using

				// populate properties select list
				getPropertyList('<?=SITE_URL?>', $('#frmpromotion select[name=promoitem-property]'), passedpropid, 0);

				/* no need
				$('#frmpromotion fieldset:first-child').fadeIn('slow');
				*/
				
				$('#myPromoModal').modal('show');

			});


			// remove (Required) on focus
			$('#frmpromotion :input').on('focus', function(){
				//console.log('focused');
				$(this).siblings('label').children('.requiredmsg').css('display', 'none');
			})

			/* no need
			$('#frmpromotion input[name=promoitem-page]').change(function(){

				
				//if($('#frmpromotion input[name=promoformbtn]').length)
				//{
					//$('#frmpromotion input[name=promoformbtn]').remove();
				//}

				//if($('#frmpromotion button[name=promoformnext]').length)
				//{
					//$('#frmpromotion button[name=promoformnext]').remove();
				//}

				$('#frmpromotion div.formbottom').html('');

				if($(this).is(':checked'))
				{
					$('#frmpromotion div.formbottom').html('<button type="button" class="btn btn-next" name="promoformnext" id="promoformnext">Next</button>');					
				}
				else
				{
					// add submit btn
					$('#frmpromotion div.formbottom').html('<input type="submit" class="btn btn-submit" name="promoformbtn" id="promoformbtn" value="Add Item!"/>');
				}
			})*/


			/* no need
			$('#frmpromotion').on('click', 'button[name=promoformnext]', function(){

				// valildate the form

				var res = validator1();

				if(!res)
				{
					console.log('found errors!!!!!!!!');
					return false;
				}


				console.log(res);
				//return false;


				// good -> add data to local session storage -> sessionStorage.setItem('key', 'value');
				sessionStorage.setItem('promodata', JSON.stringify(res));


				// bring step 2 
				var parent_fieldset = $(this).parents('fieldset');

				parent_fieldset.fadeOut(400, function() {
	    			$(this).next().fadeIn();
	    		});

				$('#myPromoModal div.modal-content').slideDown('slow', function(){
					$(this).css('height','550px');
				})

				// remove buttons from promo-details form
				$('#frmpromotion div.formbottom').remove();

				// attach buttons to promo-page form
				$('#frmpromotion div#promo-page').append('<div class="field formbottom" style="margin-top: 20px;"><button type="button" class="btn btn-previous" name="promoformprevious">Previous</button>&nbsp;<input type="submit" class="btn btn-submit" name="submitstep2" id="submitstep2" value="Add Promotion!"/></div>');

				// remove previous button if exists
				
				//if(!$('#frmpromotion button[name=promoformprevious]').length || !$('#frmpromotion input[name=submitstep2]').length)
				//{
					//$('#frmpromotion button[name=promoformprevious]').remove();
					//$('#frmpromotion div#promo-page').append('<div class="field formbottom" style="margin-top: 20px;"><button type="button" class="btn btn-previous" name="promoformprevious">Previous</button>&nbsp;<input type="submit" class="btn btn-submit" name="submitstep2" id="submitstep2" value="Add Promotion!"/></div>');
				//}
	    		
			})
			*/

			/* previous step - no need
		    $('#frmpromotion').on('click', 'button[name=promoformprevious]', function() {
		    	$(this).parents('fieldset').fadeOut(400, function() {
		    		
		    		
		    		//if(!$('#frmpromotion button[name=promoformnext]').length)
					//{
						//$('#frmpromotion div.formbottom').prepend('<button type="button" class="btn btn-next" name="promoformnext" id="promoformnext">Next</button>');
					//}

		    		$(this).prev().fadeIn();
		    		
		    		$('#myPromoModal div.modal-content').css('height', '420px');

		    		$('#frmpromotion div.formbottom').remove();

		    		$('#frmpromotion div#promo-details').append('<div class="field formbottom" style="margin-top: 20px;"><button type="button" class="btn btn-next" name="promoformnext" id="promoformnext">Next</button></div>');
		    		//$('#frmpromotion div.formbottom').html('<button type="button" class="btn btn-next" name="promoformnext" id="promoformnext">Next</button>');		

		    	});
		    });*/


			$('#frmpromotion').on('submit', function(e){
				e.preventDefault();

				var promoform = $(this);

				var errors = false;

				var needfile = true;

				//var toSubmit = {};

				/* if promodata is in the session get it and clear it - NO NEED
				if(sessionStorage.getItem('promodata'))
				{
					toSubmit = JSON.parse(sessionStorage.getItem('promodata'));
					console.log('Retrieved data stored in the session storage.');
					console.log(toSubmit);
					sessionStorage.clear();
					console.log('Cleared data from session storage.');
				}*/
				
				/* NO NEED
				var submitbtn = (promoform.find('input[name=promoformbtn]').length ? 'submit1' : (promoform.find('input[name=submitstep2]').length ? 'submit2' : ''));

				console.log('submit -> '+submitbtn);
				//return false;
				*/

				toSubmit = validator1();

				if(toSubmit == false)
				{
					console.log('Validator1 Returned FALSE!');
					errors = true;
				}

				/* NO NEED
				if(submitbtn == 'submit1')
				{
					//formData = sessionStorage.getItem('promodata');
					console.log('in submit1 action ..');
					toSubmit = validator1();

					if(toSubmit == false)
					{
						console.log('found errors in submit1...');
						errors = true;
					}

				}
				else
				{

					var PageDataFields = [
						$('#frmpromotion input[name=promoitem-page-title]'),
						$('#frmpromotion textarea[name=promoitem-page-desc]')
					];

					$.each(PageDataFields, function(i,f){
						
						if(f.val() == '')
						{
							errors = true;

							errel = f.siblings('label').children('.requiredmsg');

							if(errel.length)
							{
								errel.css('display','inline');
							}

						}
						else
						{
							//formData.append(element.attr('name'), element.val());
							toSubmit[f.attr('name')] = f.val();
						}
					});

				} */

				if(errors)
				{
					console.log('Cant Submit!!! Please Correct Errors!');
					console.log(toSubmit);
					return false;
				}
				else
				{
					console.log('Good to submit -> prepare form data');
					console.log(toSubmit);
					console.log(promoImage);

					// prepare formdata
					$.each(toSubmit, function(p, v){
						//console.log(p+' : '+v);
						formData.append(p,v);
					});

					// attach image if required
					if('name' in promoImage)
					{
						// formData.append(fileinputelement.attr('name'), file, file.name);
						formData.append(promoImage.el.attr('name'), promoImage.src, promoImage.name);						
					}


					// dispatch ajax
					var urltopost = promoform.attr('action');

					$.ajax({
						method: 'POST',
						url: urltopost,
						data: formData,
						contentType: false,
						dataType: 'json',
						processData: false,

					})
					.done(function(res){
						console.log('request returned ...');
						console.log(res); 		// Object {action: "add", status: true, msg: "success", item_id: "2"}

						/*
						if(res.action == 'add' && res.status == true)
						{
							// show page 
						}

						//return false;
						//response = JSON.parse(res);	// when we set the expected data type to json and when we send with json_encode, no need to parse it with JSON.parse(res)

						//console.log(res.status);*/
						
						if(res.status == true)
						{
							$.get('<?=SITE_URL?>/getpromoitemlist.php', { propertyid: passedpropid }, function(promoitemlist){
								$('.promo-item-list').html(promoitemlist);
								//console.log(navitemlist);
							})

							promoform.trigger('reset');	// reset form

							// if action == 'update' remove navitem-id hidden element
							if(res.action == 'update')
							{
								// remove navitem-id hidden element
								promoform.find('input[name=promoitem-id]').remove();
							}

							$('#promo-image-preview').html('<p>Image not selected!</p>');		// reset preview

							$('#myPromoModal').modal('hide');

							//console.log('Success');
						}
					});

				}

			})

			// handle edit
			$('.promo-item-list').on('click', '.editpromoitem', function(e){
				e.preventDefault();
				//console.log($(this).attr('data-nav-item-id'));
				console.log('promo item click');

				//$('#frmpromotion div#promo-details').append('<div class="field formbottom" style="margin-top: 20px; clear: left;"></div>');

				//$('#promo-image-preview').html('<p>Image not selected!</p>');		// reset preview

				var promoitemid = $(this).attr('data-promo-item-id');
				console.log(promoitemid);

				$.get('<?=SITE_URL?>/getpromoitemdetails.php', { promoitem: promoitemid }, function(promoitemlist){
					//$('.nav-item-list').html(navitemlist);
					console.log(promoitemlist);
					
					itemdata = JSON.parse(promoitemlist);		// parse json_encoded response from php. this is required as we didn't tell $.get that we expect to get json
					// {"id":"28","page_id":"20","title":"fdsa","tagline":"fdsffds","description":"fdsa","target_pg":"1","image":"d1d3c92262fc555b604c7ac70b5e0d2f_555007896.png"}

					//console.log(itemdata.title);

					// set form data
					getPropertyList('<?=SITE_URL?>', $('#frmpromotion select[name=promoitem-property]'), passedpropid, itemdata.property_id);
					$('#frmpromotion input[name=promoitem-title]').val(itemdata.title);
					$('#frmpromotion textarea[name=promoitem-desc]').val(itemdata.description);
					$('#frmpromotion select[name=promoitem-property]').val(itemdata.property_id).change();
					$('#frmpromotion input[name=promoitem-trigger][value="'+itemdata.trigger_action+'"]').prop('checked', true);

					/*
					if(itemdata.haspage == 1)
					{
						//$('#frmpromotion input[name=promoitem-page-title]').val(itemdata.pp_title);
						//$('#frmpromotion textarea[name=promoitem-page-desc]').val(itemdata.pp_desc);

						$('#frmpromotion input[name=promoitem-page]').prop('checked', true);
						//$('#frmpromotion div.formbottom').prepend('<button type="button" class="btn btn-next" name="promoformnext" id="promoformnext">Next</button>');
					}*/

					if(itemdata.enabled == 1)
					{
						$('#frmpromotion input[name=promoitem-enabled').prop('checked', true);
					}
					

					$('#frmpromotion input[name=promoitemsave]').val('Update Item!');

					$('#promo-image-preview').html('<img src="<?=LIVE_SITE_URL?>/images/promotions/'+itemdata.property_slug+'/'+itemdata.image+'" style="max-width: 100%; max-height: 100%;">');
					//$('#frmproperties select[name=propitem-browsertab]').val(itemdata.browser_window).change();

					// set the action
					//$('#frmpromotion div.promohiddenstuff').append('<input type="hidden" name="promoitem-action" value="update"/>');
					$('#frmpromotion input[name=promoitem-action]').val("update");

					// set promo item id
					$('#frmpromotion div.promohiddenstuff').prepend('<input type="hidden" name="promoitem-id" value="'+itemdata.id+'"/>');

				})

				//$('#frmpromotion fieldset:first-child').fadeIn('slow');
				$('#myPromoModal').modal('show');	

			});

			// execute on model hide
			$('#myPromoModal').on('hidden.bs.modal', function(e){
				$('#frmpromotion').trigger('reset');		// reset form
				$('#frmpromotion select[name=promoitem-property]').html('');
				$('.requiredmsg').css('display','none');	// hide Required messages if visible
				//$('#frmpromotion fieldset').css('display', 'none');
				//$('#frmpromotion div.formbottom').remove();
				//$('#frmpromotion div.promohiddenstuff').html('');
				
				//$('#myPromoModal div.modal-content').css('height', '420px');

				if($('#frmpromotion input[name=promoitem-id]').length)
				{
					// remove promoitem-id hidden element
					$('#frmpromotion input[name=promoitem-id]').remove();
					//navform.find('input[name=navitem-id]').remove();
				}				
			});

			$('select[name=property-selector]').change(function(){
				console.log('property selector: '+ $(this).val());
				var prid = $(this).val();
				getPromotionData('<?=SITE_URL?>', prid);				
			})

			// trigger2 toggle
			$('#frmpromotion input[name=promoitem-trigger]').on('change', function(){
				if($(this).val() == 'image')
				{
					$('#frmpromotion div.trigger2').show();
				}
				else
				{
					$('#frmpromotion div.trigger2').hide();
				}
			})

			/*
			$('#frmpromotion input[id=trigger2]').on('click', function(){
				console.log('helo halo halo');
				$('#frmpromotion div.trigger2').show();
			})*/
			/*
			$('.promo-item-list').on('change', '#property-selector', function(){
				console.log('property selector: '+ $(this).val());
				var prid = $(this).val();
				getPromotionData('<?=SITE_URL?>', prid);
			})*/

			/* image manager - based on magnific popup */
			$('body').on('click', '.promo-item-image', function(e){
				
				e.preventDefault();
				//$(this).trigger('focus');
				$(this).magnificPopup({
					type: 'ajax'
				}).magnificPopup('open');

				/* inline popup
				e.preventDefault();
				var imagepath = $(this).attr('href');

				$(this).magnificPopup({
					items: {
						src: '<div class="image-detail-view white-popup"><div style="float: left; width: 330px;"><img src="'+imagepath+'" style="width: 100%;"/></div><div class="" style="margin-left: 330px; padding-left: 10px;"><form id="frmsliderimgdetailupdate"><div class="field"><label for="alttext">Alt Tag Text</label><br><input type="text" name="alttext" id="alttext"/></div><div class="field"><label for="desctext">Short Caption</label><br><textarea name="desctext" id="desctext" rows="3" cols="50"></textarea></div><div class="field"><input type="submit" name="submit" value="Update"/></div></form></div><br style="clear: left;"/></div>',
						type: 'inline'
						//type: 'ajax'
					}
				}).magnificPopup('open');*/
			});

			/* process promo image upload */
			$('body').on('click', '#frmpromoimgprocess input[name=submit]', function(e){
				e.preventDefault();
				console.log('promo popup image');
				//var form = $(this);

				var submitaction = $(this).attr('data-action');
				console.log(submitaction);

				var formData = new FormData();

				// if there's no file -> error
				if($('#frmpromoimgprocess input[name=promopop-image]').val() == '')
				{
					console.log('image not selected');
					// $(this).siblings('label').children('.requiredmsg').css('display', 'none');
					$('#frmpromoimgprocess input[name=promopop-image]').siblings('label').children('span.requiredmsg').html('Please select an image!').css('display','initial');
					return false;
				}
				else
				{

					//var fileinputelement = $('#frm...').find('input[name=....]');
					var promopopimgel = $('#frmpromoimgprocess input[name=promopop-image]');

					ppimgfile = promopopimgel[0].files[0];

					// validate file type
					if(!ppimgfile.type.match('image.*'))
					{
						promopopimgel.siblings('label').children('span.requiredmsg').html('Wrong File Type! Please Select an Image.').css('display','initial');
						return false;
					}

					formData.append(promopopimgel.attr('name'), ppimgfile, ppimgfile.name);					
				} 

				// we are good. prepare rest of the form data
				formData.append('do', submitaction);			// action
				formData.append('promo-id', $('#frmpromoimgprocess input[name=promo-item-id]').val());
				formData.append('image-id', $('#frmpromoimgprocess input[name=promopop-image-id]').val());

				var urltopost = $('#frmpromoimgprocess').attr('action');

				$.ajax({
					method: 'POST',
					url: urltopost,
					data: formData,
					contentType: false,
					dataType: 'json',
					processData: false,

				})
				.done(function(res){
					console.log(res); 

					
					if(res.status == true)
					{
						$.magnificPopup.close();

						getPromotionData('<?=SITE_URL?>', passedpropid);
						//populatePgSlider('<?=SITE_URL?>', <?=$_GET['pg']?>);
						//getPageGallery('<?=SITE_URL?>', pg, section);
					}
				});

			});


			$('body').on('focus', '#frmpromoimgprocess input[name=promopop-image]', function(){
				console.log('focused');
				$(this).siblings('label').children('span.requiredmsg').html('').css('display','none');
			});			
			

	<?php
	break;

	case 'Downloads':
	?>

			console.log('Downloads JS Loaded');
			//var scriptname = 'navigation';

			// set property id
			passedpropid = '<?=$_GET["data"]?>';
			console.log('passed propid :'+passedpropid);

			// open modal
			$('#popdownloadmodel').on('click', function(e){
				e.preventDefault();
				console.log('modal trigger clicked');

				// reset form
				$('#frmdownloads').trigger('reset');		// this we can do when modal hides. no harm having it in both places

				// set form action and submit button name
				$('#frmdownloads input[name=downloaditem-action]').val("add");
				$('#frmdownloads input[name=downloaditemsave]').val('Add Item!');

				// populate properties select list
				getPropertyList('<?=SITE_URL?>', $('#frmdownloads select[name=downloaditem-property]'), passedpropid, 0);

				// show form
				$('#myDownloadModal').modal('show');

			});


			// remove (Required) text set on error elements when focused
			$('#frmdownloads :input').on('focus', function(){
				//console.log('focused');
				$(this).siblings('label').children('.requiredmsg').css('display', 'none');
			})


			//formData = new FormData();
			fileData = {};

			formData = new FormData();

			toSubmit = {};			

			function validator()
			{

				var formDataObj = {};

				// define form elements for promotion details
				var elements = [
						$('#frmdownloads select[name=downloaditem-property]'),
						$('#frmdownloads input[name=downloaditem-title]'),
						$('#frmdownloads input[name=downloaditem-file]'),
						$('#frmdownloads input[name=downloaditem-action]')
					];

				var form = $('#frmdownloads');				

				var errors = false;

				// validate each input element
				$.each(elements, function(index, element){
					//console.log($(this).attr('name'));

					//element = $(this);
					console.log(element.val());


					if(element.val() == '')
					{

						// skip checking image file for upadtes
						if(element.attr('name') == 'downloaditem-file')
						{
							if(form.find('input[name=downloaditem-action]').val() == 'update')
							{
								// skip iteration
								return true;
							}
						}

						errors = true;
						errel = element.siblings('label').children('.requiredmsg');

						if(errel.length)
						{
							errel.css('display','inline');
						}
						//element.siblings('label').children('.requiredmsg').css('display','inline');
					}
					else
					{

						if(element.attr('name') == 'downloaditem-action' && element.val() == 'update')
						{
							formDataObj['downloaditem-action'] = 'update';
							formDataObj['downloaditem-id'] = form.find('input[name=downloaditem-id]').val();
						}
						else
						{
							if(element.attr('name') != 'downloaditem-file')
							{
								formDataObj[element.attr('name')] = element.val();
							}
							
						}
						
					}
					
				});

				console.log('in validation ....');
				console.log(formDataObj);

				if(errors)
				{
					return false;
				}
				else
				{

					if(($('#frmdownloads').find('input[name=downloaditem-action]').val() == 'update' && $('#frmdownloads').find('input[name=downloaditem-file]').val() != '') || $('#frmdownloads').find('input[name=downloaditem-action]').val() == 'add')
					{
						var addfile = true;
						//sformData.append('imageupadated', 'true');
					}

					if(addfile == true)
					{
						var fileinputelement = $('#frmdownloads').find('input[name=downloaditem-file]');

						var file = fileinputelement[0].files[0];

						console.log(file);

						/* validate file type
						if(!file.type.match('application/pdf'))
						{
							fileinputelement.siblings('label').children('.requiredmsg').html('Please Select a PDF file!').css('display','inline');
							return false;
						}*/

						console.log('File Good!');

						//formData.append(fileinputelement.attr('name'), file, file.name);
						// keep this in a separate object to avoid losing file data when serialized 
						fileData['el'] = fileinputelement;
						fileData['name'] = file.name;
						fileData['src'] = file;			
					}					

					console.log('no errors -> check object before returning..');
					console.log(formDataObj);
					return formDataObj;
				}
			}


			$('#frmdownloads').on('submit', function(e){
				
				// prevent submit
				e.preventDefault();

				// assign form object to a variable
				var downloaderform = $(this);

				// set errors to FALSE (default assumed)
				var errors = false;

				// set needfile to TRUE (default assumed)
				var needfile = true;

				// validate form inputs and assign the result to toSubmit var
				toSubmit = validator();

				if(toSubmit == false)
				{
					console.log('Validator Returned FALSE!');
					errors = true;
				}


				if(errors)
				{
					console.log('Cant Submit!!! Please Correct Errors!');
					console.log(toSubmit);
					return false;
				}
				else
				{
					console.log('Good to submit -> prepare form data');
					console.log(toSubmit);
					console.log(fileData);

					// prepare formdata
					$.each(toSubmit, function(p, v){
						//console.log(p+' : '+v);
						formData.append(p,v);
					});

					// attach image if required
					if('name' in fileData)
					{
						// formData.append(fileinputelement.attr('name'), file, file.name);
						formData.append(fileData.el.attr('name'), fileData.src, fileData.name);						
					}

					// dispatch ajax
					var urltopost = downloaderform.attr('action');

					$.ajax({
						method: 'POST',
						url: urltopost,
						data: formData,
						contentType: false,
						dataType: 'json',
						processData: false,

					})
					.done(function(res){
						console.log('request returned ...');
						console.log(res); 		// Object {action: "add", status: true, msg: "success", item_id: "2"}
						
						if(res.status == true)
						{
							$.get('<?=SITE_URL?>/getdownloaditemlist.php', { propertyid: passedpropid }, function(downloaditemlist){
								$('.download-item-list').html(downloaditemlist);
								//console.log(navitemlist);
							})

							downloaderform.trigger('reset');	// reset form

							// if action == 'update' remove navitem-id hidden element
							if(res.action == 'update')
							{
								// remove navitem-id hidden element
								downloaderform.find('input[name=downloaditem-id]').remove();
							}

							$('#myDownloadModal').modal('hide');

							//console.log('Success');
						}
					});

				}

			})

			// handle item edit
			$('.download-item-list').on('click', '.editdownloaditem', function(e){
				e.preventDefault();
				console.log('download item click');


				var downloaditemid = $(this).attr('data-download-item-id');
				console.log(downloaditemid);

				$.get('<?=SITE_URL?>/getdownloaditemdetails.php', { downloaditem: downloaditemid }, function(downloaditemdetails){
					//$('.nav-item-list').html(navitemlist);
					console.log(downloaditemdetails);
					
					itemdata = JSON.parse(downloaditemdetails);		// parse json_encoded response from php. this is required as we didn't tell $.get that we expect to get json
					// {"id":"28","page_id":"20","title":"fdsa","tagline":"fdsffds","description":"fdsa","target_pg":"1","image":"d1d3c92262fc555b604c7ac70b5e0d2f_555007896.png"}

					//console.log(itemdata.title);

					// set form data
					getPropertyList('<?=SITE_URL?>', $('#frmdownloads select[name=downloaditem-property]'), passedpropid, itemdata.property_id);
					$('#frmdownloads input[name=downloaditem-title]').val(itemdata.item_title);
					$('#frmdownloads select[name=downloaditem-property]').val(itemdata.property_id).change();
					$('#frmdownloads input[name=downloaditemsave]').val('Update Item!');
					
					// set the action
					//$('#frmdownloads div.promohiddenstuff').prepend('<input type="hidden" name="downloaditem-action" value="update"/>');
					$('#frmdownloads input[name=downloaditem-action]').val("update");

					// set download item id
					$('#frmdownloads div.downloadhiddenstuff').prepend('<input type="hidden" name="downloaditem-id" value="'+itemdata.id+'"/>');

				})

				$('#myDownloadModal').modal('show');	

			});

			// execute on model hide
			$('#myDownloadModal').on('hidden.bs.modal', function(e){

				// reset form
				$('#frmdownloads').trigger('reset');

				// clear property select list		
				$('#frmdownloads select[name=downloaditem-property]').html('');

				// hide (required) text items
				$('.requiredmsg').css('display','none');	// hide Required messages if visible

				if($('#frmdownloads input[name=downloaditem-id]').length)
				{
					// remove downloaditem-id hidden element
					$('#frmdownloads input[name=downloaditem-id]').remove();
					//navform.find('input[name=navitem-id]').remove();
				}

			});

			$('select[name=property-selector]').change(function(){
				console.log('property selector: '+ $(this).val());
				var prid = $(this).val();
				getDownloadItemData('<?=SITE_URL?>', prid);				
			})
			/*
			$('.promo-item-list').on('change', '#property-selector', function(){
				console.log('property selector: '+ $(this).val());
				var prid = $(this).val();
				getPromotionData('<?=SITE_URL?>', prid);
			})*/
			

	<?php
	break;

}
