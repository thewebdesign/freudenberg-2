<?php
require '../appdata/cms/bootstrap.php';

	$page = (int)$_GET['pgid'];
	$image = $_GET['image'];

	//list($imagename, $ext) = explode('.', $image);

	//$imageid = substr($imagename, -32);
	//$imageid = $imagename;

	$imagedata = $db->getRow('SELECT * FROM tblslides WHERE `image_name` = ?', array($image));


?>
<div class="image-detail-view white-popup">
	<div style="float: left; width: 330px;">
		<img src="<?=LIVE_SITE_URL.'/images/'.$imagedata->section_id.'/'.$page.'/'.$image?>" style="width: 100%;"/>
	</div>
	<div class="" style="margin-left: 330px; padding-left: 10px;">
		<form id="frmsliderimgdetailupdate" action="<?=SITE_URL.'/processimage.php'?>">
			<div class="field">
				<label for="alttext">Alt Tag Text <span style="font-size: 10px;">(Optional)</span></label><br>
				<input type="text" name="alttext" id="alttext" value="<?=$imagedata->alt_text?>"/>
			</div>

			<div class="field">
				<label for="desctext">Short Caption <span style="font-size: 10px;">(Optional)</span></label><br>
				<textarea name="desctext" id="desctext" rows="3" cols="50"><?=$imagedata->desc_text?></textarea>
			</div>

			<div class="field">
				<input type="hidden" name="imagename" value="<?=$imagedata->image_name?>"/>
				<input type="hidden" name="imageid" value="<?=$imagedata->id?>"/>
				<input type="submit" name="submit" data-action="update" value="Update"/>&nbsp;&nbsp;<input type="submit" name="submit" data-action="delete" value="Delete Image"/>
			</div>
		</form>
	</div>
	<br style="clear: left;"/>
</div>
