<?php
require '../appdata/cms/bootstrap.php';

	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{


		if($_POST['desc-id'] == 0)
		{

			// add new
			$fields = '`page_id`,`sub_title`,`main_title`,`body_text`';
			$values = array($_POST['desc-page-id'], $_POST['subtitle'], $_POST['maintitle'], $_POST['bodytext']);
			$placeholders = '?,?,?,?';
			$mode = 'insert';

			$qry = 'INSERT INTO tbldescriptions ('.$fields.') VALUES('.$placeholders.')';

			$res = $db->addRecord($qry, $values);

			if(!$res)
			{
				echo json_encode(array('action' => 'add', 'status' => false, 'msg' => print_r($db->getError(), true), 'qry' => $qry));
			}
			else
			{
				echo json_encode(array('action' => 'add', 'status' => true, 'msg' => 'insert success', 'id' => $res));
			}
		}
		else
		{
			// update
			$fields = '`sub_title`=?,`main_title`=?,`body_text`=?';
			$values = array($_POST['subtitle'], $_POST['maintitle'], $_POST['bodytext'], $_POST['desc-id']);
			$placeholders = '?,?,?,?';
			$mode = 'update';

			$qry = 'UPDATE tbldescriptions SET '.$fields.' WHERE `id` = ?';

			if(!$db->updateRecord($qry, $values))
			{
				echo json_encode(array('action' => 'update', 'status' => false, 'msg' => print_r($db->getError(), true), 'qry' => $qry));
			}
			else
			{
				echo json_encode(array('action' => 'update', 'status' => true, 'msg' => 'update success'));
			}
		}


	}