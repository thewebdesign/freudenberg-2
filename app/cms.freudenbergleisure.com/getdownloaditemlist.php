<?php
require '../appdata/cms/bootstrap.php';

	// get promo item list for $_GET['propertyid']
	$propid = (int)$_GET['propertyid'];

	if($propid == 1)
	{
		// get all promotions
		$downloaditemslist = $db->getRows('SELECT tbldownloaditems.*, tblproperties.property_slug, tblproperties.property_name FROM tbldownloaditems INNER JOIN tblproperties ON tbldownloaditems.property_id = tblproperties.id ORDER BY tbldownloaditems.id DESC', array());
	}
	else
	{
		$downloaditemslist = $db->getRows('SELECT tbldownloaditems.*, tblproperties.property_slug, tblproperties.property_name FROM tbldownloaditems INNER JOIN tblproperties ON tbldownloaditems.property_id = tblproperties.id WHERE tbldownloaditems.property_id = ? ORDER BY tbldownloaditems.id DESC', array($propid));
	}


	ob_start();
	?>
	<div class="downloaditems-list-container" style="margin-top: 20px;">
	<div class="downloaditems-list-body">
	<?php if($downloaditemslist): ?>
	<?php foreach($downloaditemslist as $listitem): ?>
	<div class="downloaditem-wrap" style="/*clear: left;*/ padding-bottom: 20px; padding-top: 20px; padding-left: 10px; border-bottom: 1px solid darkgray;">
		<div class="downloaditem-image" style="width: 200px; float: left;">
			<img src="<?=SITE_URL?>/assets/text_document_complete-512.png?>" style="width: 25%; /*height: 100%;*/"/>
		</div>
		<div class="downloaditem-details" style="float: left; margin-left: 20px;">
			<table style="width: 200px;">
				<tr>
					<td>Applies To:</td>
					<td><?=$listitem->property_name?></td>
				</tr>
				<tr>
					<td>Item Title:</td>
					<td><?=$listitem->item_title?></td>
				</tr>	
				<tr>
					<td>Item Source:</td>
					<td><a href="<?=LIVE_SITE_URL?>/files/<?=$listitem->property_slug?>/<?=$listitem->item_file?>" target="_blank">Click To Open</a></td>
				</tr>														
			</table>
			<p><a href="#" class="editdownloaditem" data-download-item-id="<?=$listitem->id?>">Edit</a>&nbsp;|&nbsp;<a href="#" class="deletedownloaditem" data-download-item-id="<?=$listitem->id?>">Delete</a></p>
		</div>	
		<br style="clear: left;"/>	
	</div>

	<?php
	endforeach;
	?>
	<?php else: ?>
		<p>There are no download items available!</p>
	<?php endif; ?>
	</div><!-- end downloaditems-list-body -->
	</div><!-- end downloaditems-list-container -->
	<?php
	$out = ob_get_clean();
	echo $out;
	exit;
	?>