<?php
require_once '../appdata/cms/bootstrap.php';

if(isset($_SESSION['USER']))
{
	// logged in
	$property = $db->getRow('SELECT property_slug FROM tblproperties WHERE id = ?', array($_SESSION['USER']->property_id));

	header('Location: '.SITE_URL . '/manage/'.$property->property_slug);
	exit;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	// POSTED

	// validate 
	$errors = false;
	$errmsg = [];


	if(!isset($_POST['username']) || empty($_POST['username']))
	{
		$errors = true;
		$errmsg[] = 'Please enter your user name';
	}

	if(!isset($_POST['password']) || empty($_POST['password']))
	{
		$errors = true;
		$errmsg[] = 'Please enter your password';
	}

	if(!$errors)
	{
		// no errors

		$res = $db->getRow('SELECT * FROM users WHERE `username` = ? AND `password` = ?', array($_POST['username'], md5($_POST['password'])));

		if(!$res->id)
		{
			// no match
			$errors = true;
			$errmsg[] = 'Invalid Login! Please try again!';
		}
		else
		{
			$_SESSION['USER'] = $res;

			if($res->access_level == 1)
			{
				// admin (manage all properties)
				header('Location: '.SITE_URL.'/manage/freudenberg/');
				exit;
			}
			else
			{
				// only a property admin
				switch($res->property_id)
				{
					case 1:
						header('Location: '.SITE_URL.'/manage/randholee/');
					break;

					case 2:
						header('Location: '.SITE_URL.'/manage/firs/');
					break;

					case 3:
						header('Location: '.SITE_URL.'/manage/ellens/');
					break;

				}
			}
			
		}
	}
}

require '../appdata/cms/templates/index.tpl';