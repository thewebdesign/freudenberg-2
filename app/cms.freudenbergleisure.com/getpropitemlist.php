<?php
require '../appdata/cms/bootstrap.php';

	// get navigation item list
	//$pgid = (int)$_GET['pgid'];

	$propdata = $db->getRows('SELECT * FROM tbl_home_property_data');

	//echo json_encode($navdata);


	ob_start();
	foreach($propdata as $propitem):

	?>
	<div class="propitem-wrap" style="/*clear: left;*/ padding-bottom: 20px; padding-top: 20px;">
		<div class="propitem-image" style="width: 200px; height: 200px; float: left;">
			<img src="<?=LIVE_SITE_URL.'/images/properties/'.$propitem->prop_image?>" style="width: 100%; /*height: 100%;*/"/>
		</div>
		<div class="propitem-details" style="float: left; margin-left: 20px;">
			<table style="width: 200px;">
				<tr>
					<td>Name:</td>
					<td><?=$propitem->prop_name?></td>
				</tr>	
				<tr>
					<td>Description:</td>
					<td><?=$propitem->prop_description?></td>
				</tr>	
				<tr>
					<td>Link:</td>
					<td><a href="<?=$propitem->prop_link?>" target="_blank"><?=$propitem->prop_link?></a></td>
				</tr>	
				<tr>
					<td>Open Link In:</td>
					<td><?php echo ($propitem->browser_window == '_blank' ? 'New Tab' : 'Current Tab') ?></td>
				</tr>													
			</table>
			<p><a href="#" class="editpropitem" data-prop-item-id="<?=$propitem->id?>">Edit item</a></p>
		</div>	
		<br style="clear: left;"/>	
	</div>

	<?php
	endforeach;
	$out = ob_get_clean();

	echo $out;
	exit;
	?>