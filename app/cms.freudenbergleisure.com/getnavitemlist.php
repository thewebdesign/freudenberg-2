<?php
require '../appdata/cms/bootstrap.php';

	// get navigation item list
	$pgid = (int)$_GET['pgid'];

	$navdata = $db->getRows('SELECT tblnavigation.*, tblpages.page_name FROM tblnavigation INNER JOIN tblpages ON tblnavigation.target_pg = tblpages.id WHERE tblnavigation.page_id = ?', array($pgid));

	//echo json_encode($navdata);


	ob_start();
	foreach($navdata as $navitem):

	?>
	<div class="navitem-wrap" style="/*clear: left;*/ padding-bottom: 20px; padding-top: 20px;">
		<div class="navitem-image" style="width: 200px; /*height: 200px;*/float: left;">
			<img src="<?=LIVE_SITE_URL.'/images/navigation/'.$navitem->page_id.'/'.$navitem->image?>" style="width: 100%; /*height: 100%;*/"/>
		</div>
		<div class="navitem-details" style="float: left; margin-left: 20px; width: 75%;">
			<div class="navitem-details-row">
				<?=$navitem->title?>
			</div>
			<div class="navitem-details-row">
				<?=$navitem->description?>
			</div>
			<div class="navitem-details-row">
				<?=$navitem->page_name?>
			</div>						
			<p><a href="#" class="editnavitem" data-nav-item-id="<?=$navitem->id?>">Edit item</a></p>
		</div>
		<br style="clear: left;"/>	
	</div>

	<?php
	endforeach;
	$out = ob_get_clean();

	echo $out;
	exit;
	?>