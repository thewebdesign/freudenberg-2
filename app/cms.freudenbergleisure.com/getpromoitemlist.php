<?php
require '../appdata/cms/bootstrap.php';

	// get promo item list for $_GET['propertyid']
	$propid = (int)$_GET['propertyid'];

	if($propid == 1)
	{
		// get all promotions
		$promoitemslist = $db->getRows('SELECT tblpromotions.*, tblproperties.property_slug, tblproperties.property_name FROM tblpromotions INNER JOIN tblproperties ON tblpromotions.property_id = tblproperties.id ORDER BY tblpromotions.id DESC', array());
	}
	else
	{
		$promoitemslist = $db->getRows('SELECT tblpromotions.*, tblpromotionpages.page_id, tblproperties.property_slug, tblproperties.property_name FROM tblpromotions INNER JOIN tblproperties ON tblpromotions.property_id = tblproperties.id LEFT JOIN tblpromotionpages ON tblpromotionpages.promotion_id = tblpromotions.id WHERE tblpromotions.property_id = ? ORDER BY tblpromotions.id DESC', array($propid));
	}

	$promopageids = [
			1 => 73,
			2 => 70,
			3 => 71,
			4 => 72
	];
	//echo json_encode($navdata);


	ob_start();
	?>
	<div class="promo-list-container" style="margin-top: 20px;">
	<div class="promo-list-body">
	<?php if($promoitemslist): ?>
	<?php foreach($promoitemslist as $listitem): ?>
	<div class="promoitem-wrap" style="/*clear: left;*/ padding-bottom: 20px; padding-top: 20px; padding-left: 10px;">
		<div class="promoitem-image" style="width: 200px; height: 200px; float: left;">
			<img src="<?=LIVE_SITE_URL.'/images/promotions/'.$listitem->property_slug.'/'.$listitem->image?>" style="width: 100%; /*height: 100%;*/"/>
		</div>
		<div class="promoitem-details" style="float: left; margin-left: 20px;">
			<table style="width: 200px;">
				<tr>
					<td>Applies To:</td>
					<td><?=$listitem->property_name?></td>
				</tr>
				<tr>
					<td>Title:</td>
					<td><?=strip_tags($listitem->title)?></td>
				</tr>	
				<tr>
					<td>Description:</td>
					<td><?=strip_tags($listitem->description)?></td>
				</tr>	
				<tr>
					<td>Trigger</td>
					<td>
						<?php
							switch($listitem->trigger_action)
							{
								case 'none':
									echo 'None';

								break;

								case 'page':
									$pgdata = $db->getRow('SELECT * FROM tblpromotionpages WHERE promotion_id = ?', array($listitem->id));

									if(!$pgdata->page_id)
									{
										// no page exists
										echo '<a href="'.SITE_URL.'/manage/'.$listitem->property_slug.'/addpage/'.base64_encode($promopageids[$listitem->property_id]).'/'.base64_encode($listitem->id).'">Create Page</a>';
									}
									else
									{
										// already has
										echo '<a href="'.SITE_URL.'/manage/'.$listitem->property_slug.'/edit/'.base64_encode($listitem->page_id).'">Update Page</a>';
									}							

								break;

								case 'image':
										$imagedata = $db->getRow('SELECT * FROM tblpromotionimages WHERE promotion_id = ?', array($listitem->id));
										if(!$imagedata->id)
										{
											echo '<a href="'.SITE_URL.'/manage_promo_image.php?promo='.$listitem->id.'" class="promo-item-image">Add Image</a>';
										}
										else
										{
											echo '<a href="'.SITE_URL.'/manage_promo_image.php?promo='.$listitem->id.'" class="promo-item-image">View &amp; Update Image</a>';
										}
										
								break;
							}
						?>						
					</td>
				</tr>	
				<tr>
					<td>Enabled:</td>
					<td><?=($listitem->enabled ? 'Yes' : 'No')?></td>
				</tr>													
			</table>
			<p><a href="#" class="editpromoitem" data-promo-item-id="<?=$listitem->id?>">Edit</a><!--&nbsp;|&nbsp;<a href="#" class="deletepromoitem" data-promo-item-id="<?=$listitem->id?>">Delete</a>--></p>
		</div>	
		<br style="clear: left;"/>	
	</div>

	<?php
	endforeach;
	?>
	<?php else: ?>
		<p>There are no promotions available!</p>
	<?php endif; ?>
	</div><!-- end promo-list-body -->
	</div><!-- end promo-list-container -->
	<?php
	$out = ob_get_clean();
	echo $out;
	exit;
	?>
