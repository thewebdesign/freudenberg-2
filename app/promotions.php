<?php 
$pg = ['property' => 'freudenberg', 'page' => 'promotions'];
include 'includes/header.php'; 
?>
    <style>
        .ctacard-wrapper {clear: both;overflow: hidden;padding-top: 0px !important;padding-bottom: 0px !important;}
        .ctacard-wrapper .ctacard-card:nth-child(2) {margin-left: 0% !important; margin-right: 0% !important; }
        .ctacard-wrapper .ctacard-card:nth-child(3) {margin-left: 5%; margin-right: 5% }
        #route{margin-left:21.4%}	




        #fade{
            display: none;
            position: fixed;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: #000;
            z-index:+111111;

            opacity:.80;
            filter: alpha(opacity=70);
        }
        #light{

            display: none;
            margin:0 auto;
            width: 50%;



        }

        .promo{
            position:fixed;

            z-index:+11111111;
            top: 75px;
            overflow:visible;
            -moz-animation: fadein 2s; /* Firefox */
            -webkit-animation: fadein 2s; /* Safari and Chrome */
            -o-animation: fadein 2s; /* Opera */
        }
        @keyframes fadein {
            from {
                opacity:0;
            }
            to {
                opacity:1;
            }
        }
        @-moz-keyframes fadein { /* Firefox */
            from {
                opacity:0;
            }
            to {
                opacity:1;
            }
        }
        @-webkit-keyframes fadein { /* Safari and Chrome */
            from {
                opacity:0;
            }
            to {
                opacity:1;
            }
        }
        @-o-keyframes fadein { /* Opera */
            from {
                opacity:0;
            }
            to {
                opacity: 1;
            }
        }
   


.tabs nav a {
    
    font-size: 15px !important;}

  

        .clo{
            position:fixed;
            z-index:1003;
            margin-top: 4%;
            margin-left: 33%;
            width: initial;

        }
.tabs nav #freuden.tab-current {
    background-color: rgb(231, 240, 246);
    border-top: solid 3px #1a97eb;
    border-left: solid 1px #388bc2;
    border-right: solid 1px #519bcd;
}
.tabs nav li.tab-current a {
    color: #171616;
}
.tabs nav #freuden.tab-current:before, .tabs nav #freuden.tab-current:after {
    background: #1a97eb;
}
@media screen 
  and (device-width: 360px) 
  and (device-height: 640px) 
  and (-webkit-device-pixel-ratio: 3) {



.tabs nav ul, .tabs nav ul li a {
    
    font-size: 7px !important;
}
.grid figure, .grid1 figure, .grid2 figure {
  
    margin: 5px 17% !important;
    min-width: 232px !important;
   
}
.tabs {
   
    margin-top: 240px !important;
}
.booking_room {
   
    top: 10.5em !important;
   
}
}

@media only screen 
  and (min-device-width: 320px) 
  and (max-device-width: 340px)
  and (-webkit-min-device-pixel-ratio: 2)
  and (orientation: portrait) {


.tabs nav a {
   
    font-size: 6px !important;}

    .grid figure, .grid1 figure, .grid2 figure {
  
    margin: 5px 11% ;
    min-width: 232px !important;
   
}
.tabs {
   
    margin-top: 235px !important;
}
.booking_room {
   
    top: 9.9em ;
   
}
}
@media only screen 
  and (min-device-width: 375px) 
  and (max-device-width: 667px) 
  and (-webkit-min-device-pixel-ratio: 2) { 
    .tabs nav a {
   
    font-size: 7px !important;}

    .grid figure, .grid1 figure, .grid2 figure {
  
    margin: 5px 19% !important;
    min-width: 232px !important;
   
}
.tabs {
   
    margin-top: 240px !important;
}
}
@media only screen 
  and (min-device-width: 768px) 
  and (max-device-width: 1024px) 
  and (-webkit-min-device-pixel-ratio: 1) {

    .tabs nav a {
    
   
    font-size: 12px !important;
    line-height: 2em !important;
    padding: 0px 1em !important;
    
}
.tabs {
   
    margin-top: 240px !important;
}
  }

@media only screen 
  and (min-device-width: 375px) 
  and (max-device-width: 667px) 
  and (-webkit-min-device-pixel-ratio: 2) { 
.booking_room {
    
    top: 11em;
   
}

}
@media only screen 
  and (min-device-width: 414px) 
  and (max-device-width: 736px) 
  and (-webkit-min-device-pixel-ratio: 3) {
 .booking_room {
    
    top: 12em;
   
}

}




    </style>

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">

            <h1 class="hide-visual">Freudenberg Leisure</h1>  
            <?php include 'includes/navigation.php'; ?> 

        </header><!--  #header  -->

        <?php include 'includes/booking.php'; ?> 

        <div class="blur">  

            <div class="node--page_basic mode--full">  
                <aside role="complementary">
                    <?php include 'includes/_slider.php'; ?>
                </aside>    

                <div id="route" style="margin-left: 21.4% !important;">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Special Offers</li>
                    </breadcrumb>
                </div>

                <aside role="complementary" id="main">

                    <div id="tabs" class="tabs">
                        <nav>
                            <ul style="list-style:none;">
                                <li id="freuden"><a href="#section-4">FREUDENBERG LEISURE<br/>PROMOTIONS</a></li>
                                <li id="randh2"><a href="#section-1">RANDHOLEE<br/>PROMOTIONS</a></li>
                                <li id="firs2"><a href="#section-2">THE FIRS<br/>PROMOTIONS</a></li>
                                <li id="ellens2"><a href="#section-3">ELLEN'S PLACE<br/>PROMOTIONS</a></li>
                            </ul>
                        </nav>

<!--====================FREUDENBERG================-->
                        <div class="content" style="background-color: rgba(206, 224, 237, 0.5);">  
                            <section id="section-4">
                                <div class="mediabox">
                                    <div class="grid">
                                        <div class="hdr-two hide ddd" style="text-align: left; padding: 10px; font-size:1.2em;">FREUDENBERG PROMOTIONS</div>
                                        <?php $promotions = $cms->getPromotions(1); ?>
                                        <?php if(count($promotions) > 0): ?>
                                        <?php foreach($promotions as $promo): ?>
                                           <figure class="effect-oscar">
                                              <img src="images/promotions/<?=$promo->property_slug?>/<?=$promo->image?>" alt=""/>
                                              <figcaption>
                                                <h2><?=$promo->title?></h2>
                                                <p><?=$promo->description?></p>
                                                <?php if(!empty($promo->trigger_action)): ?>
                                                    <?php if($promo->trigger_action == 'page'): ?>
                                                        <a href="promotions/<?=$cms->getPromoPage($promo->id)?>">View More</a>
                                                    <?php elseif($promo->trigger_action == 'image'): ?>
                                                        <a class="promo-pop" href="images/promotions/<?=$promo->property_slug?>/popups/<?=$cms->getPromoPopupImage($promo->id)?>">View more</a>
                                                    <?php else: ?>
                                                        <a href="#">View more</a>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                              </figcaption>
                                           </figure>                                
                                        <?php endforeach; ?>
                                        <?php else: ?>
                                          <div class="hdr-two" style="font-style: italic; margin-bottom: 15px;"> No promotions are currently available. </div>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </section>
                        </div>

                        <div class="content" style="background-color: rgba(253, 229, 229, 0.5);">  
                            <section id="section-1">
                                <div class="mediabox">
                                    <div class="grid">
                                        <div class="hdr-two hide ddd" style="text-align: left; padding: 10px; font-size:1.2em;">RANDHOLEE PROMOTIONS</div>
                                        <?php $promotions = $cms->getPromotions(2); ?>
                                        <?php if(count($promotions) > 0): ?>
                                        <?php foreach($promotions as $promo): ?>
                                           <figure class="effect-oscar">
                                              <img src="images/promotions/<?=$promo->property_slug?>/<?=$promo->image?>" alt=""/>
                                              <figcaption>
                                                <h2><?=$promo->title?></h2>
                                                <p><?=$promo->description?></p>
                                                <?php if(!empty($promo->trigger_action)): ?>
                                                    <?php if($promo->trigger_action == 'page'): ?>
                                                        <a href="<?=RANDHOLEE_DOMAIN.'promotions/'.$cms->getPromoPage($promo->id)?>">View More</a>
                                                    <?php elseif($promo->trigger_action == 'image'): ?>
                                                        <a class="promo-pop" href="images/promotions/<?=$promo->property_slug?>/popups/<?=$cms->getPromoPopupImage($promo->id)?>">View more</a>
                                                    <?php else: ?>
                                                        <a href="#">View more</a>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                              </figcaption>
                                           </figure>                                
                                        <?php endforeach; ?>
                                        <?php else: ?>
                                          <div class="hdr-two" style="font-style: italic; margin-bottom: 15px;"> No promotions are currently available. </div>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </section>
                        </div>


                        <div class="content" style="background-color: rgba(240, 249, 215, 0.5);">  
                            <section id="section-2">
                                <div class="mediabox">     
                                    <div class="grid1">
                                        <div class="hdr-two hide ddd" style="text-align: left; padding: 10px; font-size:1.2em;">THE FIRS PROMOTIONS</div>
                                        <?php $promotions = $cms->getPromotions(3); ?>
                                        <?php if(count($promotions) > 0): ?>
                                        <?php foreach($promotions as $promo): ?>
                                           <figure class="effect-oscar">
                                              <img src="images/promotions/<?=$promo->property_slug?>/<?=$promo->image?>" alt=""/>
                                              <figcaption>
                                                <h2><?=$promo->title?></h2>
                                                <p><?=$promo->description?></p>
                                                <?php if(!empty($promo->trigger_action)): ?>
                                                    <?php if($promo->trigger_action == 'page'): ?>
                                                        <a href="<?=FIRS_DOMAIN.'promotions/'.$cms->getPromoPage($promo->id)?>">View More</a>
                                                    <?php elseif($promo->trigger_action == 'image'): ?>
                                                        <a class="promo-pop" href="images/promotions/<?=$promo->property_slug?>/popups/<?=$cms->getPromoPopupImage($promo->id)?>">View more</a>
                                                    <?php else: ?>
                                                        <a href="#">View more</a>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                              </figcaption>
                                           </figure>                                
                                        <?php endforeach; ?>
                                        <?php else: ?>
                                          <div class="hdr-two" style="font-style: italic; margin-bottom: 15px;"> No promotions are currently available. </div>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </section>
                        </div> 


                        <div class="content" style="background-color: rgba(223, 239, 255, 0.5);">  
                            <section id="section-3">
                                <div class="mediabox">
                                    <div class="grid2">
                                        <div class="hdr-two hide ddd" style="text-align: left; padding: 10px; font-size:1.2em;">ELLEN'S PLACE PROMOTIONS</div>
                                        <?php $promotions = $cms->getPromotions(4); ?>
                                        <?php if(count($promotions) > 0): ?>
                                        <?php foreach($promotions as $promo): ?>
                                           <figure class="effect-oscar">
                                              <img src="images/promotions/<?=$promo->property_slug?>/<?=$promo->image?>" alt=""/>
                                              <figcaption>
                                                <h2><?=$promo->title?></h2>
                                                <p><?=$promo->description?></p>
                                                <?php if(!empty($promo->trigger_action)): ?>
                                                    <?php if($promo->trigger_action == 'page'): ?>
                                                        <a href="<?=ELLENS_DOMAIN.'promotions/'.$cms->getPromoPage($promo->id)?>">View More</a>
                                                    <?php elseif($promo->trigger_action == 'image'): ?>
                                                        <a class="promo-pop" href="images/promotions/<?=$promo->property_slug?>/popups/<?=$cms->getPromoPopupImage($promo->id)?>">View more</a>
                                                    <?php else: ?>
                                                        <a href="#">View more</a>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                              </figcaption>
                                           </figure>                                
                                        <?php endforeach; ?>
                                        <?php else: ?>
                                          <div class="hdr-two" style="font-style: italic; margin-bottom: 15px;"> No promotions are currently available. </div>
                                        <?php endif; ?>
                                    </div> 
                                </div>
                            </section>
                        </div>              
                </aside>
            </div>  

            <footer id="footer" role="contentinfo"> 

                <?php include 'trip-advisor.php'; ?>
                <?php /* ?>    <aside role="complementary">
                  <div class="ctatext-buildadventure ctatext-wrapper">
                  <div class="ctatext-text">
                  <h1 class="hdr-seven">Build your own Adventure</h1>
                  <div class="hdr-two">Your Perfect Getaway Awaits</div>
                  <p>Whatever your lifestyle or pace, Freudenberg Leisure offers something unique for everyone.</p>
                  <a class="btn-arrow" href="#">Book Now</a>
                  </div><!--  .ctatext-text  -->
                  </div><!--  .ctatext-wrapper  -->
                  </aside><?php */ ?>

                <?php include 'includes/footer.php'; ?>

                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                <script type="text/javascript" src="assets/js/jquery.magnific-popup.min.js"></script>

                <script>

                    jq191 = jQuery.noConflict( true );

                    jq191('.promo-pop').magnificPopup({
                        type: 'image'
                    })

                    function lightbox_open(elid) {
                        console.log(elid);
                        window.scrollBy(0, 0);
                        document.getElementById(elid).style.display = 'block';
                        document.getElementById('fade').style.display = 'block';

                    }

                    function lightbox_close() {
                        document.getElementById(elid).style.display = 'none';

                        document.getElementById('fade').style.display = 'none';
                    }


                </script>

                </body>

                </html>