<?php 
$sliderFlag = $cms->getSliderFlag(); 
$slider = ($sliderFlag == 1 ? $cms->getSlider(27) : $cms->getSlider());
?>
<section id="demo">
<h5 class="hide-visual">Freudenberg Leisure Slider</h5>
<div class="center">
<div class="smoothslides" id="myslideshow1">
<?php foreach($slider as $item): ?>
  <img src="images/slider/<?=$item->page_id?>/<?=$item->image_name?>" alt="<?=$item->alt_text?>" />
<?php endforeach; ?>
</div>
    <!--
    <div class="hidden-xs hidden-sm" style="position:absolute; margin-top: -42%; margin-left: 85%;border:0px;">
       <img src="assets/img/TT-bronze.png" alt="Bestweb logo" width="150" height="150" style="margin-top:20px" /> 
    </div>
    -->
</div>
</section>
<a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>