<?php

	// set destination text
	$destinations = array(
			1 => 'DESTINATIONS AT COLOMBO:',
			2 => 'DESTINATIONS AT KANDY:',
			3 => 'DESTINATIONS AT NUWARA ELIYA:'
		);

	// get list of items
	$experiences = $cms->getExperiences();

	//$destinations = array_unique($experiences->destination_id);

	$headerprinted = 0;
?>

<?php foreach($destinations as $dk => $dv): ?>
	<ul>
		<?php if($headerprinted != $dk): ?>
			<h1 class="hdr-seven promo_header">
			<?php
				$headerprinted = $dk;
				echo $dv;
			?>
			</h1>
		<?php endif; ?>
		<?php foreach($experiences as $ex): ?>
			<?php if($ex->destination_id == $dk): ?>
                <li class="highlight" style="background: #ebebeb url('images/experiences/<?=$ex->image?>') no-repeat 50% 50%; background-size: cover;">
                    <a class="highlight-hotspot" href="#"></a>
                    <div class="highlight-background"></div><!--  .highlight-background  -->
                    <div class="highlight-content">
                        <div class="highlight-content-inside">
                            <h2 class="hdr-four"><?=$ex->tagline?></h2>
                            <div class="twoo fadeitem"><?=$ex->description?></div>
                        </div><!--  .highlight-content-inside  -->
                    </div><!--  .highlight-content  -->
                </li><!--  .highlight  -->
            <?php endif; ?> 			
		<?php endforeach; ?>
	</ul>
	<div style="clear:both"></div>
<?php endforeach; ?>