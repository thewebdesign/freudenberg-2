<?php $gallery = $cms->getGallery(); ?>
<div class="slider-wrapper theme-default">
    <div id="slider" class="nivoSlider">
    	<?php foreach($gallery as $item): ?>
    		<img src="<?=PARENT_DOMAIN?>images/gallery/<?=$item->page_id?>/<?=$item->image_name?>">
    	<?php endforeach; ?>
    </div>
</div>