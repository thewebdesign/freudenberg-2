﻿<?php 
$pg = ['property' => 'freudenberg', 'page' => 'experience'];
include 'includes/header.php'; 
?>

    <style>
        .promo_header{margin-top:20px;}
    </style>

    <body class="node-type-accommodation-list">

        <header id="header" role="banner">
            <?php include 'includes/navigation.php'; ?> 

        </header><!--  #header  -->
        <?php include 'includes/booking.php'; ?> 

        <div style="clear:both"></div>

        <div class="blur">  
            <div class="node--page_basic mode--full">    
                <aside >
                    <!-- <header>
                    </header>     -->
                    <?php include 'includes/_slider.php'; ?>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Experiences</li>
                    </breadcrumb>
                </div>

                <div id="main" role="main">     
                    <article role="article" style="padding-top:10px;">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text pad_top">
                                <?php //require 'includes/showdescription.php'; ?>
                                
                                <div class="hdr-two">Experiences</div>
                                <p style="text-align:justify; font-size:17px;">Our three indulgence way outs pack a potent dose in terms of unique experiences won’t be anywhere else while enjoying the best hospitality and world class service. When you are staying in the dense, high-rise city of Colombo at Ellen’s Place you can visit Royal Colombo Golf club for a round of golf or enjoy panoramic views and wonderful photo opportunities, shopping and nightlife with a joyful mix of cultures and cuisines. One good way to experience the splendor of Kandy is to stay at the Randholee Resort, a great entry point to view Kandy Perehera, the Temple of the Tooth Relic, the Royal Botanical Gardens or the Hantane Mountain Range. Lastly, for a totally different experience to your urban adventure you must stay at Firs Bungalow in Nuwara Eliya to use the Victoria Golf Course, go horse riding, visit the Hakgala Botanical Gardens, hike on Horton Plains or take in the breathtaking views of World’s End.</p>
                                        
                            </div><!--  .ctatext-wrapper  -->
                        </div><!--  .ctatext-text  -->

<?php //echo "hello";die; ?>

                        <div class="activities-list highlight-panels">
                            <?php //require 'includes/getexperiences.php'; ?>
                        </div><!--  .highlight-panels  -->
                    </article>
                </div><!--  #main  -->
            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <?php include 'trip-advisor.php'; ?>         

            <?php /* ?>    <aside role="complementary">
              <div class="ctatext-buildadventure ctatext-wrapper">
              <div class="ctatext-text">
              <h1 class="hdr-seven">Build your own Adventure</h1>
              <div class="hdr-two">Your Perfect Getaway Awaits</div>
              <p>Facilities guaranteed to kindle pure relaxation and bliss, Whatever your lifestyle or pace, Randholee offers something unique for everyone.</p>
              <a class="btn-arrow" href="#">Book Now</a>
              </div><!--  .ctatext-text  -->
              </div><!--  .ctatext-wrapper  -->
              </aside><?php */ ?>

            <footer id="footer" role="contentinfo"> 
                <?php include 'includes/footer.php'; ?> 

                </body>
                </html>
