<?php
require_once('top.php');

if (isset($_POST['submit_fsaleisure']) && $_POST['site'] === 'fsaleisure') {
    $hotel_name = filter_var($_POST['hotels'], FILTER_SANITIZE_STRING);

    $arrival_date = filter_var($_POST['arrival_date'], FILTER_SANITIZE_STRING);
    list($arrival_year, $arrival_month, $arrival_day) = explode('-', $arrival_date);

    $departure_date = filter_var($_POST['departure_date'], FILTER_SANITIZE_STRING);
    list($departure_year, $departure_month, $departure_day) = explode('-', $departure_date);

    if (isset($hotel_name) && $hotel_name !== '-1'):
        if ($hotel_name === 'randholee'):
            $submit_URL = 'https://www.booking.com/hotel/lk/randholee-luxury-resort-kandy.en-gb.html?';
        elseif ($hotel_name === 'firs'):
            $submit_URL = 'https://www.booking.com/hotel/lk/the-firs.en-gb.html?';
        elseif ($hotel_name === 'ellens'):
            $submit_URL = 'https://www.booking.com/hotel/lk/ellen-39-s-place.en-gb.html?';
        endif;
    endif;

    $parameters = 'aid=330843&checkin_monthday=' . $arrival_day . '&checkin_year_month=' . $arrival_year . '-' . $arrival_month . '&checkout_monthday=' . $departure_day . '&checkout_year_month=' . $departure_year . '-' . $departure_month . '&dist=0&sb_price_type=total&type=total&';

    // echo "<script data-cfasync='false' > window.location.replace('" . $submit_URL . $parameters . "') </script>";
    header("Location:{$submit_URL}{$parameters}");
    unset($_POST);
    unset($submit_URL);
    unset($parameters);
    exit;
}
?>