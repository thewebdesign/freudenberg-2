<div class="tab">
  <button class="tablinks" onclick="openTab(event, 'Description')">Description</button>
  <button class="tablinks" onclick="openTab(event, 'Navigation')">Navigation Panel</button>
  <button class="tablinks" onclick="openTab(event, 'Properties')">Properties</button>
</div>

<div id="Description" class="tabcontent">
  <h1>Edit <?php echo (isset($parentpg->page_name) ? $parentpg->page_name . ' -> ' : '') . $pagedata->page_name; ?> Page Description <span id="actionfeedback" style=""></span></h1>
  <form id="frmpgdesc" action="<?=SITE_URL.'/updatedesc.php'?>">
  	 <div class="field">
  	 	<label for="subtitle">Sub Title</label><br>
  	 	<input type="text" name="subtitle" id="subtitle" value=""/>
  	 </div>
  	 <div class="field">
  	 	<label for="maintitle">Main Title <span class="requiredmsg">(Required)</span></label><br>
  	 	<input type="text" name="maintitle" id="maintitle" value=""/>
  	 </div>
  	 <div class="field">
  	 	<label for="bodytext">Body Text <span class="requiredmsg">(Required)</span></label><br>
  	 	<textarea name="bodytext" id="bodytext" rows="10" cols="75"></textarea>
  	 </div>
  	 <div class="field">
  	 	<input type="hidden" name="desc-id" value=""/>
  	 	<input type="hidden" name="desc-page-id" value=""/>
  	 	<input type="submit" id="submit" name="submit"/>
  	 </div>
  </form>
</div>

<div id="Navigation" class="tabcontent" style="/*height: 100%;*/">
  <h1>Manage <?=$pagedata->page_name?> Page Navigation</h1>

  <div class="navlayout" style="/*height: 100%;*/padding-top: 50px;">
	 	<div class="3imagenav">
	 		<div class="" style="border: 1px dotted red; padding: 15px; background: rgba(197, 197, 2, 0.23);">
	 			<p><a href="#" id="popme">+ Add New Item</a></p>
	 		</div>
		</div>
		<div class="nav-item-list-container">
			<h3>Navigation Items</h3>
			<div class="nav-item-list">
			</div>
		</div>
  </div>
</div>

<div id="Slider" class="tabcontent">
  <h1>Manage <?php echo (isset($parentpg->page_name) ? $parentpg->page_name . ' -> ' : '') . $pagedata->page_name; ?> Slider Images</h1>

</div>

<div id="Gallery" class="tabcontent">
  <h1>Manage <?php echo (isset($parentpg->page_name) ? $parentpg->page_name . ' -> ' : '') . $pagedata->page_name; ?> Slider Images</h1>

</div>

<div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" style="height: 420px; padding: 10px;">
				<form id="frmnavigation" method="post" action="<?=SITE_URL.'/ajax.php'?>" enctype="multipart/form-data">
	  	   			<div style="float: left; width: 500px;">
  						<h3 style="margin-top: 0px;">Navigation Item 1</h3>
  						<div class="field" style="float: left; margin-right: 80px;">
  							<label for="navitem-name">Name <span class="requiredmsg">(Required)</span></label><br>
   							<input type="text" name="navitem-name" id="navitem-name" value=""/>
   						</div>
   						<div class="field" style="margin-top: 10px;">
  							<label for="navitem-target">Target Page <span class="requiredmsg">(Required)</span></label><br>
  							<select name="navitem-target">
  								<option value="">Select Page</option>
  								<option value="1">Page 2</option>
  								<option value="2">Page 3</option>
  							</select>
  						</div>					   				
   						<div class="field" style="margin-top: 10px; clear: left;">
  							<label for="navitem-tagline">Tagline <span class="requiredmsg">(Required)</span></label><br>
  							<input type="text" style="width: 374px;" name="navitem-tagline" id="navitem-tagline" value=""/>
  						</div>
			  				
  						<div class="field" style="margin-top: 10px;">
  							<label for="navitem-desc">Description <span class="requiredmsg">(Required)</span></label><br>
  							<textarea name="navitem-desc" id="navitem-desc" rows="5" cols="50"></textarea>
  						</div>
  						<div class="field" style="">
  							<label for="navitem-image">Select Image <span class="requiredmsg">(Required)</span></label><br>
  							<input type="file" id="navitem-image" name="navitem-image" data-preview-element="nav-image-preview" onchange="preview_image(this);"/>
  						</div>
  						<div class="field hiddenstuff" style="margin-top: 20px;">
  							<input type="hidden" name="navitem-page-id" value="<?=$pagedata->id?>"/>
  							<input type="hidden" name="navitem-action" value=""/>
  							<!--<button type="button" id="navitemupload" name="navitemupload">Add Item</button>-->
  							<input type="submit" name="navitemsave" id="navitemsave" value="Add Item!"/>
  						</div>
					</div>
					<div class="nav-image-preview-area" style="float: right; width: 265px; height: 336px; overflow: hidden; margin-right: 70px; margin-top: 6px; text-align: center; border-left: 1px solid darkgrey; padding-left: 10px;">
						<h3>Image Preview</h3>
						<div id="nav-image-preview">
							<p>Image not selected!</p>
						</div>
					</div>
				</form>
		</div>
	</div>
</div>

<!--
<div id="myModal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      ...
    </div>
  </div>
</div>-->

<!--
<div id="Tokyo" class="tabcontent">
  <h3>Tokyo</h3>
  <p>Tokyo is the capital of Japan.</p>
</div>-->