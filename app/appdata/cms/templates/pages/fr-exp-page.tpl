<div class="tab">
  <button class="tablinks" onclick="openTab(event, 'Description')">Page Description</button>
  <button class="tablinks" onclick="openTab(event, 'Experiences')">Manage Experiences</button>
  <button class="tablinks" style="float: right; color: white; background: green;" onclick="window.location = '<?=SITE_URL.'/manage/'.$property_slug.'/addpage/'.urlencode(base64_encode($pageid))?>'">+ Add Sub Page</button>  
</div>

<div id="Description" class="tabcontent active">
  <h1>Edit <?=$pagedata->page_name?> Page Description</h1>
  <form id="desc">
  	<table>
  		<tr>
  			<td><label for="subtitle">Sub Title</label></td>
  			<td><input type="text" name="subtitle" id="subtitle" value=""/></td>
  		</tr>
  		<tr>
   			<td><label for="maintitle">Main Title</label></td>
  			<td><input type="text" name="maintitle" id="maintitle" value=""/></td> 		
  		</tr>
  		<tr>
   			<td><label for="bodytext">Body Text</label></td>
  			<td><textarea name="bodytext" id="bodytext" rows="10" cols="75"></textarea></td> 		
  		</tr> 
  		<tr>
  			<td></td>
  			<td><button type="button" id="updbtn" name="updbtn">Update</button></td>
  		</tr> 		
  	</table>
  </form>
</div>

<div id="Experiences" class="tabcontent" style="height: 100%">
  <h1>Manage <?=$pagedata->page_name?> Page Image Gallery</h1>
  <div id="dropzone">
  	<form action="/file-upload" class="dropzone" id="my-awesome-dropzone"></form>
  </div>
  <div class="gallery">
  	<div class="gallery-img">
  		<img src="https://placehold.it/150x150"/>
  		<br>
  		<span><a href="#">Edit Details</a></span>
  	</div>
  	<div class="gallery-img">
  		<img src="https://placehold.it/150x150"/>
  		<br>
  		<span><a href="#">Edit Details</a></span>  		
  	</div>
  	<div class="gallery-img">
  		<img src="https://placehold.it/150x150"/>
  		<br>
  		<span><a href="#">Edit Details</a></span>  		
  	</div>
  	<div class="gallery-img">
  		<img src="https://placehold.it/150x150"/>
  		<br>
  		<span><a href="#">Edit Details</a></span>  		
  	</div>
  	<div class="gallery-img">
  		<img src="https://placehold.it/150x150"/>
  		<br>
  		<span><a href="#">Edit Details</a></span>  		
  	</div>  
  	<div class="gallery-img">
  		<img src="https://placehold.it/150x150"/>
  		<br>
  		<span><a href="#">Edit Details</a></span>  		
  	</div>  		  	  	  	
  </div>
</div>