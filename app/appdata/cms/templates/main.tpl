<!DOCTYPE html>
<html lang="en">
<head>
	<title><?=$pageTitle?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<!-- Bootstrap 3.3.7 -->
	<link href="<?=SITE_URL.'/assets/bootstrap-3.3.7-dist/css/bootstrap.min.css'?>" rel="stylesheet" type="text/css"/>
	
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

	<!-- for rich text editor 
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
	<link rel="stylesheet" href="<?=SITE_URL?>/assets/froala_editor_2.5.1/css/froala_editor.pkgd.min.css">
	<link rel="stylesheet" href="<?=SITE_URL?>/assets/froala_editor_2.5.1/css/froala_style.min.css"> -->

	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	  
	<!-- Theme style -->
	<link rel="stylesheet" href="<?=SITE_URL?>/assets/AdminLTE-2.3.11/dist/css/AdminLTE.min.css">

	<!-- AdminLTE Skin -->
	<link rel="stylesheet" href="<?=SITE_URL?>/assets/AdminLTE-2.3.11/dist/css/skins/skin-blue.min.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->	

	<link href="<?=SITE_URL.'/assets/tabs.css'?>" rel="stylesheet" type="text/css"/>
	<link href="<?=SITE_URL.'/assets/dropzone.css'?>" rel="stylesheet" type="text/css"/>
	<link href="<?=SITE_URL.'/assets/fr-home-image-panel.css'?>" rel="stylesheet" type="text/css"/>
	<!--<link href="<?=SITE_URL.'/assets/tabs-1.css'?>" rel="stylesheet" type="text/css"/>-->

	<link href="<?=SITE_URL.'/assets/dropzone-overrides.css'?>" rel="stylesheet" type="text/css"/>

		  <!--for the uploadify-->
	<link rel="stylesheet" type="text/css" href="<?=SITE_URL.'/assets/jquery.ad-gallery.css'?>">
  	<link href="<?=SITE_URL.'/assets/default.css'?>" rel="stylesheet" type="text/css" />
  	<link href="<?=SITE_URL.'/assets/uploadify.css'?>" rel="stylesheet" type="text/css" />

  	<!-- magnific popup css -->
  	<link rel="stylesheet" type="text/css" href="<?=SITE_URL.'/assets/magnific-popup.css'?>"/>


	<style type="text/css">
		ul.userdata{
			list-style: none;
		}

		ul.userdata li{
			display: inline;
		}

		/*
		.sidebar {
			width: 25%;
			float: left;
		}

		.contents-with-sidebar{
			width: 75%;
			float: right;
		}*/

		.gallery-img {
			float: left;
		}

		.requiredmsg, .errnotify {
			color: red;
			/*font-size: 10px;*/
			display: none;
		}

		.actionfeedback {
			font-size: 10px;
			padding: 5px;
		}

		.success {
			color: white;
			background: green;
		}

		.failed {
			color: white;
			background: red;
		}

		.hidden{
			visibility: hidden;
		}

		.navitem-details table, .propitem-details table, .expitem-details table, .promoitem-details table, .downloaditem-details table {
			width: 500px !important;
		}

		.navitem-details table tr td, .propitem-details table tr td, .expitem-details table tr td, .promoitem-details table tr td, .downloaditem-details table tr td {
			/*width: 140px;*/
			padding-bottom: 10px;
			padding-right: 20px;
			width: 100px;
		}

		.promoitem-wrap:hover{
			background: rgba(60, 141, 188, 0.12);
		}

		.slider-item {
			list-style: none;
			float: left;
			margin: 0 5px 5px 0;
		}

		.white-popup {
		  position: relative;
		  background: #FFF;
		  padding: 20px;
		  width:auto;
		  max-width: 723px;
		  margin: 20px auto;
		}

		.field {
			margin-bottom: 20px;
		}

		.singletabdisplay{
			padding: 6px 12px;
    		border: 1px solid #ccc;
    		border-top: none;
		}

		form#frmpromotion fieldset {
			display: none;
		}

		.navitem-details-row {
			margin-bottom: 5px;
		}

		td {
			vertical-align: top;
		}

		
		div.fr-inline {
			/*width: 100%;*/
			padding: 6px 12px;
			font-size: 14px;
			line-height: 1.42857143;
			color: #555;
			background-color: #fff;
			background-image: none;
			border: 1px solid #ccc;
			transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
		}

	</style>


</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
	  <!-- Main Header -->
	  <header class="main-header">

	    <!-- Logo -->
	    <a href="index2.html" class="logo">
	      <!-- mini logo for sidebar mini 50x50 pixels -->
	      <span class="logo-mini"><b>A</b>LT</span>
	      <!-- logo for regular state and mobile devices -->
	      <span class="logo-lg"><b>Admin</b>LTE</span>
	    </a>

	    <!-- Header Navbar -->
	    <nav class="navbar navbar-static-top" role="navigation">
	      <!-- Sidebar toggle button-->
	      <!--
	      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
	        <span class="sr-only">Toggle navigation</span>
	      </a>-->
	      <!-- Navbar Right Menu -->
	      <div class="navbar-custom-menu">
	        <ul class="nav navbar-nav">
	          <!-- User Account Menu -->
	          <li class="dropdown user user-menu">
	            <!-- Menu Toggle Button -->
	            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	              <!-- The user image in the navbar-->
	              <img src="<?=SITE_URL?>/assets/AdminLTE-2.3.11/dist/img/avatar5.png" class="user-image" alt="User Image">
	              <!-- hidden-xs hides the username on small devices so only the image appears. -->
	              <span class="hidden-xs"><?=$_SESSION['USER']->real_name?></span>
	            </a>
	            <ul class="dropdown-menu">
	              <!-- The user image in the menu -->
	              <li class="user-header">
	                <img src="<?=SITE_URL?>/assets/AdminLTE-2.3.11/dist/img/avatar5.png" class="img-circle" alt="User Image">

	                <p>
	                  <?=$_SESSION['USER']->real_name?> - <?=$propertydata->property_name?>
	                  <small>Member since Apr. 2017</small>
	                </p>
	              </li>
	              <!-- Menu Body -->

	              <!-- Menu Footer-->
	              <li class="user-footer">
	                <div class="pull-right">
	                  <a href="<?=SITE_URL.'/logout.php'?>" class="btn btn-default btn-flat">Sign out</a>
	                </div>
	              </li>
	            </ul>
	          </li>
	        </ul>
	      </div>
	    </nav>
	    
	  </header>

	<!--
		<div class="header">
			<ul class="userdata" style="float: right;">
				<li><?=$_SESSION['USER']->real_name?></li>
				<li>|</li>
				<li><?=$propertydata->property_name?></li>
				<li>|</li>
				<li><a href="<?=SITE_URL.'/logout.php'?>">Logout</a></li>
			</ul>
		</div>
	
		<br style="clear: right;">
	-->
      <?php if($_GET['section'] != 'xxxx'): ?>
	  <!-- Left side column. contains the logo and sidebar -->
	  <aside class="main-sidebar">

	    <!-- sidebar: style can be found in sidebar.less -->
	    <section class="sidebar">

	      <!-- Sidebar user panel (optional) -->
	      <div class="user-panel">
	        <div class="pull-left image">
	          <img src="<?=SITE_URL?>/assets/AdminLTE-2.3.11/dist/img/avatar5.png" class="img-circle" alt="User Image">
	        </div>
	        <div class="pull-left info">
	          <p><?=$_SESSION['USER']->real_name?></p>
	          <!-- Status -->
	          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
	        </div>
	      </div>

	      <!-- search form (Optional) -->
	      <!--
	      <form action="#" method="get" class="sidebar-form">
	        <div class="input-group">
	          <input type="text" name="q" class="form-control" placeholder="Search...">
	              <span class="input-group-btn">
	                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
	                </button>
	              </span>
	        </div>
	      </form>
	      -->
	      <!-- /.search form -->

	      <!-- Sidebar Menu -->
			<ul class="sidebar-menu">	
				<li class="header">ADMIN SECTIONS</li>
				<li <?=(!isset($_GET['action']) ? 'class="active"' : '')?>><a href="<?=SITE_URL.'/manage/'.$_GET['property'].'/'?>"><i class="fa fa-dashboard"></i> <span>DASHBOARD</span></a></li>
				<?php //if($_GET['property'] == 'freudenberg'): ?>
				<li <?=(in_array($pagedata->id, array(27,42,37,38)) ? 'class="active"' : '' )?>><a href="<?=SITE_URL.'/manage/'.$_GET['property'].'/edit/'.($propertydata->id == 1 ? urlencode(base64_encode(27)) : ($propertydata->id == 2 ? urlencode(base64_encode(42)) : ($propertydata->id == 3 ? urlencode(base64_encode(37)) : ($propertydata->id == 4 ? urlencode(base64_encode(38)) : 0))))?>"><i class="fa fa-photo"></i> <span>MAIN SLIDER</span></a></li>
				<?php //endif; ?>
				<li <?=($pagedata->id == 29 ? 'class="active"' : '' )?>><a href="<?=SITE_URL.'/manage/'.$_GET['property'].'/edit/'.urlencode(base64_encode(29))?>"><i class="fa fa-bullhorn"></i> <span>PROMOTIONS</span></a></li>
				<li <?=(in_array($pagedata->id, array(28,41,34,35)) ? 'class="active"' : '' )?>><a href="<?=SITE_URL.'/manage/'.$_GET['property'].'/edit/'.($propertydata->id == 1 ? urlencode(base64_encode(28)) : ($propertydata->id == 2 ? urlencode(base64_encode(41)) : ($propertydata->id == 3 ? urlencode(base64_encode(34)) : ($propertydata->id == 4 ? urlencode(base64_encode(35)) : 0))))?>"><i class="fa fa-th-large"></i> <span>GALLERY</span></a></li>
				<?php if($_GET['property'] == 'freudenberg'): ?>
				<li <?=($pagedata->id == 69 ? 'class="active"' : '' )?>><a href="<?=SITE_URL.'/manage/'.$_GET['property'].'/edit/'.urlencode(base64_encode(69))?>"><i class="fa fa-bullhorn"></i> <span>DOWNLOADS</span></a></li>
				<!--<li <?=(isset($_GET['action']) && $_GET['action'] == 'menu' ? 'class="active"' : '')?>><a href="<?=SITE_URL.'/manage/'.$_GET['property'].'/menu';?>"><i class="fa fa-bars"></i> <span>MANAGE MENU</span></a></li>-->
				<?php endif; ?>				
				<!--<li><a href=""><i class="fa fa-bars"></i> <span>MENU</span></a></li>-->
				<li class="header">MANAGE PAGES</li>
				<li <?=(isset($_GET['action']) && $_GET['action'] == 'addpage' ? 'class="active"' : '')?>><a href="<?=SITE_URL.'/manage/'.$_GET['property'].'/addpage';?>"><i class="fa fa-plus-square"></i> <span>ADD NEW PAGES</span></a></li>
				<?php foreach($pages as $pg): ?>
					<?php if(!in_array($pg->id, array(70,71,72,73))): ?> <!-- this is to block promotion pages from appearing -->
					<?php if(isset($submenuitems[$pg->id])): ?>
						<li class="treeview <?=($pagedata->child_of == $pg->id ? 'active' : '')?>">
							<a href="#"><i class="fa fa-list"></i> <span><?=$pg->page_name?> <i class="fa fa-edit" class="edit" onclick="javascript:window.location='<?=SITE_URL."/manage/".$_GET['property']."/edit/".urlencode(base64_encode($pg->id))?>';"></i></span>
								<span class="pull-right-container">
									<i class="fa fa-caret-down pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
							<?php foreach($submenuitems[$pg->id] as $subpg): ?>
								<li <?=($pagedata->id == $subpg->id ? 'class="active"' : '' )?>><a href="<?=SITE_URL.'/manage/'.$_GET['property'].'/edit/'.urlencode(base64_encode($subpg->id))?>"><?=$subpg->page_name?></a></li>
							<?php endforeach; ?>
							</ul>
						</li>
					<?php else: ?>
						<li <?=($pagedata->id == $pg->id ? 'class="active"' : '' )?>><a href="<?=SITE_URL.'/manage/'.$_GET['property'].'/edit/'.urlencode(base64_encode($pg->id))?>"><?=$pg->page_name?></a></li>
					<?php endif; ?>
					<?php endif; ?> <!-- this is to block promotion pages from being appearing -->
				<?php endforeach; ?>
			</ul>
	      <!-- /.sidebar-menu -->
	    </section>
	    <!-- /.sidebar -->
	  </aside>
	<?php endif; ?>
	<div class="content-wrapper" style="min-height: 750px;">
		<?php require $template; ?>
	</div>

	</div>

	<script type="text/javascript" src="<?=SITE_URL.'/assets/jquery-3.2.0.min.js'?>"></script>
	<!--<script type="text/javascript" src="<?=SITE_URL.'/assets/jquery.form.js'?>"></script>-->
	<!--<script type="text/javascript" src="<?=SITE_URL.'/assets/tabs-1.js'?>"></script>-->

	<script type="text/javascript" src="<?=SITE_URL.'/assets/jquery.ad-gallery.js?rand=995'?>"></script>
	<script type="text/javascript" src="<?=SITE_URL.'/assets/swfobject.js'?>"></script>
  	<script type="text/javascript" src="<?=SITE_URL.'/assets/jquery.uploadify.v2.1.0.min.js'?>"></script>

  	<!-- magnific popup js -->
	<script type="text/javascript" src="<?=SITE_URL.'/assets/jquery.magnific-popup.min.js'?>"></script>


	<!-- moved into loadscript.php
  	<script type="text/javascript">


  		$('#QueueUploadStatus').hide();
  		$('#QueueUploadStatus1').hide();
  		var filecount = 0;
  		var totalfiles = 0;
  		var uploaded = 0;
		//uploadify:
		$("#uploadify").uploadify({
			'uploader'       : '<?=SITE_URL.'/assets/uploadify.swf'?>',
			'script'         : '<?=SITE_URL.'/uploadify.php'?>',
			'cancelImg'      : '<?=SITE_URL.'/assets/cancel.png'?>',
			'folder'         : '<?=$pagedata->id?>',
			'queueID'        : 'fileQueue',
			'auto'           : true,
			'multi'          : true,
			'queueSizeLimit' : 5,
			'fileDesc'		 : 'jpg, gif',
			'fileExt'		 : '*.jpg;*.gif',
			'sizeLimit'      : '512000',//max size bytes - 500kb
			'checkScript'    : '<?=SITE_URL.'/check.php'?>', //if we take this out, it will never replace files, otherwise asks if we want to replace
			'onAllComplete'  : function() {
								//$('#switch-effect').unbind('change');
								//$('#toggle-slideshow').unbind('click');
								//galleries[0].slideshow.stop();
								//start();
								//$('#fileQueueCounter').html('<span style="color: green;">Finished Uploading '+totalfiles+' Files!</span>');
								$('#QueueUploadStatus').hide();

								$('#QueueUploadStatus1').html('Finished Uploading '+totalfiles+' Files!').show('fast').fadeOut(3000);

								//$('#fileQueue').html('');
								
								filecount = 0;
								totalfiles = 0;
								uploaded = 0;

								},
			onSelect       	 : function() {

								filecount++;
								totalfiles = filecount;

								//console.log(filecount);
								//$('#fileQueueCounter').html(totalfiles+' Files Selected');

								},

			onCancel       	 : function() {

								filecount--;
								totalfiles = filecount;
								$('#fileQueueCounter').html(totalfiles+' Files Selected');

								if(totalfiles == 0)
								{
									$('#fileQueueCounter').html('');
								}

								},

			onComplete     	 : function() {

									filecount--;
									outof = totalfiles - filecount;
									$('#fileQueueRunningCounter').html(outof);
									$('#fileQueueTotal').html(totalfiles);
									$('#QueueUploadStatus').show();

									// call populate()
									populatePgSlider('<?=SITE_URL?>', <?=$pagedata->id?>);
								}					
		});  	
  	</script>
  	-->

	<!--<script type="text/javascript" src="<?=SITE_URL.'/assets/dropzone.js'?>"></script>-->
	<script type="text/javascript" src="<?=SITE_URL.'/assets/imageuploader.js'?>"></script>

	<script type="text/javascript" src="<?=SITE_URL.'/assets/bootstrap-3.3.7-dist/js/bootstrap.min.js'?>"></script>
	
	<!-- AdminLTE App -->
	<script src="<?=SITE_URL?>/assets/AdminLTE-2.3.11/dist/js/app.js"></script>

	<!-- For Rich Text Editor 
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>	
    <script type="text/javascript" src="<?=SITE_URL?>/assets/froala_editor_2.5.1/js/froala_editor.pkgd.min.js"></script>
    <script type="text/javascript" src="<?=SITE_URL?>/assets/froala_editor_2.5.1/js/plugins/inline_style.min.js"></script> -->


	<!--<script type="text/javascript" src="<?=SITE_URL.'/assets/nav_section.php?pg='.$pagedata->id;?>"></script>-->

	<!--<script type="text/javascript" src="<?=SITE_URL.'/assets/tabcontrol.js'?>"></script>-->

    <!-- TinyMCE -->
    <script type="text/javascript" src="<?=SITE_URL?>/assets/tinymce_4.5.6/tinymce/js/tinymce/tinymce.min.js"></script>

    <script type="text/javascript">
    	tinymce.init({ 
    		selector:'textarea#bodytext',
    		height : 450,
    		menubar: false,
    		plugins: "pagebreak code",
    		toolbar: "pagebreak bold italic underline strikethrough alignleft aligncenter alignright alignjustify fontselect fontsizeselect cut copy paste outdent indent blockquote subscript superscript hr bullist numlist link unlink openlink charmap table code",
    		pagebreak_separator: "<!-- section_separator -->",
    		pagebreak_split_block: true
    		//toolbar: 'fontselect fontsizeselect cut copy paste hr bullist numlist link unlink openlink charmap table'
    	});

    </script>

    <!-- this depends on tinymce -->
	<script type="text/javascript" src="<?=SITE_URL.'/assets/functions.js'?>"></script>

	<!-- for freudenberg leisure property details form -->
	<?php if($propertydata->id == 1 && isset($errfields)): ?>
	<script type="text/javascript">
	<?php foreach($errfields as $fieldkey => $fieldval): ?>
	<?php $fieldtype = ($fieldkey == 'address' ? 'textarea' : 'input'); ?>
		$('#frmpropertydetails <?=$fieldtype?>[name=<?=$fieldkey?>]').siblings('label').children('.errnotify').html('(<?=(!empty($fieldval) ? $fieldval : 'Required')?>)').css('display','inline');
	<?php endforeach; ?>
	</script>
	<?php endif; ?>


	<script type="text/javascript">

		var alreadyincluded = false;
		//var scriptname;

		function openTab(evt, tabName, el) {

			//console.log(el);
			var extra = typeof el == 'object' && el.hasAttribute('data-extra') ? el.getAttribute('data-extra') : el;
			//var extra = el.getAttribute('data-extra');
			//console.log(currentelement);

			//var alreadyincluded = false;
			// remove #loadscriptjs if exists
			
			if($('#'+ tabName.toLowerCase() +'-js').length)
			{

				alreadyincluded = true;
				console.log(tabName + 'js already loaded');

				// remove previous scripts - this doesn't make sense. doesn't matter to remove it from the dom. the script will still exeucte
					// in the memory
				/*
				console.log('got script :'+ scriptname);
				scriptname = scriptname + '-js';

				console.log('now script is :'+scriptname);

				if(scriptname != tabName.toLowerCase() + '-js')
				{
					$('#'+scriptname).remove();
				}*/

			}

			//console.log(alreadyincluded);

			if(!alreadyincluded)
			{
			    // create js element
			    var jscript = document.createElement('script');

			    jscript.type = 'application/javascript';

			    jscript.id = tabName.toLowerCase() + '-js';

			    jscript.src = '<?=SITE_URL?>/loadscript.php?tab='+tabName+'&pg='+<?=$pagedata->id?>+'&data='+extra;

			    document.body.appendChild(jscript);		

			    //scriptname = tabName.toLowerCase() + '-js';		// now loaded script. 
			    //console.log('script: '+scriptname);
			}

			// perform init tasks for the tab on each tab click
			switch(tabName)
			{
				case 'Description':

					populatePgDesc('<?=SITE_URL?>', <?=$pagedata->id?>);

					/*
					$.get('<?=SITE_URL.'/getdata.php'?>', { pgid: <?=$pagedata->id?> }, function(res){

						//$('.nav-item-list').html(navitemlist);
						//console.log(navitemlist);
						data = JSON.parse(res);
						// populate form here
						$('#frmpgdesc input[name=subtitle]').val(data.sub_title);
						$('#frmpgdesc input[name=maintitle]').val(data.main_title);
						$('#frmpgdesc textarea[name=bodytext]').val(data.body_text);
						$('#frmpgdesc input[name=desc-id]').val(isNaN(data.id) ? 0 : data.id);
						$('#frmpgdesc input[name=desc-page-id]').val(<?=$pagedata->id?>);
					});*/


				break;

				case 'Navigation':

					populatePgNav('<?=SITE_URL?>', <?=$pagedata->id?>);

					/*
					$.get('<?=SITE_URL.'/getnavitemlist.php'?>', { pgid: <?=$pagedata->id?> }, function(navitemlist){
						$('.nav-item-list').html(navitemlist);
						//console.log(navitemlist);
					})*/

				break;

				case 'Experiences':

					populatePgExp('<?=SITE_URL?>');

					/*
					$.get('<?=SITE_URL.'/getexpitemlist.php'?>', function(expitemlist){
						$('.exp-item-list').html(expitemlist);
						//console.log(navitemlist);
					})*/

				break;

				case 'Properties':

					getHomePageProperties('<?=SITE_URL?>');

				break;

				case 'Gallery':

					//populatePgSlider('<?=SITE_URL?>', <?=$pagedata->id?>);
					//var section = el.getAttribute('data-section');
					getPageGallery('<?=SITE_URL?>', <?=$pagedata->id?>, extra);

				break;

				case 'Promotions':

					//populatePropertyList('<?=SITE_URL?>');
					getPromotionData('<?=SITE_URL?>', extra);

				break;

				case 'Downloads':
					getDownloadItemData('<?=SITE_URL?>', extra);
				break;
			}


			if(evt != null)
			{
			    // Declare all variables
			    var i, tabcontent, tablinks;

			    // Get all elements with class="tabcontent" and hide them
			    tabcontent = document.getElementsByClassName("tabcontent");
			    for (i = 0; i < tabcontent.length; i++) {
			        tabcontent[i].style.display = "none";
			    }

			    // Get all elements with class="tablinks" and remove the class "active"
			    tablinks = document.getElementsByClassName("tablinks");
			    for (i = 0; i < tablinks.length; i++) {
			        tablinks[i].className = tablinks[i].className.replace(" active", "");
			    }

			    // Show the current tab, and add an "active" class to the button that opened the tab
			    document.getElementById(tabName).style.display = "block";
			    evt.currentTarget.className += " active";				
			}

		}


		function showNavLayout(layout){

		    $('.navlayout').children().hide();
		    $('.'+layout).show();
		}

		// trigger automatic click on description tab
		(function(){
			$('.auto').trigger('click');
		})();


		/**
		 * if .singletabdisplay is set
		 * call openTab manually
		 */
		if($('.singletabdisplay').length)
		{
			var elid = $('.singletabdisplay').attr('id');
			var extraattr = $('.singletabdisplay').attr('data-extra');

			openTab(null,elid,extraattr);
		}
	</script>

	<script type="text/javascript">
		setTimeout(function(){
			$('.alert').fadeOut(3000);
		}, 3000);
		
	</script>

	<!-- moved into loadscript.php
  	<script type="text/javascript">
  		
  			// have chained magnificPopup('open') as it required two clicks initiatially
  			(function(){ 	
  				$('.slider-image-list').on('click', '.slider-item-image', function(e){
  					
  					e.preventDefault();
  					//$(this).trigger('focus');
  					$(this).magnificPopup({
  						type: 'ajax'
  					}).magnificPopup('open');

  					/* inline popup
  					e.preventDefault();
  					var imagepath = $(this).attr('href');

  					$(this).magnificPopup({
  						items: {
  							src: '<div class="image-detail-view white-popup"><div style="float: left; width: 330px;"><img src="'+imagepath+'" style="width: 100%;"/></div><div class="" style="margin-left: 330px; padding-left: 10px;"><form id="frmsliderimgdetailupdate"><div class="field"><label for="alttext">Alt Tag Text</label><br><input type="text" name="alttext" id="alttext"/></div><div class="field"><label for="desctext">Short Caption</label><br><textarea name="desctext" id="desctext" rows="3" cols="50"></textarea></div><div class="field"><input type="submit" name="submit" value="Update"/></div></form></div><br style="clear: left;"/></div>',
  							type: 'inline'
  							//type: 'ajax'
  						}
  					}).magnificPopup('open');*/
  				})	


  				/* update slider image alt and desc
  				$('#frmsliderimgdetailupdate').on('submit', function(e){
  					e.preventDefault();
  					console.log('clicked to update image details');
  				})*/

  				$('body').on('click', '#frmsliderimgdetailupdate input[name=submit]', function(e){
  					e.preventDefault();
  					console.log('clicked to update image details');
  					//var form = $(this);

  					var submitaction = $(this).attr('data-action');
  					console.log(submitaction);

  					// if fields are empty return false
  					if(submitaction == 'update')
  					{
	   					if($('#frmsliderimgdetailupdate input[name=alttext]').val() == '' &&  $('#frmsliderimgdetailupdate textarea[name=desctext]').val() == '')
	  					{
	  						return false;
	  					} 						
  					}

  					console.log('laaaaa')
  					// we are good. prepare form data
  					var formData = new FormData();

  					formData.append('alttext', $('#frmsliderimgdetailupdate input[name=alttext]').val());
  					formData.append('desctext', $('#frmsliderimgdetailupdate textarea[name=desctext]').val());
  					formData.append('frmaction', submitaction);
  					formData.append('imagename', $('#frmsliderimgdetailupdate input[name=imagename]').val());
  					formData.append('imageid', $('#frmsliderimgdetailupdate input[name=imageid]').val());


					var urltopost = $('#frmsliderimgdetailupdate').attr('action');

					$.ajax({
						method: 'POST',
						url: urltopost,
						data: formData,
						contentType: false,
						dataType: 'json',
						processData: false,

					})
					.done(function(res){
						console.log(res); 

						if(res.status == true)
						{
							$.magnificPopup.close();

							populatePgSlider('<?=SITE_URL?>', <?=$pagedata->id?>);
						}
						//response = JSON.parse(res);	// when we set the expected data type to json and when we send with json_encode, no need to parse it with JSON.parse(res)

						//console.log(res.status);
						/*
						if(res.status == true)
						{
							$.get('http://freudenberg.cms/getexpitemlist.php', function(expitemlist){
								$('.exp-item-list').html(expitemlist);
								//console.log(navitemlist);
							})

							expform.trigger('reset');	// reset form

							// if action == 'update' remove navitem-id hidden element
							if(res.action == 'update')
							{
								// remove navitem-id hidden element
								expform.find('input[name=expitem-id]').remove();
							}

							$('#exp-image-preview').html('<p>Image not selected!</p>');		// reset preview

							$('#myExperienceModal').modal('hide');

							//console.log('Success');
						}*/
					});					
  				})


  			})();


  			



  		/*
  		$('.slider-image-list').on('click', '.slider-item-image').magnificPopup({
  			type: 'image'
  		})*/
  	</script>
  	-->
  	<?php if(!isset($_GET['action'])): ?>
 	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAs0ulbGRPK0XoFc-vqyHVQXqIZ3wj8pU0&callback=initMap"></script>
 	<script type="text/javascript">
	 	function initMap() {
	        var uluru = {lat: <?=$propertydata->property_map_latitude?>, lng: <?=$propertydata->property_map_longitude?>};
	        var map = new google.maps.Map(document.getElementById('gmappreview'), {
	          zoom: 18,
	          center: uluru
	        });
	        var marker = new google.maps.Marker({
	          position: uluru,
	          map: map
	        });
	    }
 	</script>
 	<?php endif; ?>

 	<?php if(isset($_GET['action']) && $_GET['action'] == 'addpage'): ?>
 		<script type="text/javascript">
 			$('.slider-option-wrapper').hide();
 			$('#pgslideropt').click(function(){
 				$('.slider-option-wrapper').toggle();
 				console.log('clicked');
 			})
 		</script>
 	<?php endif; ?>

 	<?php if(isset($_GET['action']) && $_GET['action'] == 'menu'): ?>
 		<script type="text/javascript">
 			$('#popmenumanagermodel').click(function(){
 				$('#MenuManagerModal').modal('show');
 			})
 		</script>
 	<?php endif; ?>

</body>
</html>
