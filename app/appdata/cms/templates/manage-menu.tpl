<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?=$pageTitle?></h1>
  <!--
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
  -->
</section>

<!-- Main content -->
<section class="content">
<?php if($errors): ?>
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<h4><i class="icon fa fa-ban"></i> Alert!</h4>
	<ul style="list-style: none; padding-left: 0px;">
	<?php foreach($errmsg as $err): ?>
		<li><?=$err?></li>
	<?php endforeach; ?>
	</ul>
</div>	
<?php elseif(isset($_GET['pg'])): ?>
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<h4><i class="icon fa fa-check"></i> Alert!</h4>
	A new page '<?=htmlspecialchars(urldecode($_GET['pg']), ENT_QUOTES)?>' is added successfully!
</div>
<?php endif; ?>
<div class="box box-primary">
<div class="box-header with-border">
	<a href="#" id="popmenumanagermodel">Add a Menu Item</a>
</div>
<form method="post" action="" role="form">
	<div class="box-body">

	</div>
	<!-- /.box-body -->
	<div class="box-footer">

	</div>
</form>
</div>
</section>
<!-- /.content -->
  <!-- Model -->
  <div id="MenuManagerModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="MenuManagerLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" style="padding: 10px;">
          <form id="frmmenumanager" method="post" action="<?=SITE_URL.'/ajax-downloads.php'?>" enctype="multipart/form-data">
              <div id="menuitem-details" style="/*width: 65%;*/">
                <h3 style="margin-top: 0px;">Add a Menu Item</h3>
                <div class="form-group" style="margin-top: 10px;">
                  <label for="menuitem-name">Name <span class="requiredmsg">(Required)</span></label><br>
                  <input type="text" style="width: 80%" name="menuitem-name" id="menuitem-name" value="" class="form-control" />
                  <!--<select name="downloaditem-property" class="form-control" style="width: 60%;"></select>-->
                </div> 
                <div class="form-group" style="clear: left;">
                  <label for="menuitem-target">Select Target<span class="requiredmsg">(Required)</span></label><br>
                  <select name="downloaditem-property" class="form-control" style="width: 80%;"></select>
                </div>                                  
                <div class="field downloadhiddenstuff">
                  <input type="hidden" name="downloaditem-page-id" value="<?=$pagedata->id?>"/>
                  <input type="hidden" name="downloaditem-action" value=""/>
                  <input type="submit" name="downloaditemsave" id="downloaditemsave" value="Add Item!" class="btn btn-primary"/>                
                </div>
              </div>
          </form>
      </div>
    </div>
  </div>