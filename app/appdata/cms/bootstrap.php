<?php
error_reporting(E_ALL);
session_start();

date_default_timezone_set("Asia/Colombo");

require_once 'config.php';
require_once 'Db.php';

	define('SITE_URL', 'http://cms.freudenbergleisure.com');
	//define('APP_ROOT', __DIR__);
	//define('DOC_ROOT', pathinfo(__FILE__,PATHINFO_DIRNAME).'/');
	//define('APP_ROOT', str_replace("\\","/", realpath(pathinfo(__FILE__, PATHINFO_DIRNAME).'/../../')).'/');
	define('APP_ROOT', realpath(pathinfo(__FILE__, PATHINFO_DIRNAME).'/../../'));

	// define custom exception handler
	function exception_handler($exception)
	{
		/*
		ob_start();
		include 'error.php';
		$out = ob_get_clean();

		echo $out;*/

		echo $exception;
	}

	set_exception_handler('exception_handler');

	// establish db connection
	$db = new Db($config['db']);


require_once 'functions.php';