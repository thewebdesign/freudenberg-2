﻿<?php 
require_once('top.php');
try{
    if(!NoCSRF::check('token', $_POST)){
        header('HTTP/1.0 403 Forbidden');
        header('Location:'.HTTP_PATH);
    }
}
catch(exception $e){
    header('HTTP/1.0 403 Forbidden');
    die($e->getMessage());
}
include 'includes/header.php'; 
require_once('classes/GoogleCustomSearch.php');

$searchstring = Generic::noHTML(filter_var($_POST['search'], FILTER_SANITIZE_STRING));

try{

    $search = new iMarc\GoogleCustomSearch($search_engine_data['main']['se_id'], $search_engine_data['main']['api_key']);
    $results = $search->search($searchstring);
}
catch(Exception $e){
    echo $e->getMessage();
    header('Location:'.HTTP_PATH);
    exit;
}

$result_count = ($results->totalResults > 10) ? 10 : $results->totalResults;
?>


    <style>
        .promo_header{margin-top:20px;}

        @import "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700";

/*.container { margin-top: 20px; }*/
.mb20 { margin-bottom: 20px; } 

hgroup { padding-left: 15px; border-bottom: 1px solid #ccc; }
hgroup h1 { font: 500 normal 1.625em "Roboto",Arial,Verdana,sans-serif; color: #2a3644; margin-top: 0; line-height: 1.15; }
hgroup h2.lead { font: normal normal 1.125em "Roboto",Arial,Verdana,sans-serif; color: #2a3644; margin: 0; padding-bottom: 10px; }

.search-result .thumbnail { border-radius: 0 !important; }
.search-result:first-child { margin-top: 0 !important; }
.search-result { margin-top: 20px; }
.search-result .col-md-2 { border-right: 1px dotted #ccc; min-height: 140px; }
.search-result ul { padding-left: 0 !important; list-style: none;  }
.search-result ul li { font: 400 normal .85em "Roboto",Arial,Verdana,sans-serif;  line-height: 30px; }
.search-result ul li i { padding-right: 5px; }
.search-result .col-md-7 { position: relative; }
.search-result h3 { font: 500 normal 1.375em "Roboto",Arial,Verdana,sans-serif; margin-top: 0 !important; margin-bottom: 10px !important; }
.search-result h3 > a, .search-result i { color: #248dc1 !important; }
.search-result p { font: normal normal 1.125em "Roboto",Arial,Verdana,sans-serif; } 
.search-result span.plus { position: absolute; right: 0; top: 126px; }
.search-result span.plus a { background-color: #248dc1; padding: 5px 5px 3px 5px; }
.search-result span.plus a:hover { background-color: #414141; }
.search-result span.plus a i { color: #fff !important; }
.search-result span.border { display: block; width: 97%; margin: 0 15px; border-bottom: 1px dotted #ccc; }

.search-container{width: 1170px;margin-left: 20%;}

@media only screen  and (max-width:1400px) and (min-width: 1200px){
	.search-container{margin-left: 7%;}
}
@media screen and (max-width: 1600px) and (min-width: 1401px){
	.search-container{margin-left: 15%;}
}

    </style>

    <body class="node-type-accommodation-list">

        <header id="header" role="banner">
            <?php include 'includes/navigation.php'; ?> 

        </header><!--  #header  -->
        <?php include 'includes/booking.php'; ?> 

        <div style="clear:both"></div>

        <div class="blur">  
            <div class="node--page_basic mode--full">    
                <aside role="complementary">
                    <header>
                    </header>    
                    <?php include 'includes/slider.php'; ?>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Experiences</li>
                    </breadcrumb>
                </div>

                <div id="main" role="main">     
                    <div style="margin: 0 auto">
                        
<!-- Search Results-->

<div class="search-container" >

    <hgroup class="mb20">
        <h1>Search Results</h1>
        <h2 class="lead"><strong class="text-danger"><?php echo $result_count ?></strong> results were found for the search for <strong class="text-danger"><?php echo $searchstring ?></strong></h2>                               
    </hgroup>

    <section class="col-xs-12 col-sm-6 col-md-12">

        <?php if($searchstring == ""): ?>

            <p style="color:red">Please enter a search term in the search box</p>
       
        <?php endif ?>


        <?php foreach ($results->results as $item): ?>

            <article class="search-result row">
                <div class="col-xs-12 col-sm-12 col-md-3">
                    <a href="<?php echo $item->link ?>" title="<?php echo $item->title ?>" class="thumbnail"><img src="<?php echo $item->thumbnail==""? 'assets/img/logo_g.png' : $item->thumbnail ?>" alt="<?php echo $item->title ?>" /></a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-2">
<!--                     <ul class="meta-search">
                        <li><i class="glyphicon glyphicon-calendar"></i> <span>02/15/2014</span></li>
                        <li><i class="glyphicon glyphicon-time"></i> <span>4:28 pm</span></li>
                        <li><i class="glyphicon glyphicon-tags"></i> <span>People</span></li>
                    </ul> -->
                </div>
                <div class="col-xs-12 col-sm-12 col-md-7 excerpet">
                    <h3><a href="#" title=""><?php echo $item->title ?></a></h3>
                    <p><?php echo $item->htmlSnippet ?></p>                        
                    </a></span>
                </div>
                <span class="clearfix borda"></span>
            </article>

        <?php endforeach; ?>

    </section>
</div>



                    </div>
                </div><!--  #main  -->
            </div><!--  #node-details  -->

            <div style="clear:both"></div>       

            <footer id="footer" role="contentinfo"> 
                <?php include 'includes/footer.php'; ?> 

                </body>

                </html>
