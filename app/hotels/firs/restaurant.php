﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_firs.php'; ?> 

    <style>
        table {border:none !important;text-align: left !important;}
        table td {border:none !important;text-align: left !important;}
    </style>
    <body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_firs.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_firs.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/rest/rest_slider1.jpg') no-repeat 50% 50%; background-size: cover;"></div> 
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/rest/rest_slider2.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/rest/rest_slider3.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/rest/rest_slider4.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    </div>

                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span><a href="facilities.php">Facilities</a></li>
                        <li><span class="arrow"> &gt; </span>Restaurant</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Restaurant</div>          
                                <p style="text-align:justify; font-size:16px;">Tantalise your taste-buds with a symphony of flavours from around the globe. Blending rustic tastes with modern flair, The Firs offers a delightful range of fusion cuisine ranging from Western to Indian to Continental and Eastern fare to appease the palate of even the most discerning guest. Each impeccably-presented dish prepared by the hotel's experienced team of culinary experts will take guests on an unforgettable gastronomic journey.
                                    <br><br>Capable of accommodating up to 15 guests, the hotel's main restaurant is an alluring/ideal venue to relish scrumptious breakfasts, lunches and dinners. The bright, yet intimate atmosphere of the venue creates the perfect setting to indulge in a culinary feast. In-room dining is also available for guests wishing to indulge in the privacy of their room.</p>

                                <h1 class="hdr-seven" style="text-align:left; font-size:13px;">Restaurant Overview</h1>
                                <table style="width: 75%; border:none" border="0" cellspacing="2" cellpadding="0">
                                    <tbody>
                                        <tr style="border:none">
                                            <td><strong>Restaurant Capacity:</strong></td>
                                            <td align="right">15 Guests</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Cuisine Available:</strong></td>
                                            <td align="right">Fusion Cuisine (Western, Indian, Continental, Eastern)</td>
                                        </tr>              
                                    </tbody>
                                </table>

                                <div style="clear:both;"></div> 
                                <?php include 'inner_slider.php'; ?>

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->
        </div>
        <div style="clear:both"></div>
        <?php include 'trip-advisor.php'; ?> 
        <footer id="footer" role="contentinfo">  
            <?php include '../../includes/footer_firs.php'; ?> 
        </footer>    
    </body>
</html>
