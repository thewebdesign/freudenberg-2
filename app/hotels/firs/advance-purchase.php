﻿<!DOCTYPE html>

<html class="no-js">
    <?php include '../../includes/header_firs.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_firs.php'; ?> 

        </header><!--  #header  -->  

        <?php include '../../includes/booking_firs.php'; ?> 

        <div class="blur">   

            <div id="node-6" class="node--accommodation_list mode--full">

                <aside role="complementary">
                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/firs_slider_3.jpg') no-repeat 50% 50%; background-size: cover;"></div> 
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_2.jpg') no-repeat 50% 50%; background-size: cover;"></div>                       
                    </div>

                </aside>  

                <main id="main" role="main">

                    <article role="article" style="background-image:url(assets/img/promo-bg.png); background-repeat:no-repeat;">

                        <div class="ctatext-wrapper">

                            <div class="ctatext-text">         

                                <h1 class="hdr-seven" style="text-align:center; font-size:17px; padding:10px; font-weight:100;">ADVANCE PURCHASE DISCOUNT</h1>

                                <div class="hdr-two">-Save 20% with Advance Purchase Discount-</div>          

                                <h1 class="hdr-seven" style="text-align:left; font-size:22px; padding:10px; font-weight:100;"><em>The Firs Nuwara Eliya  Early Bird Deal</em></h1>
                                <p style="text-align:justify; font-size:14px;">Time to think ahead! Make your booking 30 days in advance and enjoy 20% off on any room or meal plan. <br><br>Make your plans early, place your booking 30 days or more in advance, and you will be rewarded with a 20% discount on any room on any basis, be it Room only or B&B.</p>
                                <h1 class="hdr-seven hdr-seven-ect">Special Offer Include</h1>
                                <ul class="priv-poly">
                                    <li>20% discount for reservations 30 days forward on all meal plans for all room types</li>
                                    <li>Free Wifi</li>
                                </ul>

                                <h1 class="hdr-seven hdr-seven-ect">Special Offer Duration</h1>
                                <ul class="priv-poly">
                                    <li>21st October 2015 to 29th February 2016</li>
                                </ul>

                                <p style="text-align:justify; font-size:14px;">Suite Room rates starting from USD 170/- Per room per night</p>

                                <h1 class="hdr-seven hdr-seven-ect">CONDITIONS</h1>
                                <ul class="priv-poly">
                                    <li>Rates indicated above include a 10% service charge, & other government taxes.</li>
                                    <li>Rates above are indicative rates per room per night and are subject to change without notice.</li>
                                    <li>This offer is on a non-refundable cancellation policy. If canceled, modified or in case of no-show, the    total price of the reservation will be charged.</li>
                                    <li>Total amount of the reservation will be charged at the time of the reservation.</li>
                                    <li>Bookable period is from  21st October 2015 to 29th February 2016 with the stay period falling 30 days forward, until  29th February 2016.</li>
                                    <li>The travel/stay period for this offer will be 6th January 2015 to 31st August 2015.</li>
                                    <li>This promotion cannot be combined with any other promotion or discount and is subject to availability.</li>
                                </ul>


                            </div><!--  .ctatext-text  -->

                        </div><!--  .ctatext-wrapper  -->                          

                    </article>            

                </main>   

            </div><!--  #node-details  -->



            <div style="clear:both"></div>

            <footer id="footer" role="contentinfo">  

                <?php include '../../includes/footer_firs.php'; ?> 

            </footer>    

    </body>

</html>

