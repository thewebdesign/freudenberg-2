﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_firs.php'; ?> 

    <body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_firs.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_firs.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">
                    <?php include '../../includes/slider_firs.php'; ?>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Location</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Location</div>          
                                <p style="text-align:justify; font-size:16px;">Located in Nuwara Eliya aloft a hillock overlooking Lake Gregory, The Firs offers breathtaking vistas of the surrounding tea estates and greenery. The hotel is ideally located within an hour of some of the most picturesque sights and historical attractions of the hill country - the Hakgala Botanical Gardens, Queen Victoria Park, as well as the Horton Plains National Park.<br><br>
                                    Nuwara Eliya is also commonly known as "Little England" since the British had once tried to turn it into a typical English village; evidence of this can be seen in the English-style lawns and gardens that are still being maintained by private homes. With the essence of British colonial style living clearly evident, visitors to the city will be fascinated by the nostalgia of bygone days.</p>  

                                <h1 class="hdr-seven" style="text-align:left; font-size:13px;"><strong>Location Highlights:</strong></h1>
                                <ul class="accom-list">
                                    <li>Gregory Lake</li>
                                    <li>Hakgala Botanical Gardens</li>
                                    <li>Queen Victoria Park</li>
                                    <li>Horton Plains National Park</li>
                                    <li>World's End</li>
                                </ul> 
                                <div style="clear:both;"></div> 
                                <?php include 'inner_slider.php'; ?>

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>    
            </div><!--  #node-details  -->

            <div style="clear:both"></div>

            <?php include 'trip-advisor.php'; ?> 
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_firs.php'; ?> 
            </footer>    
    </body>
</html>
