﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_firs.php'; ?> 

    <body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_firs.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_firs.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/room/rooms1.jpg') no-repeat 50% 50%; background-size: cover;"></div>  
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/room/rooms2.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/room/rooms3.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    </div>

                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span><a href="facilities.php">Facilities</a></li>
                        <li><span class="arrow"> &gt; </span>Room Service</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Room Service</div>          
                                <p style="text-align:justify; font-size:16px;">The Firs strives to provide superior levels of comfort and all-round impeccable service to guests during their stay at the bungalow. Our multilingual staff will also be at hand and can also be contacted at any time.</p>

                                <h1 class="hdr-seven" style="text-align:left; font-size:13px;">Additional Services Offered by The Firs:</h1>
                                <ul class="accom-list">
                                    <li>In-room Dining Service on request</li>
                                    <li>Daily Housekeeping</li>
                                </ul>                

                                <div style="clear:both;"></div> 
                                <?php include 'inner_slider.php'; ?>

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <?php include 'trip-advisor.php'; ?> 
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_firs.php'; ?> 
            </footer>    
    </body>
</html>
