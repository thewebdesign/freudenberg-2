﻿<?php
$pg = ['property' => 'firs', 'page' => 'contactus'];
require_once('../../top.php');
require_once(DOC_ROOT.'includes/contact-us-process.php');
include DOC_ROOT.'includes/header_firs.php';
?>

    <body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-accommodation-list">
        <header id="header" role="banner">
            <h1 class="hide-visual">The Firs - Contact Us</h1>    
            <?php include '../../includes/navigation_firs.php'; ?> 
        </header><!--  #header  -->
        <div class="blur">  
            <div>
                <div id="main" role="main">
                    <div class="wrapper">
                        <article role="article"> 
                            <div class="ddd" style="position:relative; top:100px; float:right; width: 40%;"><img src="assets/images/cont-us.jpg" style="border: 8px solid #D5D4BA; border-radius: 10px;"></div>
                            <div style="position:relative; top:100px;">
                                <?php if (isset($hasError)) { ?>
                                    <div class="form-item webform-component webform-component-errors" id="error_msg">
                                        
                                        <ul>
                                             <?php foreach($errors as $error): ?>

                                                <li><?php echo $error ?></li>

                                            <?php endforeach ?>

                                        </ul>
                                    </div>
                                <?php } if (isset($success_msg)) { ?>
                                    <div class="form-item webform-component webform-component-success" id="success_msg">
                                        <label><?php echo $success_msg; ?></label>
                                    </div>
                                <?php } ?>
                                <form class="webform-client-form webform-client-form-78" enctype="multipart/form-data" action="" method="post" id="webform-client-form-78" accept-charset="UTF-8">

                                <input type="hidden" name="form_token" value="<?php echo $csrf_token ?>">
                                <input type="hidden" name="hotel" value='firs'>

                                    <div>
                                        <div class="form-item webform-component webform-component-textfield webform-component--name">
                                            <label for="edit-submitted-name">Subject </label>
                                            <input type="text" id="edit-submitted-name" name="subject" value="<?php echo $subject; ?>" size="60" maxlength="128" class="form-text" required>
                                        </div>
                                        <div class="form-item webform-component webform-component-textfield webform-component--name">
                                            <label for="edit-submitted-name">Name </label>
                                            <input type="text" id="edit-submitted-name" name="name" value="<?php echo $name; ?>" size="60" maxlength="128" class="form-text" required>
                                        </div>
                                        <div class="form-item webform-component webform-component-email webform-component--email">
                                            <label for="edit-submitted-email">Email Address </label>
                                            <input class="form-text" type="email" id="edit-submitted-email" name="email" value="<?php echo $email; ?>" size="60" required>
                                        </div>
                                        <div class="form-item webform-component webform-component-textfield webform-component--email">
                                            <label for="edit-submitted-email">Contact Number </label>
                                            <input class="form-text" type="text" id="edit-submitted-email" name="phone" value="<?php echo $phone; ?>" size="60" required>
                                        </div>
                                        <div class="form-item webform-component webform-component-textfield webform-component--email">
                                            <label for="edit-submitted-email">Country</label>
                                            <select name="country" id="countat_country" class="form-text" required>
                                                <option value="">Select Your Country</option>
                                                <?php
                                                foreach ($countries as $country) {
                                                    echo '<option value="' . $country . '"';
                                                    if ($user_country == $country) {
                                                        echo 'selected>' . $country . '</option>';
                                                    } else {
                                                        echo '>' . $country . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-item webform-component webform-component-textarea webform-component--comment---question">
                                            <label for="edit-submitted-comment-question">Message </label>
                                            <div class="form-textarea-wrapper resizable">
                                                <textarea id="edit-submitted-comment-question" name="comments" cols="60" rows="5" class="form-textarea" required><?php echo $comments; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
                                        <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=en"></script>
                                        <div class="form-actions">
                                            <input class="webform-submit button-primary form-submit" type="submit" name="contact_submit" value="Submit">
                                        </div>
                                    </div>
                                </form>
                            </div>


                            <?php $fr = $cms->getPropertyDetails(1); ?>
                            <ul class="add_freud">
                                <li><strong><?=$fr->property_name?></strong></li>
                                <?php 
                                    $addressparts = explode(',', $fr->property_address);

                                    foreach($addressparts as $part):
                                ?>
                                    <li><?=$part?></li>
                                <?php endforeach; ?>
                                <br>
                                <?php if(!empty($fr->property_telephone)): ?>
                                <li>Tel.: <?=$fr->property_telephone?></li>
                                <?php endif; ?>
                                <?php if(!empty($fr->property_fax)): ?>
                                <li>Fax: <?=$fr->property_fax?></li>
                                <?php endif; ?>
                                <?php if(!empty($fr->property_email)): ?>
                                <li>E-mail: <?=$fr->property_email?></li>    
                                <?php endif; ?>
                            </ul>
                            <!--
                            <ul class="add_freud">
                                <li><strong>Freudenberg Leisure</strong></li>
                                <li>103/14,</li>
                                <li>Dharmapala Mawatha,</li>
                                <li>Colombo 7,</li>
                                <li>Sri Lanka.</li><br>
                                <li>Tel.: +94 (0) 11 2445282</li>
                                <li>Fax: +94 (0) 11 2440083</li>
                                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                            </ul>
                            -->

                            <?php $firs = $cms->getPropertyDetails(3); ?>
                            <ul class="add_firs">
                                <li><strong><?=$firs->property_name?></strong></li>
                                <?php 
                                    $addressparts = explode(',', $firs->property_address);

                                    foreach($addressparts as $part):
                                ?>
                                    <li><?=$part?></li>
                                <?php endforeach; ?>
                                <br>
                                <?php if(!empty($firs->property_telephone)): ?>
                                <li>Tel.: <?=$firs->property_telephone?></li>
                                <?php endif; ?>
                                <?php if(!empty($firs->property_fax)): ?>
                                <li>Fax: <?=$firs->property_fax?></li>
                                <?php endif; ?>
                                <?php if(!empty($firs->property_email)): ?>
                                <li>E-mail: <?=$firs->property_email?></li>    
                                <?php endif; ?>
                            </ul> 

                            <!--
                            <ul class="add_firs">
                                <li><strong>The Firs</strong></li>
                                <li>85/2,</li>
                                <li>Upper Lake Road,</li>
                                <li>Nuwara Eliya,</li>
                                <li>Sri Lanka.</li><br>
                                <li>Tel.: +94 522 222 387</li>
                                <li>Fax: +94 522051687</li>
                                <li>E-mail:  info@firs.lk</li>
                            </ul>
                            -->             
                            <div style="clear:both"></div>
                            <link rel="stylesheet" href="assets/css/colorbox.css" />
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
                            <script src="assets/js/jquery.colorbox.js"></script>
                            <script>
                                $(function ()
                                {
                                    $(".example6").colorbox({iframe: true, innerWidth: 800, innerHeight: 300});
                                })
                            </script>
                            <div id="contact_box">
                                <div class="qr_main">
                                    <div class="qr_pic"><a href="map_pop.html" class="example6"><img src="assets/img/map.png" width="63" height="63" /></a></div>
                                    <div class="qr_cnt">Google map</div>
                                </div>
                                <div class="qr_main">
                                    <div class="qr_pic"><a href="cont_pop.html" class="example6"><img src="assets/img/contactus.png" width="63" height="63" /></a></div>
                                    <div class="qr_cnt">Contact Us</div>
                                </div> 
                            </div>     
                            <div style="clear:both"></div>
                            <script src="https://maps.googleapis.com/maps/api/js"></script>
                            <script>
                                var myCenter = new google.maps.LatLng(6.963518, 80.781524);
                                function initialize()
                                {
                                    var mapProp = {
                                        center: myCenter,
                                        zoom: 18,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                    };
                                    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
                                    var marker = new google.maps.Marker({
                                        position: myCenter,
                                    });
                                    marker.setMap(map);
                                }
                                google.maps.event.addDomListener(window, 'load', initialize);
                            </script>
                            <div id="googleMap"></div>     
                        </article>
                    </div><!--  .wrapper  -->
                </div><!--  #main  -->
            </div><!--  #node-details  -->
            <br><br><br><br><br><br>
            <div style="clear:both"></div>
            <?php include '../../includes/footer_randolee.php'; ?>  
    </body>
</html>
