﻿<?php 
$pg = ['property' => 'firs', 'page' => 'accommodation'];
include '../../includes/header_firs.php';
?>
    <body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_firs.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_firs.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">
                    <?php include '../../includes/slider_firs.php'; ?>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Accommodation</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">
                                  <?php require '../../includes/showdescription.php'; ?>     
                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          

                        <!--  .highligh-panels  -->          

                        </aside>               
                    </article>   

                </main>  
                <?php include 'trip-advisor.php'; ?> 
            </div><!--  #node-details  -->
            <?php include '../../includes/footer_firs.php'; ?> 
        </div><!--  .blur  -->      
    </body>
</html>
