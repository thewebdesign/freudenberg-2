﻿<?php 
$pg = ['property' => 'firs', 'page' => 'facilities'];
include '../../includes/header_firs.php';
?>
    <body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_firs.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_firs.php'; ?> 

        <div class="node--page_basic mode--full">
            <aside role="complementary">
                <?php include '../../includes/slider_firs.php'; ?>  
            </aside> 

            <div id="route">
                <breadcrumb class="menu">
                    <li><a href="index.php">Home</a></li>
                    <li><span class="arrow"> &gt; </span>Facilities</li>
                </breadcrumb>
            </div>

            <div id="main" role="main">     
                <article role="article" style="padding-top:10px;">
                    <div class="ctatext-wrapper">
                        <div class="ctatext-text">
                            <?php require '../../includes/showdescription.php'; ?>        
                        </div><!--  .ctatext-wrapper  -->
                    </div><!--  .ctatext-text  -->
                    <div style="clear:both"></div>

                    <div class="activities-list highlight-panels">
                            <?php require '../../includes/shownavigation-3.php'; ?>
                    </div><!--  .highlight-panels  -->
                </article>
            </div><!--  #main  -->
        </div><!--  #node-details  -->
        <?php include 'trip-advisor.php'; ?> 
        <footer id="footer" role="contentinfo">     

            <?php include '../../includes/footer_firs.php'; ?> 

    </body>
</html>
