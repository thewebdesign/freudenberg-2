﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_firs.php'; ?> 

    <body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_firs.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_firs.php'; ?> 

        <div class="node--page_basic mode--full">
            <aside role="complementary">
                <h1 class="hide-visual">Facilities</h1>
                <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                    <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/facili_slider1.jpg') no-repeat 50% 50%; background-size: cover;"></div> 
                    <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/facili_slider2.jpg') no-repeat 50% 50%; background-size: cover;"></div>   
                    <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/facili_slider3.jpg') no-repeat 50% 50%; background-size: cover;"></div>     
                    <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/facili_slider4.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/facili_slider5.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/facili_slider6.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                </div> 

                <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>     
            </aside> 

            <div id="route">
                <breadcrumb class="menu">
                    <li><a href="index.php">Home</a></li>
                    <li><span class="arrow"> &gt; </span>Facilities</li>
                </breadcrumb>
            </div>

            <div id="main" role="main">     
                <article role="article" style="padding-top:10px;">
                    <div class="ctatext-wrapper">
                        <div class="ctatext-text">
                            <div class="hdr-two">Facilities</div>
                            <p style="text-align:justify; font-size:17px;">Revel in the true essence of opulence and richness at The Firs, where a host of facilities, services and amenities await guests to ensure that there is never a dull moment during their stay.</p>

                            <h1 class="hdr-seven" style="text-align:left; font-size:13px;">Our services and facilities include:</h1>
                            <ul class="accom-list">
                                <li>Dining Facilities</li>
                                <li>Room Service</li>
                                <li>Laundry Service</li>
                                <li>Daily Housekeeping</li>
                                <li>In-room Dining Service</li>
                                <!--<li>Complimentary Butler Service</li>-->
                                <li>Complimentary Wi-Fi Internet Access</li>
                            </ul>        
                        </div><!--  .ctatext-wrapper  -->
                    </div><!--  .ctatext-text  -->
                    <div style="clear:both"></div>

                    <div class="activities-list highlight-panels">
                        <ul>
                            <li class="highlight" style="background: #ebebeb url('assets/images/sliders/facili/bar/inner/4.jpg') no-repeat 50% 50%; background-size: cover;"> 
                                <a class="highlight-hotspot" href="bar.php"></a>
                                <div class="highlight-background"></div><!--  .highlight-background  -->
                                <div class="highlight-content">
                                    <div class="highlight-content-inside">
                                        <h2 class="hdr-four">Bar</h2>     
                                        <div class="hdr-two fadeitem">Sip on a tantalising cocktail </div>                 
                                        <a class="btn-arrow fadeitem" href="bar.php">Read More</a>
                                    </div><!--  .highlight-content-inside  -->
                                </div><!--  .highlight-content  -->
                            </li><!--  .highlight  -->  

                            <li class="highlight" style="background: #ebebeb url('assets/images/sliders/facili/rest/inner/5.jpg') no-repeat 50% 50%; background-size: cover;">
                                <a class="highlight-hotspot" href="restaurant.php"></a>
                                <div class="highlight-background"></div><!--  .highlight-background  -->
                                <div class="highlight-content">
                                    <div class="highlight-content-inside">
                                        <h2 class="hdr-four">Restaurant</h2>
                                        <p class="fadeitem">Tantalise your taste-buds with a symphony of flavours from around the globe</p>     
                                        <a class="btn-arrow fadeitem" href="restaurant.php">Read More</a>
                                    </div><!--  .highlight-content-inside  -->
                                </div><!--  .highlight-content  -->
                            </li><!--  .highlight  -->  

                            <li class="highlight" style="background: #ebebeb url('assets/images/sliders/facili/room/inner/3.jpg') no-repeat 50% 50%; background-size: cover;">
                                <a class="highlight-hotspot" href="room-services.php"></a>
                                <div class="highlight-background"></div><!--  .highlight-background  -->
                                <div class="highlight-content">
                                    <div class="highlight-content-inside">
                                        <h2 class="hdr-four">Room Services</h2>
                                        <p class="fadeitem">The Firs strives to provide superior levels of comfort and all-round impeccable service to guests during their stay at the hotel</p>     
                                        <a class="btn-arrow fadeitem" href="room-services.php">Read More</a>
                                    </div><!--  .highlight-content-inside  -->
                                </div><!--  .highlight-content  -->
                            </li><!--  .highlight  -->       
                        </ul>
                    </div><!--  .highlight-panels  -->
                </article>
            </div><!--  #main  -->
        </div><!--  #node-details  -->
        <?php include 'trip-advisor.php'; ?> 
        <footer id="footer" role="contentinfo">     

            <?php include '../../includes/footer_firs.php'; ?> 

    </body>
</html>
