<?php 
$pg = ['property' => 'randholee', 'page' => 'promotions'];
include '../../includes/header_randholee.php';
?>
	<style>
    @media screen and (max-width:768px){
	.ctacard-text p{font-size: 1.1em;}} 


#fade{
    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:+111111;
 
    opacity:.80;
    filter: alpha(opacity=70);
}
#light{
	
    display: none;
margin:0 auto;
    width: 50%;

 

}

.promo{
position:fixed;

 z-index:+1111111111;
 top: 75px;
    overflow:visible;
	  	-moz-animation: fadein 2s; /* Firefox */
    -webkit-animation: fadein 2s; /* Safari and Chrome */
    -o-animation: fadein 2s; /* Opera */
}
@keyframes fadein {
    from {
        opacity:0;
    }
    to {
        opacity:1;
    }
}
@-moz-keyframes fadein { /* Firefox */
    from {
        opacity:0;
    }
    to {
        opacity:1;
    }
}
@-webkit-keyframes fadein { /* Safari and Chrome */
    from {
        opacity:0;
    }
    to {
        opacity:1;
    }
}
@-o-keyframes fadein { /* Opera */
    from {
        opacity:0;
    }
    to {
        opacity: 1;
    }
}


.clo{
	position:fixed;
	z-index:1003;
   margin-top: 3%;
    margin-left: 33%;
	    width: initial;

}


    </style>

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">

            <?php include '../../includes/navigation_randholee.php'; ?> 

        </header><!--  #header  -->

        <?php include '../../includes/booking_randholee.php'; ?> 

        <div class="blur">  

            <div class="node--page_basic mode--full">  
                <aside role="complementary">
                    <?php include '../../includes/slider_randholee.php'; ?>
                </aside>   
                
                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Promotions</li>
                    </breadcrumb>
                </div> 

                <aside role="complementary">
                    <div class="grid">
                    <div class="hdr-two" style="text-align: left; padding: 10px; font-size:1.2em;">RANDHOLEE PROMOTIONS</div>

                        <?php $promotions = $cms->getPromotions(2); ?>
                        <?php if(count($promotions) > 0): ?>
                        <?php foreach($promotions as $promo): ?>
                           <figure class="effect-chico">
                              <img src="<?=PARENT_DOMAIN.'images/promotions/'.$promo->property_slug.'/'.$promo->image?>" alt=""/>
                              <figcaption>
                                <h2><?=$promo->title?></h2>
                                <p><?=$promo->description?></p>
                                <?php if(!empty($promo->trigger_action)): ?>
                                    <?php if($promo->trigger_action == 'page'): ?>
                                        <a href="<?=RANDHOLEE_DOMAIN.'promotions/'.$cms->getPromoPage($promo->id)?>">View More</a>
                                    <?php elseif($promo->trigger_action == 'image'): ?>
                                        <a class="promo-pop" href="<?=PARENT_DOMAIN.'images/promotions/'.$promo->property_slug.'/popups/'.$cms->getPromoPopupImage($promo->id)?>">View more</a>
                                    <?php else: ?>
                                        <a href="#">View more</a>
                                    <?php endif; ?>
                                <?php endif; ?>
                              </figcaption>
                           </figure>                                
                        <?php endforeach; ?>
                        <?php else: ?>
                          <div class="hdr-two" style="font-style: italic; margin-bottom: 15px;"> No promotions are currently available. </div>
                        <?php endif; ?>   
						
                    </div><!--  .ctacard-wrapper  -->
                </aside>    

            </div>  

            <footer id="footer" role="contentinfo"> 

                <?php include 'trip-advisor.php'; ?>

                <?php include '../../includes/footer_randolee.php'; ?>

                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                <script type="text/javascript" src="assets/js/jquery.magnific-popup.min.js"></script>
 

                <script>

                    jq191 = jQuery.noConflict( true );

                    jq191('.promo-pop').magnificPopup({
                        type: 'image'
                    })
                 

                    function lightbox_open(){
                         window.scrollBy(0,0);
                        document.getElementById('light').style.display='block';
                        document.getElementById('fade').style.display='block';  

                    }

                    function lightbox_close(){
                        document.getElementById('light').style.display='none';

                        document.getElementById('fade').style.display='none';
                    }


                </script>

    </body>

</html>