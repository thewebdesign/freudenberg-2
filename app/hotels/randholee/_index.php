﻿<!DOCTYPE html>

<html class="no-js">

    <?php include '../../includes/header_randholee.php'; ?>

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">

            <h1 class="hide-visual">Randholee Luxury Resort - Randholee Luxury Resort Home Page</h1>  
            <?php include '../../includes/navigation_randholee.php'; ?> 

		
			
			
        </header><!--  #header  -->

        <?php include '../../includes/booking_randholee.php'; ?> 

        <div class="blur">  

            <div class="node--page_basic mode--full">  
                <aside role="complementary">
                    <header>
                        <h1 class="hide-visual">Randholee Luxury Resort Slideshow</h1>
                    </header> 
                    <?php include '../../includes/_slider_randholee.php'; ?>
                </aside>     

                <aside role="complementary">
                    <header>
                        <h1 class="hide-visual">Randholee Luxury Resort Experiences</h1>
                    </header>

                    <div id="main" role="main">
				
                    <div class="trip-advisor-logo" id="demo">
						<img class="trip-ad-close" src="assets/img/cls.png" onclick="myFunction()"  >
					<a href="https://www.tripadvisor.co.uk/Hotel_Review-g304138-d1594031-Reviews-Randholee_Luxury_Resort-Kandy_Central_Province.html" target="_blank"><img class="trip-ad" src="assets/images/trip-advisor-randholee.png"></a>
					
					</div>

                        <div class="wrapper">
                            <article role="article">
                                <div class="ctatext-wrapper">
                                    <div class="ctatext-text">
                                        <h1 class="hdr-seven" style="margin-bottom: 20px;">Randholee Resort and Spa</h1>
                                        <div class="hdr-two">“Perfect hide-out for those looking for<br/>rest &amp; Relaxation!”</div>
                                        <p align="justify">
                                            Randholee Resort features a unique blend of culture, cuisine and corporate comforts at the melting pot of cultural diversity and a rich heritage in Kandy an ideal destination for a fun family holiday or business retreat. It is located in the backdrop of fascinating mountains at the heart of this world heritage city. This resort is close to the sacred temple of the tooth relic, Sri Dalada Maligawa and several ancient and historic temples including Gadaladeniya and Lankatillake temples and Peradeniya Botanical Gardens. Randholee Resort also offers boat rides in Kandy Lake and nature trails. It provides body and beauty treatments and visitors can take advantage of recreational amenities as well.    
                                        </p>
                                        <div class="double-button">
                                            <!--<a class="btn-underline" href="#">Special Promotions</a>-->
                                            <!--<a class="btn-underline" href="honeymoon.php">Honeymoon</a>-->
                                        </div><!--  .double-button  -->
                                    </div><!--  .ctatext-text  -->   
                                </div><!--  .ctatext-wrapper  -->
                            </article>
                        </div><!--  .wrapper  -->
                    </div><!--  #main  -->

                    <div class="experience-thumblist highlight-panels">
                        <ul>  
                            <div class="experience-thumblist highlight-panels">
                                <ul>          

                                    <li class="highlight" style="background: #ebebeb url('assets/images/romancee.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="honeymoon.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">       
                                                <h2 class="hdr-four">Romance &amp; Relaxation</h2>    
                                                <div class="hdr-two fadeitem"><em>Peace and Quiet Romantic</em></div>  
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">See All Experiences</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 


                                    <li class="highlight" style="background: #ebebeb url('assets/images/adventure.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="infinity-pool.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">
                                                <h2 class="hdr-four">Infinity Pool</h2> 
                                                <div class="hdr-two fadeitem"><em>Immerse yourself in the refreshing water</em></div>  
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">Read More</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 

                                    <li class="highlight" style="background: #ebebeb url('assets/images/generations.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="promotions.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">       
                                                <h2 class="hdr-four">Promotions</h2>   
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">See All Promotions</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 

                                    <li class="highlight" style="background: #ebebeb url('assets/images/retreats.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="assets/360/poolview.html" target="_blank"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">   
                                                <h2 class="hdr-four">360 Experience</h2> 
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">View</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 

                                    <li class="highlight" style="background: #ebebeb url('assets/images/gallery.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="gallery.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">
                                                <h2 class="hdr-four">Gallery</h2> 
                                                <div class="hdr-two fadeitem"><em></em></div> 
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">View</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 
                                </ul>
                            </div><!--  .experience-thumblist .highlight-panels  --> 
                        </ul>
                    </div><!--  .experience-thumblist .highlight-panels  -->
                </aside>    

            </div><!--  #node-details  -->

            <footer id="footer" role="contentinfo"> 

                <?php include 'trip-advisor.php'; ?>
                <?php /* ?>                <aside role="complementary">
                  <div class="ctatext-buildadventure ctatext-wrapper">
                  <div class="ctatext-text">
                  <h1 class="hdr-seven">Build your own Adventure</h1>
                  <div class="hdr-two">Your Perfect Getaway Awaits</div>
                  <p>Whatever your lifestyle or pace, Freudenberg Leisure offers something unique for everyone.</p>
                  <a class="btn-arrow" href="#">Book Now</a>
                  </div><!--  .ctatext-text  -->
                  </div><!--  .ctatext-wrapper  -->
                  </aside><?php */ ?>

                <div style="clear:both"></div>

                <?php include '../../includes/footer_randolee.php'; ?>


				
				
		<script type="text/javascript">
function myFunction() {
    document.getElementById("demo").style.display = "none";
}
</script>

                </body>
				
				

                </html>