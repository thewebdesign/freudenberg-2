<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_randholee.php'; ?> 
    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_randholee.php'; ?> 
        </header><!--  #header  -->
        <?php include '../../includes/booking_randholee.php'; ?> 
        <div class="blur">  
            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">
                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_1.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_2.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_3.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_4.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_5.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_7.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_8.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    </div>
                    
                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>
                </aside>  
                
                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Accommodation</li>
                    </breadcrumb>
                </div>
                
                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">
                                <h1 class="hdr-seven">Accommodation</h1>          
                                <div class="hdr-two">Deluxe Mountain View &amp; Non View</div>          
                                <p align="justify">Step into an extraordinary world fashioned for uninterrupted peace. Nestled in the heart of Mount Pleasant and surrounded by its beauty, the views offered from our rooms are nothing short of breathtaking. The earthy tones of the rooms, the Kandyan paintings and the rich mahogany furniture add to this enchanting atmosphere. The palatial beds are fitted with soft cotton sheets that are cool and crisp to the touch. Our spacious bathrooms include a large bathtub and a commodious counter for your comfort. Equipped with amenities such as cable television and tea and coffee making facilities, the rooms combine the opulence of a palace with the comforts of home.</p>          
                                <!-- <a href="#" class="btn-underline">Book Your Getaway</a> -->       
                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->
                        <div class="highlight-panels">
                            <ul>                                
                                <li class="highlight" style="background: #ebebeb url('assets/images/suite_front.jpg') no-repeat 50% 50%; background-size: cover;">
                                	<div class="highlight-background"></div><!--  .highlight-content-inside  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">
                                                <h2 class="hdr-four">Suites</h2>     
                                                    <div class="hdr-two fadeitem">The magnificent suite</div>      
                                                    	<p class="fadeitem">The grand and graceful style that is evident throughout the property and reflects the essence of living like royalty</p>            
                                            <a href="suite.php" class="btn-arrow fadeitem">View All</a>      
                                            </div><!--  .highlight-content-inside  -->      
                                        </div><!--  .highlight-content  -->
                                </li><!--  .highlight  --> 
                                <li class="highlight" style="background: #ebebeb url('assets/images/acc_front_1.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <div class="highlight-background"></div><!--  .highlight-content-inside  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Deluxe Mountain View</h2>      
                                            <div class="hdr-two fadeitem">Upscale Comfort</div>      
                                            <p class="fadeitem">Step into an extraordinary world fashioned for uninterrupted and peace.</p>            
                                            <a href="deluxe_m_view.php" class="btn-arrow fadeitem">View All</a>      
                                        </div><!--  .highlight-content-inside  -->      
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->  
                                <li class="highlight" style="background: #ebebeb url('assets/images/acc_front_2.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <div class="highlight-background"></div><!--  .highlight-content-inside  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Deluxe</h2>      
                                            <div class="hdr-two fadeitem">Absolutely Breathtaking</div>      
                                            <p class="fadeitem">Nestled in the heart of Mount Pleasant and surrounded by its beauty.</p>            
                                            <a href="deluxe.php" class="btn-arrow fadeitem">View All</a>      
                                        </div><!--  .highlight-content-inside  -->      
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->        </ul>
                        </div><!--  .highligh-panels  -->
                    </article>          
                </main>   
            </div><!--  #node-details  -->
            <?php include 'trip-advisor.php'; ?>
            <footer id="footer" role="contentinfo">     
                <?php include '../../includes/footer_randolee.php'; ?> 
                </body>
                </html>
