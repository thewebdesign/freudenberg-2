﻿<!DOCTYPE html>

<html class="no-js">

    <?php include '../../includes/header_randholee.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_randholee.php'; ?> 

        </header><!--  #header  -->
        <?php include '../../includes/booking_randholee.php'; ?> 
        <div class="blur">  
            <div id="node-6" class="node--accommodation_list mode--full">

                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/cuisine/buffet/buffet_s.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/cuisine/buffet/buffet_s1.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/cuisine/buffet/buffet_s2.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/cuisine/buffet/buffet_s3.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/cuisine/buffet/buffet_s4.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    </div>
                    
                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>

                </aside>  
                
                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span><a href="cuisine.php">Cuisine</a></li>
                        <li><span class="arrow"> &gt; </span>Buffet</li>
                    </breadcrumb>
                </div>


                <main id="main" role="main">

                    <article role="article">

                        <div class="ctatext-wrapper">

                            <div class="ctatext-text">         

                                <div class="hdr-two">Buffet</div>          

                                <p style="text-align:justify; font-size:18px;">Dining at Randholee Resort is a sumptuous experience fit for royalty. Savour fragrant rice from India, creamy pasta from Italy or soft couscous from the Middle East during our International Buffet nights. Our Sri Lankan buffet allows guests to experience the texture and taste of traditional foods rich with spices and coconut milk. If you are in the mood for something unique, order from our a la carte menu which has an array of local and international choices.</p>  
                                <?php include 'inner_slider.php'; ?> 

                            </div><!--  .ctatext-text  -->

                        </div><!--  .ctatext-wrapper  -->                          

                    </article>      



                </main>   

            </div><!--  #node-details  -->



            <div style="clear:both"></div>

            <footer id="footer" role="contentinfo">  

                <?php include '../../includes/footer_randolee.php'; ?> 

            </footer>    

    </body>

</html>

