﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_randholee.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_randholee.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_randholee.php'; ?> 

        <div class="node--page_basic mode--full">
            <aside role="complementary">
                <h1 class="hide-visual">Facilities</h1>
                    <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/fac4.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/fac2.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/fac3.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/fac1.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/fac5.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/fac6.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/fac7.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/fac8.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/fac9.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/fac10.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                </div>  
                
                <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>        
            </aside> 

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Facilities</li>
                    </breadcrumb>
                </div>

            <div id="main" role="main">     
                <article role="article" style="padding-top:10px;">
                    <div class="ctatext-wrapper">
                        <div class="ctatext-text">
                            <div class="hdr-two">Facilities</div>
                            <p style="text-align:justify; font-size:17px;">Randholee Luxury Resort offers various facilities to make your stay with us more pleasurable. Work out in the gym, swim a lap in the pool or schedule a massage at the Ayurveda spa. Our sophisticated Business Centre includes computers with high speed Internet access and the entire premise of the hotel is wi-fi enabled so that you may browse the Internet from any corner of the hotel. Hike up a misty mountains during the day and spend your evenings playing a game of darts or pool in the comforts of the hotel. Should you run out of local currency, head straight to our Currency Exchange Centre where you can buy local currency at competitive bank rates. With so many facilities within the hotel you will never want to check-out.</p>        
                        </div><!--  .ctatext-wrapper  -->
                    </div><!--  .ctatext-text  -->


                    <div class="activities-list highlight-panels">
                        <ul>
                            <li class="highlight" style="background: #ebebeb url('assets/images/squash_court/sc2.jpg') no-repeat 50% 50%; background-size: cover;"> 
                                <a class="highlight-hotspot" href="squash-court.php"></a>
                                <div class="highlight-background"></div><!--  .highlight-background  -->
                                <div class="highlight-content">
                                    <div class="highlight-content-inside">
                                        <h2 class="hdr-four">Squash Court</h2>     
                                        <div class="twoo fadeitem">a state-of-the-art squash court</div>      
                                        <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
                                    </div><!--  .highlight-content-inside  -->
                                </div><!--  .highlight-content  -->
                            </li><!--  .highlight  -->  

                            <li class="highlight" style="background: #ebebeb url('assets/images/badminton_and_table_tennis/bm3.jpg') no-repeat 50% 50%; background-size: cover;">
                                <a class="highlight-hotspot" href="badminton-and-table-tennis.php"></a>
                                <div class="highlight-background"></div><!--  .highlight-background  -->
                                <div class="highlight-content">
                                    <div class="highlight-content-inside">
                                        <h2 class="hdr-four">Badminton & Table Tennis</h2>
                                        <div class="twoo fadeitem">Ideal for family and friends</div>
                                        <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
                                    </div><!--  .highlight-content-inside  -->
                                </div><!--  .highlight-content  -->
                            </li><!--  .highlight  -->  

                            <li class="highlight" style="background: #ebebeb url('assets/images/shopping_boutique/sb3.jpg') no-repeat 50% 50%; background-size: cover;">
                                <a class="highlight-hotspot" href="shopping-boutique.php"></a>
                                <div class="highlight-background"></div><!--  .highlight-background  -->
                                <div class="highlight-content">
                                    <div class="highlight-content-inside">
                                        <h2 class="hdr-four">Shopping Boutique</h2>
                                        <p class="fadeitem">Choose from an assortment of souvenirs and precious gemstones at Randholee's shopping boutique and take back a few mementos of your visit to Kandy.</p>     
                                        <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
                                    </div><!--  .highlight-content-inside  -->
                                </div><!--  .highlight-content  -->
                            </li><!--  .highlight  -->  

                            <li class="highlight" style="background: #ebebeb url('assets/images/infinity_pool/ip3.jpg') no-repeat 50% 50%; background-size: cover;">
                                <a class="highlight-hotspot" href="infinity-pool.php"></a>       
                                <div class="highlight-background"></div><!--  .highlight-background  -->
                                <div class="highlight-content">
                                    <div class="highlight-content-inside">
                                        <h2 class="hdr-four">Infinity Pool</h2>
                                        <div class="twoo fadeitem">Immerse yourself in the refreshing water</div>
                                        <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
                                    </div><!--  .highlight-content-inside  -->
                                </div><!--  .highlight-content  -->
                            </li><!--  .highlight  -->  

                            <li class="highlight" style="background: #ebebeb url('assets/images/wedding/wed1.jpg') no-repeat 50% 50%; background-size: cover;">
                                <a class="highlight-hotspot" href="wedding.php"></a>       
                                <div class="highlight-background"></div><!--  .highlight-background  -->
                                <div class="highlight-content">
                                    <div class="highlight-content-inside">
                                        <h2 class="hdr-four">Wedding</h2>
                                        <div class="twoo fadeitem">A royal wedding experience</div>
                                        <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
                                    </div><!--  .highlight-content-inside  -->
                                </div><!--  .highlight-content  -->
                            </li><!--  .highlight  -->  

                            <li class="highlight" style="background: #ebebeb url('assets/images/spa/spa1.jpg') no-repeat 50% 50%; background-size: cover;">
                                <a class="highlight-hotspot" href="spa.php"></a>
                                <div class="highlight-background"></div><!--  .highlight-background  -->
                                <div class="highlight-content">
                                    <div class="highlight-content-inside">
                                        <h2 class="hdr-four">Spa</h2>
                                        <div class="twoo fadeitem">Revel in a blissful sensory experience at spa</div>
                                        <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
                                    </div><!--  .highlight-content-inside  -->
                                </div><!--  .highlight-content  -->
                            </li><!--  .highlight  -->

                            <li class="highlight" style="background: #ebebeb url('assets/images/conferences_and_workshops/conf1.jpg') no-repeat 50% 50%; background-size: cover;">
                                <a class="highlight-hotspot" href="conferences-and-workshops.php"></a>
                                <div class="highlight-background"></div><!--  .highlight-background  -->
                                <div class="highlight-content">
                                    <div class="highlight-content-inside">
                                        <h2 class="hdr-four">Conferences and Workshops</h2>
                                        <div class="twoo fadeitem">The remarkable conference centre with a resplendent view.</div>
                                        <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
                                    </div><!--  .highlight-content-inside  -->
                                </div><!--  .highlight-content  -->
                            </li><!--  .highlight  -->    

                            <li class="highlight" style="background: #ebebeb url('assets/images/fitness_centre/fc6.jpg') no-repeat 50% 50%; background-size: cover;">
                                <a class="highlight-hotspot" href="fitness-centre.php"></a>
                                <div class="highlight-background"></div><!--  .highlight-background  -->
                                <div class="highlight-content">
                                    <div class="highlight-content-inside">
                                        <h2 class="hdr-four">Fitness Centre</h2>
                                        <div class="twoo fadeitem">A state-of-the-art gym</div>
                                        <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
                                    </div><!--  .highlight-content-inside  -->
                                </div><!--  .highlight-content  -->
                            </li><!--  .highlight  -->  

                            <li class="highlight" style="background: #ebebeb url('assets/images/business_centre/bc2.jpg') no-repeat 50% 50%; background-size: cover;">
                                <a class="highlight-hotspot" href="business-centre.php"></a>
                                <div class="highlight-background"></div><!--  .highlight-background  -->
                                <div class="highlight-content">
                                    <div class="highlight-content-inside">
                                        <h2 class="hdr-four">Business Centre</h2>
                                        <p class="fadeitem">Randholee Luxury Resort offers our esteemed guests various facilities to utilize when conducting business from our hotel, leaving you free to leave your laptops and cell phones at home.</p>
                                        <!--<a class="btn-arrow fadeitem" href="#">Read More</a>-->
                                    </div><!--  .highlight-content-inside  -->
                                </div><!--  .highlight-content  -->
                            </li><!--  .highlight  -->        
                        </ul>
                    </div><!--  .highlight-panels  -->
                </article>
            </div><!--  #main  -->
        </div><!--  #node-details  -->



        <footer id="footer" role="contentinfo">  

            <?php include 'trip-advisor.php'; ?> 
            <?php include '../../includes/footer_randolee.php'; ?> 

    </body>
</html>
