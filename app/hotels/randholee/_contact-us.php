﻿<?php 
require_once('../../top.php');
require_once(DOC_ROOT.'includes/contact-us-process.php');
require_once(DOC_ROOT.'includes/header_randholee.php');
?>

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <h1 class="hide-visual">Randholee Luxury Resort - Contact Us</h1>    
            <?php include '../../includes/navigation_randholee.php'; ?> 
        </header><!--  #header  -->
        <div class="blur">  
            <div>
                <div id="main" role="main">
                    <div class="wrapper">
                        <article role="article">
                            <div class="ddd" style="position:relative; top:80px; float:right; width: 44%;"><img src="assets/images/cont-us.jpg" style="border: 8px solid #DB9C72; border-radius: 10px;"></div>
                            <div style="position:relative; top:110px;">
                                <?php if (isset($hasError)) { ?>
                                    <div class="form-item webform-component webform-component-errors" id="error_msg">
                                      
                                        <ul>
                                            <?php foreach($errors as $error): ?>

                                                <li><?php echo $error ?></li>

                                            <?php endforeach ?>
                                        </ul>
                                    </div>
                                <?php } if (isset($success_msg)) { ?>
                                    <div class="form-item webform-component webform-component-success" id="success_msg">
                                        <label><?php echo $success_msg; ?></label>
                                    </div>
                                <?php } ?>
                                <form class="webform-client-form webform-client-form-78" enctype="multipart/form-data" action="" method="post" id="webform-client-form-78" accept-charset="UTF-8">

                                <input type="hidden" name="form_token" value="<?php echo $csrf_token ?>">
                                <input type="hidden" name="hotel" value='randholee'>

                                    <div>
                                        <div class="form-item webform-component webform-component-textfield webform-component--name">
                                            <label for="edit-submitted-name">Subject </label>
                                            <input type="text" id="edit-submitted-name" name="subject" value="<?php echo $subject; ?>" size="60" maxlength="128" class="form-text" required>
                                        </div>
                                        <div class="form-item webform-component webform-component-textfield webform-component--name">
                                            <label for="edit-submitted-name">Name </label>
                                            <input type="text" id="edit-submitted-name" name="name" value="<?php echo $name; ?>" size="60" maxlength="128" class="form-text" required>
                                        </div>
                                        <div class="form-item webform-component webform-component-email webform-component--email">
                                            <label for="edit-submitted-email">Email Address </label>
                                            <input class="form-text" type="email" id="edit-submitted-email" name="email" value="<?php echo $email; ?>" size="60" required>
                                        </div>
                                        <div class="form-item webform-component webform-component-textfield webform-component--email">
                                            <label for="edit-submitted-email">Contact Number </label>
                                            <input class="form-text" type="text" id="edit-submitted-email" name="phone" value="<?php echo $phone; ?>" size="60" required>
                                        </div>
                                        <div class="form-item webform-component webform-component-textfield webform-component--email">
                                            <label for="edit-submitted-email">Country</label>
                                            <select name="country" id="countat_country" class="form-text" required>
                                                <option value="">Select Your Country</option>
                                                <?php
                                                foreach ($countries as $country) {
                                                    echo '<option value="' . $country . '"';
                                                    if ($user_country == $country) {
                                                        echo 'selected>' . $country . '</option>';
                                                    } else {
                                                        echo '>' . $country . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-item webform-component webform-component-textarea webform-component--comment---question">
                                            <label for="edit-submitted-comment-question">Message </label>
                                            <div class="form-textarea-wrapper resizable">
                                                <textarea id="edit-submitted-comment-question" name="comments" cols="60" rows="5" class="form-textarea" required><?php echo $comments; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
                                        <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=en"></script>
                                        <div class="form-actions">
                                            <input class="webform-submit button-primary form-submit" type="submit" name="contact_submit" value="Submit">
                                        </div>
                                    </div>
                                </form>
                            </div>      
                            <ul class="add_freud">
                                <li><strong>Freudenberg Leisure</strong></li>
                                <li>103/14,</li>
                                <li>Dharmapala Mawatha,</li>
                                <li>Colombo 7,</li>
                                <li>Sri Lanka.</li><br>
                                <li>Tel.: +94 (0) 11 2445282</li>
                                <li>Fax: +94 (0) 11 2440083</li>
                                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                            </ul>
                            <ul class="add_rand">
                                <li><strong>Randholee Resort and Spa</strong></li>
                                <li>Heerassagala Rd,</li>
                                <li>Bowalawatte,</li>
                                <li>Kandy,</li>
                                <li>Sri Lanka.</li><br>
                                <li>Tel.: + 94 81 2217741 – 3</li>
                                <li>Fax: + 94 81 2217744</li>
                                <li>E-mail:  reservations@randholeeresorts.com</li>
                            </ul>             
                            <div style="clear:both"></div>     
                            <link rel="stylesheet" href="assets/css/colorbox.css" />
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
                            <script src="assets/js/jquery.colorbox.js"></script>
                            <script>
                                $(function ()
                                {
                                    $(".example6").colorbox({iframe: true, innerWidth: 800, innerHeight: 300});
                                })
                            </script>
                            <div id="contact_box">
                                <div class="qr_main">
                                    <div class="qr_pic"><a href="map_pop.html" class="example6"><img src="assets/img/map.jpeg" width="63" height="63" /></a></div>
                                    <div class="qr_cnt">Google map</div>
                                </div>
                                <div class="qr_main">
                                    <div class="qr_pic"><a href="assets/img/contt_qr.jpg" class="example6"><img src="assets/img/contact.jpeg" width="63" height="63" /></a></div>
                                    <div class="qr_cnt">Contact Us</div>
                                </div> 
                            </div>     
                            <div style="clear:both"></div>
                            <script src="https://maps.googleapis.com/maps/api/js"></script>
                            <script>
                                var myCenter = new google.maps.LatLng(7.263146, 80.616914);
                                function initialize()
                                {
                                    var mapProp = {
                                        center: myCenter,
                                        zoom: 18,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                    };
                                    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
                                    var marker = new google.maps.Marker({
                                        position: myCenter,
                                    });
                                    marker.setMap(map);
                                }
                                google.maps.event.addDomListener(window, 'load', initialize);
                            </script>
                            <div id="googleMap"></div>      
                        </article>
                    </div><!--  .wrapper  -->   
                </div><!--  #main  -->
            </div><!--  #node-details  -->
            <br><br><br><br><br><br>
            <footer id="footer" role="contentinfo">  
                <?php /* ?>     <aside role="complementary">
                  <div class="ctatext-buildadventure ctatext-wrapper">
                  <div class="ctatext-text">
                  <h1 class="hdr-seven">Build your own Adventure</h1>
                  <div class="hdr-two">Your Perfect Getaway Awaits</div>
                  <p>Whatever your lifestyle or pace, Freudenberg Leisure offers something unique for everyone.</p>
                  <a class="btn-arrow" href="#">Book Now</a>
                  </div>
                  </div>
                  </aside><?php */ ?>
                <?php include 'trip-advisor.php'; ?>
                <?php include '../../includes/footer_randolee.php'; ?>  
                </body>
                </html>
