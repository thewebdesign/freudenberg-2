<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_randholee.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_randholee.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_randholee.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">
                    <?php include '../../includes/_slider_randholee.php'; ?>
                </aside>
                
                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span><a href="facilities.php">Facilities</a></li>
                        <li><span class="arrow"> &gt; </span>Conferences and Workshops</li>
                    </breadcrumb>
                </div>  

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">  
                                <h1 class="hdr-seven">The remarkable conference centre with a resplendent view.</h1>   
                                <div class="hdr-two">Conferences and Workshops</div>          
                                <p style="text-align:justify; font-size:16px;">
                                    Randholee Resorts is a wonderful location for modest conferences and corporate events. The exceptional location and the brilliant Kandy climate contribute to the unique environment in which to set your conference. We offer outstanding services and extensive facilities to make your conference successful and memorable. We are able to accommodate approximately 50 people for various functions and the seating and hall arrangements can be altered to your specifications. The location, service and facilities we offer make our conference centre one of the finest venues available in Kandy.</p>  
                                <?php include 'inner_slider.php'; ?> 

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_randolee.php'; ?> 
            </footer>    
    </body>
</html>
