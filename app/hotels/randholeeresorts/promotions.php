﻿<!DOCTYPE html>

<html class="no-js">
	<style>
    @media screen and (max-width:768px){
	.ctacard-text p{font-size: 1.1em;}}    
    </style>

    <?php include '../../includes/header_randholee.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">

            <?php include '../../includes/navigation_randholee.php'; ?> 

        </header><!--  #header  -->

        <?php // include '../../includes/booking_randholee.php'; ?> 

        <div class="blur">  

            <div class="node--page_basic mode--full">  
                <aside role="complementary">
                    <?php include '../../includes/slider_randholee.php'; ?>
                </aside>   
                
                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Promotions</li>
                    </breadcrumb>
                </div> 

                <aside role="complementary">
                    <div class="grid">
                    <div class="hdr-two" style="text-align: left; padding: 10px; font-size:1.2em;">RANDHOLEE PROMOTIONS</div>
                    	<figure class="effect-chico">
                            <img src="assets/images/stay49t_promo.jpg" alt="img10"/>
                            <figcaption>
                                <h2>STAY 4 NIGHTS AND GET THE <span>ONE NIGHT FREE!</span></h2>
                                <p>Book 04 consecutive nights at Randholee Resorts. Receive our best available rate and your fourth night free!</p>
                                <a href="long-stay.php">View more</a>
                            </figcaption>			
                        </figure>
                        <figure class="effect-chico">
                            <img src="assets/images/lastmin_promo.jpg" alt="img09"/>
                            <figcaption>
                                <h2>LAST MINUTE <span>HOT DEAL</span></h2>
                                <p>Calling all last minute decision makers! Book your holiday with us within 7 days of your arrival and enjoy an astonishing 40% discount!</p>
                                <a href="#">View more</a>
                            </figcaption>			
                        </figure>
                        <figure class="effect-chico">
                            <img src="assets/images/honeymoon_promo.jpg" alt="img10"/>
                            <figcaption>
                                <h2>Honeymoon at <span>Randholee</span></h2>
                                <p>Wrapped in a pleasant climate, surrounded by the beauty of nature and pampered like royalty, have the most memorable honeymoon.</p>
                                <a href="#">View more</a>
                            </figcaption>			
                        </figure>
                        <figure class="effect-chico">
                            <img src="assets/images/resort_promo.jpg" alt="img10"/>
                            <figcaption>
                                <h2>RANDHOLEE RESORTS <span>ADVANCE PURCHASE</span></h2>
                                <p>RANDHOLEE Resort’s Advance Purchase rate offers savings of 20% for bookings made 30 days in advance, a relaxing vacation in a comfortable and cozy room with a variety of great meals</p>
                                <a href="#">View more</a>
                            </figcaption>			
                        </figure>                        
                       </div><!--  .ctacard-wrapper  -->
                </aside>    

            </div>  

            <footer id="footer" role="contentinfo"> 

                <?php include 'trip-advisor.php'; ?>

                <?php include '../../includes/footer_randolee.php'; ?>



                </body>

                </html>