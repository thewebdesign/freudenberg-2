﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_randholee.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_randholee.php'; ?> 
        </header><!--  #header  -->

        <?php // include '../../includes/booking_randholee.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <?php include '../../includes/slider_randholee.php'; ?>
                </aside>  
                
                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span><a href="facilities.php">Facilities</a></li>
                        <li><span class="arrow"> &gt; </span>Wedding</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Wedding</div>          
                                <p style="text-align:justify; font-size:16px;">Exchange your wedding vows as the sun sets behind the misty mountains of Kandy. Have your first dance as a newly wed couple beneath the divine clouds. Feel the aura of mother nature as you step in matrimony under the vast blue sky. Randholee Luxury Resort offers an elegant setting for a royal wedding experience. <br />Our wedding specialists at Randholee Luxury Resort have vast experience in organizing, planning and arranging weddings. Our staff will ensure that your wedding is handled with precise detail and care. All aspects, including legal procedures, will be taken care of prior to the wedding day to guarantee that everything runs smoothly for a hassle-free event. A wedding at Randholee Luxury Resort will definitely be a memorable and treasured event.</p>

                                <span style="font-size:15px; text-align:center;"><em><strong>"It’s your special day so tell us what you want and how you want it. Leave the rest to us."</strong></em></span><br><br>
                                <ul class="wed-list">
                                    <li>Location you find most suitable on hotel premises (outdoors or indoors)</li>
                                    <li>Overnight stay on full board basis on your wedding day</li>
                                    <li>Additional day room for arrangements and bridal dressing</li>
                                    <li>A basket of fresh fruits in the room </li>
                                    <li>Beautifully frilled table for the cake structure</li>
                                    <li>Antique settee with coffee table</li>
                                    <li>Registration table 5½ ft traditional oil lamp (if required)</li>
                                    <li>A bottle of sparkling wine</li>
                                    <li>Professional and dedicated staff</li>
                                    <li>All legal requirements will be handled before the day of the wedding</li>
                                    <li>Special requirements could be met if required</li>
                                </ul>

                                <div style="clear:both;"></div>

                                <h1 class="hdr-seven" style="text-align:left; font-size:14px; text-transform:none; margin-top: 20px;"><strong>Foreign Wedding package</strong></h1> 
                                <p class="wed-d">Cost – US$ 2000.00</p>  

                                <h1 class="hdr-seven" style="text-align:left; font-size:14px; text-transform:none; margin-top: 20px;"><strong>Stipulations and Conditions</strong></h1>
                                <h1 class="hdr-seven" style="text-align:left; font-size:14px; text-transform:none; margin-top: 20px;"><strong>The package is inclusive of prevailing taxes & service charges</strong></h1>
                                <h1 class="hdr-seven" style="text-align:left; font-size:14px; text-transform:none; margin-top: 20px;"><strong>Validity</strong> - 06 – six months</h1> 

                                <h1 class="hdr-seven" style="text-align:left; font-size:14px; text-transform:none; margin-top: 20px;"><strong>Couple & other invitees should venerate the local culture & law</strong></h1>  
                                <p class="wed-d">Wedding experts at Randholee have the right to organize the wedding ceremony at the location they find most suitable on hotel premises.</p>

                                <h1 class="hdr-seven" style="text-align:left; font-size:14px; text-transform:none; margin-top: 20px;"><strong>Venue</strong> - Subject to change due to the existing weather conditions</h1>

                                <h1 class="hdr-seven" style="text-align:left; font-size:14px; text-transform:none; margin-top: 20px;"><strong>Wedding Package Includes:</strong></h1>
                                <ul class="wed-list">
                                    <li>Pre wedding briefing by a professional event co ordinator and a guest relations officer</li>
                                    <li>Embellished wedding location</li>
                                    <li>Traditional ceremony with drummers and dancers</li>
                                    <li>Master of the ceremonies to conduct the event</li>
                                    <li>Traditional hymns [ psalm ] of blessing by girls</li>
                                    <li>Two tier wedding cake structure</li>
                                    <li>Bouquet & button hole</li>
                                    <li>Traditional oil lamp</li>
                                    <li>Champagne & canapes</li>
                                    <li>Event on a DVD </li>
                                    <li>Photographs in an album with negatives</li>
                                    <li>01 - framed photograph of the couple</li>
                                    <li>Going away by the bullock cart</li>
                                    <li>Marriage certificate [ in English ]</li>
                                    <li>Witnesses & assistance locally if required</li>
                                    <li>Full body massage at the ayurveda center during the stay</li>
                                    <li>Decorated room</li>
                                    <li>Special 05 course candle light dinner by the pool / in the balcony</li>
                                    <li>Fruits, flowers, special honeymoon chocolates and a bottle of wine in the room</li>
                                    <li>Gifts from the hotel</li>
                                </ul>

                                <div style="clear:both;"></div>

                                <table style="width: 95%;" border="0" cellspacing="2" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td class="hdr-seven"><strong>Supplementary</strong></td>
                                            <td class="hdr-seven" align="right"><strong>Cost US$</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Live music during the ceremony [ calypso/ organ / violinist ]</td>
                                            <td align="right">175.00</td>
                                        </tr>
                                        <tr>
                                            <td>Fire works with names of bride &amp; groom</td>
                                            <td align="right">250.00</td>
                                        </tr>
                                        <tr>
                                            <td>Traditional dress for the couple</td>
                                            <td align="right">250.00</td>
                                        </tr>
                                        <tr>
                                            <td>Beautician</td>
                                            <td align="right">150.00</td>
                                        </tr>
                                        <tr>
                                            <td>Traditional wedding jewelery for the bride – hiring</td>
                                            <td align="right">130.00</td>
                                        </tr>
                                        <tr>
                                            <td class="hdr-seven"><strong>Extra meals [ covers ]</strong></td>
                                            <td align="right">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Dinner</td>
                                            <td align="right">20.00</td>
                                        </tr>
                                        <tr>
                                            <td>Lunch</td>
                                            <td align="right">16.00</td>
                                        </tr>
                                    </tbody>
                                </table> 

                                <?php include 'inner_slider.php'; ?> 

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_randolee.php'; ?> 
            </footer>    
    </body>
</html>
