﻿<!DOCTYPE html>

<html class="no-js">
    <?php include '../../includes/header_ellens.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_ellens.php'; ?> 

        </header><!--  #header  -->  

        <?php include '../../includes/booking_ellens.php'; ?> 

        <div class="blur">   

            <div id="node-6" class="node--accommodation_list mode--full">

                <aside role="complementary">
                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/acc/delux_slider1.jpg') no-repeat 50% 50%; background-size: cover;"></div> 
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/acc/suites_slider1.jpg') no-repeat 50% 50%; background-size: cover;"></div>                       
                    </div>

                </aside>  

                <main id="main" role="main">

                    <article role="article" style="background-image:url(assets/img/promo-bg.png); background-repeat:no-repeat;">

                        <div class="ctatext-wrapper">

                            <div class="ctatext-text">         

                                <h1 class="hdr-seven" style="text-align:center; font-size:17px; padding:10px; font-weight:100;">LAST MINUTE DISCOUNT</h1>

                                <div class="hdr-two">-35% Last Minute Discount for Deluxe & Standard rooms-</div>          

                                <!--                                <h1 class="hdr-seven" style="text-align:left; font-size:22px; padding:10px; font-weight:100;"><em>The Firs Nuwara Eliya Early Bird Deal</em></h1>
                                -->                                <p style="text-align:justify; font-size:14px;">Enjoy our last minute offers with amazing discounts for travel in the next four days!</p>
                                <h1 class="hdr-seven hdr-seven-ect">Special Offer Include</h1>
                                <ul class="priv-poly">
                                    <li>35% discount for last minute reservations made within 4 days of arrival on Room Only / Bed & Breakfast  plans</li>
                                </ul>

                                <h1 class="hdr-seven hdr-seven-ect">Special Offer Duration</h1>
                                <ul class="priv-poly">
                                    <li>21st October 2015 to 29th February 2016</li>
                                </ul>

                                <p style="text-align:justify; font-size:14px;">Standard rates starting from USD 100/- Per room per night</p>

                                <h1 class="hdr-seven hdr-seven-ect">CONDITIONS</h1>
                                <ul class="priv-poly">
                                    <li>Rates indicated above include a 10% service charge, & other government taxes.</li>
                                    <li>Rates above are indicative rates per room per night and are subject to change without notice.</li>
                                    <li>This offer is on a non-refundable cancellation policy. If canceled, modified or in case of no-show, the    total price of the reservation will be charged.</li>
                                    <li>Total amount of the reservation will be charged at the time of the reservation.</li>
                                    <li>Bookable period is from  21st October 2015 to 29th February 2016 with the stay period falling 30 days forward, until  29th February 2016.</li>
                                    <li>This promotion cannot be combined with any other promotion or discount and is subject to availability.</li>
                                </ul>

                            </div><!--  .ctatext-text  -->

                        </div><!--  .ctatext-wrapper  -->                          

                    </article>            

                </main>   

            </div><!--  #node-details  -->



            <div style="clear:both"></div>

            <footer id="footer" role="contentinfo">  

                <?php include '../../includes/footer_ellens.php'; ?> 

            </footer>    

    </body>

</html>

