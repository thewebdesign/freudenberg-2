﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_ellens.php'; ?> 

    <body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_ellens.php'; ?> 
        </header><!--  #header  -->
        <?php include '../../includes/booking_ellens.php'; ?> 
        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <?php include '../../includes/_slider_ellens.php'; ?>
                </aside> 

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Location</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Location</div>          
                                <p style="text-align:justify; font-size:16px;">Home to nearly 800,000 inhabitants, Colombo is the commercial capital and the largest city in Sri Lanka. It is an ideal base for both business and leisure travellers and the vibrant and busy city has a lot to offer, including a mixture of modern life and colonial buildings. </p>  

                                <h1 class="hdr-seven" style="text-align:left; font-size:13px;"><strong>Attractions in the vicinity include:</strong></h1>
                                <ul class="accom-list">
                                    <li>The National Museum of Colombo</li>
                                    <li>The Dutch Hospital</li>
                                    <li>The Galle Face Green</li>
                                    <li>The Gangaramaya Temple</li>
                                    <li>Independence Memorial Hall</li>
                                </ul> 
                                <div style="clear:both;"></div> 

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->


            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_ellens.php'; ?> 
            </footer>    
    </body>
</html>
