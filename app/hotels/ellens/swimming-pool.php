﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_ellens.php'; ?> 

    <body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_ellens.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_ellens.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/facilities/pool/pool_slider1.jpg') no-repeat 50% 50%; background-size: cover;"></div>    
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/facilities/pool/pool_slider2.jpg') no-repeat 50% 50%; background-size: cover;"></div>      <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/facilities/pool/pool_slider3.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/facilities/pool/pool_slider4.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    </div>

                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>
                </aside>

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span><a href="facilities.php">Facilities</a></li>
                        <li><span class="arrow"> &gt; </span>Swimming Pool</li>
                    </breadcrumb>
                </div>  

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Swimming Pool</div>          
                                <p style="text-align:justify; font-size:18px;">The swimming pool at Ellen's Place is the ideal place for guests to unwind and laze the hours away. Facing the main restaurant, the pool is located underneath fragrant Araliya trees and provides delightful views of the hotel's perfectly manicured garden. Guests who simply wish to sit back and relax without getting into the water may laze the hours away in one of the swing chairs by the side of the pool.<br><br>
                                    A pool bar is also available next to the pool, offering guests a number of mocktails, juices and snacks to choose from. Simply order a cup of tea, coffee or your favourite snack, and our attentive pool staff will be more than delighted to attend to your request.</p>  

                                <?php include 'inner_slider.php'; ?>

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->
            <?php include 'trip-advisor.php'; ?>
            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_ellens.php'; ?> 
            </footer>    
    </body>
</html>
