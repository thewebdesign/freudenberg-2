﻿<?php 
$pg = ['property' => 'ellens', 'page' => 'accommodation'];
include '../../includes/header_ellens.php';
?>
    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_ellens.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_ellens.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">
                    <?php include '../../includes/slider_ellens.php'; ?>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Accommodation</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">
                                <?php require '../../includes/showdescription.php'; ?>
                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->

                        <div class="highlight-panels">
                            <?php 
                            $mnusec = 'accommodation';
                            $link_style = 'btn-arrow fadeitem';
                            require '../../includes/shownavigation-5.php'; 
                            ?>
                        </div><!--  .highligh-panels  -->

                        <?php include 'trip-advisor.php'; ?>           
                    </article>  
                </main>   
            </div><!--  #node-details  -->

            <?php include '../../includes/footer_ellens.php'; ?> 
    </body>
</html>
