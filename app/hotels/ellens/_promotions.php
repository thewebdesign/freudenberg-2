﻿<!DOCTYPE html>

<html class="no-js">

    <?php include '../../includes/header_ellens.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_ellens.php'; ?>
            <style>
                #route{margin-left: 20.7% !important;}
                @media screen and (max-width: 1366px){
                    #route{margin-left: 9% !important;}
                }
                @media screen and (max-width: 768px){
                    #route{margin-left: -1.3% !important;}
                } 
            </style>

        </header><!--  #header  -->

        <?php include '../../includes/booking_ellens.php'; ?> 

        <div class="blur">  

            <div class="node--page_basic mode--full">  
                <aside role="complementary">
                    <?php include '../../includes/_slider_ellens.php'; ?>
                </aside>    

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Promotions</li>
                    </breadcrumb>
                </div>

                <aside role="complementary">                     
                    <div class="grid">
                        <div class="hdr-two" style="text-align: left; padding: 10px; font-size:1.2em;">ELLEN'S PLACE PROMOTIONS</div>
                        <div style="clear:both"></div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="hdr-two" style="font-style: italic; margin-bottom: 15px;">  No promotions are currently available. </div>
                            <br>
                            <br>
                            <br>
                            <br>
                        <!--<figure class="effect-chico">
                            <img src="assets/images/lastmin_promo.jpg" alt="img10"/>
                            <figcaption>
                                <h2>LAST MINUTE DISCOUNT - <span>DELUXE & STANDARD ROOMS</span></h2>
                                <p>Enjoy our last minute offers with amazing discounts for travel in the next four days!</p>
                                <a href="last-minute.php">View more</a>
                            </figcaption>			
                        </figure>

                        <figure class="effect-chico">
                            <img src="assets/images/longstay_promo.jpg" alt="img10"/>
                            <figcaption>
                                <h2>15% OFF LAST MINUTE DISCOUNT - <span>SUITE ROOMS</span></h2>
                                <p>15% discount for last minute reservations made within 7 days of arrival on Room Only / Bed & Breakfast  plans</p>
                                <a href="#">View more</a>
                            </figcaption>			
                        </figure>

                        <figure class="effect-chico">
                            <img src="assets/images/stay49t_promo.jpg" alt="img10"/>
                            <figcaption>
                                <h2>STAY 4 NIGHTS AND GET THE <span>ONE NIGHT FREE </span></h2>
                                <p>Book 04 consecutive nights at Ellen's place. Receive our best available rate and your fourth night free!</p>
                                <a href="#">View more</a>
                            </figcaption>			
                        </figure>-->
                    </div><!--  .ctacard-wrapper  -->
                </aside>    

            </div>  

            <footer id="footer" role="contentinfo"> 

                <?php include 'trip-advisor.php'; ?>
                <?php /* ?>    <aside role="complementary">
                  <div class="ctatext-buildadventure ctatext-wrapper">
                  <div class="ctatext-text">
                  <h1 class="hdr-seven">Build your own Adventure</h1>
                  <div class="hdr-two">Your Perfect Getaway Awaits</div>
                  <p>Whatever your lifestyle or pace, Freudenberg Leisure offers something unique for everyone.</p>
                  <a class="btn-arrow" href="#">Book Now</a>
                  </div><!--  .ctatext-text  -->
                  </div><!--  .ctatext-wrapper  -->
                  </aside><?php */ ?>

                <?php include '../../includes/footer_ellens.php'; ?>



                </body>

                </html>