﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_ellens.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_ellens.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_ellens.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">
                    <?php include '../../includes/slider_ellens.php'; ?>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>About Us</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">About Us</div>   
                                <p style="text-align:justify; font-size:17px;">Ellen's Place was named after Ellen Senanayake - the youngest child of Mudaliyar Don Charles Gemoris Attygalle and his wife Peternella Abeykoon. Ellen Senanayake contributed much to Ceylon’s independence struggle, being the supportive wife of F. R. Senanayake, one of the post independence national heroes who propelled the Independence movement.<br><br>

                                    Ellen Senanayake, who at one point owned all the houses and land down Shady Grove Avenue in Colombo 8, built a house for her personal physician who was expected to live there and treat her and other members of her family. Years later, the house was inherited by her great grandson Hon. Vasantha Senanayake, a young and dynamic parliamentarian who decided to convert the beautiful house to a modern boutique hotel, yet retaining some features of its original decor. <br><br>

                                    He decided to name the property “Ellen’s Place” in memory of his great grandmother, an ardent Buddhist, generous Philanthropist, astute businesswoman and a devoted mother to her six children..</p>


                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->


            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_firs.php'; ?> 
            </footer>    
    </body>
</html>
