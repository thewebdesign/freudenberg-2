﻿<?php 
$pg = ['property' => 'ellens', 'page' => 'facilities'];
include '../../includes/header_ellens.php';
?>
    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_ellens.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_ellens.php'; ?> 

        <div class="node--page_basic mode--full">
            <aside role="complementary">
                <?php include '../../includes/slider_ellens.php'; ?>          
            </aside> 

            <div id="route">
                <breadcrumb class="menu">
                    <li><a href="index.php">Home</a></li>
                    <li><span class="arrow"> &gt; </span>Facilities</li>
                </breadcrumb>
            </div>

            <div id="main" role="main">     
                <article role="article" style="padding-top:10px;">
                    <div class="ctatext-wrapper">
                        <div class="ctatext-text">
                            <?php require '../../includes/showdescription.php'; ?>
                        </div><!--  .ctatext-wrapper  -->
                    </div><!--  .ctatext-text  -->

                    <div class="highlight-panels">
                        <?php 
                        $mnusec = 'facilities';
                        require '../../includes/shownavigation-5.php'; 
                        ?>
                    </div><!--  .highligh-panels  -->    
                </article>
            </div><!--  #main  -->

        </div><!--  #node-details  -->

        <footer id="footer" role="contentinfo">  

            <?php include 'trip-advisor.php'; ?> 
            <?php include '../../includes/footer_ellens.php'; ?> 

    </body>
</html>
