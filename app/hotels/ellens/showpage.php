<?php 
$pg = ['property' => 'ellens', 'page' => $_GET['pg']];
include '../../includes/header_ellens.php';
?>
<body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-accommodation-list">
<header id="header" role="banner">
  <?php include '../../includes/navigation_ellens.php';?> 
  </header><!--  #header  -->
  
<?php include '../../includes/booking_ellens.php';?> 

<div class="blur">  
  
  <div id="node-6" class="node--accommodation_list mode--full">
  <?php if($cms->hasSlider()): ?>
    <aside role="complementary">
    <?php include '../../includes/slider_ellens.php'; ?>
    </aside>    
  <?php endif; ?>
  
  <div id="route">
        <breadcrumb class="menu">
            <li><a href="index.php">Home</a></li>
            <li><span class="arrow"> &gt; </span>About Us</li>
        </breadcrumb>
  </div>  

  <main id="main" role="main">
    <article role="article">
      <div class="ctatext-wrapper">
        <div class="ctatext-text">         
          <?php require '../../includes/showdescription.php'; ?>
          
          <?php if(!isset($galleryincluded) && $cms->hasGallery()): ?>
            <div style="clear:both"></div>
            <?php include '../../includes/inner_page_gallery.php'; ?> 
          <?php endif; ?>
         </div><!--  .ctatext-text  -->
      </div><!--  .ctatext-wrapper  -->  

      <?php if($cms->hasNav()): ?>
      <div class="highlight-panels">
          <?php require '../../includes/shownavigation-1.php'; ?>
      </div><!--  .highligh-panels  -->
      <?php endif; ?>

    </article>      
          
  </main>   
</div><!--  #node-details  -->
  
    <div style="clear:both"></div>
  <footer id="footer" role="contentinfo">  
  <?php include '../../includes/footer_ellens.php';?> 
   </footer>    
</body>
</html>