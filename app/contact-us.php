<?php
$pg = ['property' => 'freudenberg', 'page' => 'contactus'];
require_once 'top.php';
require_once('includes/contact-us-process.php');
require_once('includes/header.php'); 
?> 

    <body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include 'includes/navigation.php'; ?> 
        </header><!--  #header  -->

        <div class="blur">  
            <div id="node-78" class="node--webform mode--full"> 

                <div id="main" role="main">
                    <div class="wrapper">
                        <article role="article">  
                            <div class="ddd" style="position:relative; top:90px; float:right; width: 45%;"><img src="assets/images/cont-us.jpg" style="border: 8px solid #5699B0; border-radius: 10px;"></div>
                            <div style="position:relative; top:100px;">
                                <?php if (isset($hasError)) { ?>
                                    <div class="form-item webform-component webform-component-errors" id="error_msg">
                                        
                                        <ul>
                                             <?php foreach($errors as $error): ?>

                                                <li><?php echo $error ?></li>

                                            <?php endforeach ?>
                                        </ul>
                                    </div>
                                <?php } if (isset($success_msg)) { ?>
                                    <div class="form-item webform-component webform-component-success" id="success_msg">
                                        <label><?php echo $success_msg; ?></label>
                                    </div>
                                <?php } ?>
                                <form class="webform-client-form webform-client-form-78" enctype="multipart/form-data" action="" method="post" id="webform-client-form-78" accept-charset="UTF-8">

                                <input type="hidden" name="form_token" value="<?php echo $csrf_token ?>">
                                
                                    <div>
                                        <div class="form-item webform-component webform-component-textfield webform-component--name">
                                            <label for="edit-submitted-name">Subject </label>
                                            <input type="text" id="edit-submitted-name" name="subject" value="<?php echo $subject; ?>" size="60" maxlength="128" class="form-text" required>
                                        </div>
                                        <div class="form-item webform-component webform-component-textfield webform-component--name">
                                            <label for="edit-submitted-name">Name </label>
                                            <input type="text" id="edit-submitted-name" name="name" value="<?php echo $name; ?>" size="60" maxlength="128" class="form-text" required>
                                        </div>
                                        <div class="form-item webform-component webform-component-email webform-component--email">
                                            <label for="edit-submitted-email">Email Address </label>
                                            <input class="form-text" type="email" id="edit-submitted-email" name="email" value="<?php echo $email; ?>" size="60" required>
                                        </div>
                                        <div class="form-item webform-component webform-component-textfield webform-component--email">
                                            <label for="edit-submitted-email">Contact Number </label>
                                            <input class="form-text" type="text" id="edit-submitted-email" name="phone" value="<?php echo $phone; ?>" size="60" required>
                                        </div>

                                        <div class="form-item webform-component webform-component-textfield webform-component--name">

                                            <label for="edit-submitted-name">Hotel </label>

                                            <select name="hotel" id="Contact_hotel" class="form-text" required>

                                                <option value="">Select a Hotel</option>
                                                <option value="randholee">Randholee Resorts</option>
                                                <option value="firs">The Firs</option>
                                                <option value="ellens">Ellen's Place</option>
                                            </select>

                                        </div>

                                        <div class="form-item webform-component webform-component-textfield webform-component--email">
                                            <label for="edit-submitted-email">Country</label>
                                            <select name="country" id="countat_country" class="form-text" required>
                                                <option value="">Select Your Country</option>
                                                <?php
                                                foreach ($countries as $country) {
                                                    echo '<option value="' . $country . '"';
                                                    if ($user_country == $country) {
                                                        echo 'selected>' . $country . '</option>';
                                                    } else {
                                                        echo '>' . $country . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-item webform-component webform-component-textarea webform-component--comment---question">
                                            <label for="edit-submitted-comment-question">Message </label>
                                            <div class="form-textarea-wrapper resizable">
                                                <textarea id="edit-submitted-comment-question" name="comments" cols="60" rows="5" class="form-textarea" required><?php echo $comments; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
                                        <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=en"></script>
                                        <div class="form-actions">
                                            <input class="webform-submit button-primary form-submit" type="submit" name="contact_submit" value="Submit">
                                        </div>
                                    </div>
                                </form>
                            </div>      
                        </article>      

                        <?php
                            $fr = $cms->getPropertyDetails(1);
                        ?>
                        <ul class="add_freud">
                            <li><strong><?=$fr->property_name?></strong></li>
                            <?php 
                                $addressparts = explode(',', $fr->property_address);

                                foreach($addressparts as $part):
                            ?>
                                <li><?=$part?></li>
                            <?php endforeach; ?>
                            <br>
                            <?php if(!empty($fr->property_telephone)): ?>
                            <li>Tel.: <?=$fr->property_telephone?></li>
                            <?php endif; ?>
                            <?php if(!empty($fr->property_fax)): ?>
                            <li>Fax: <?=$fr->property_fax?></li>
                            <?php endif; ?>
                            <?php if(!empty($fr->property_email)): ?>
                            <li>E-mail: <?=$fr->property_email?></li>    
                            <?php endif; ?>
                        </ul>

                        <?php
                            $firs = $cms->getPropertyDetails(3);
                        ?>                        
                        <ul class="add_firs">
                            <li><strong><?=$firs->property_name?></strong></li>
                            <?php 
                                $addressparts = explode(',', $firs->property_address);

                                foreach($addressparts as $part):
                            ?>
                                <li><?=$part?></li>
                            <?php endforeach; ?>
                            <br>
                            <?php if(!empty($firs->property_telephone)): ?>
                            <li>Tel.: <?=$firs->property_telephone?></li>
                            <?php endif; ?>
                            <?php if(!empty($firs->property_fax)): ?>
                            <li>Fax: <?=$firs->property_fax?></li>
                            <?php endif; ?>
                            <?php if(!empty($firs->property_email)): ?>
                            <li>E-mail: <?=$firs->property_email?></li>    
                            <?php endif; ?>
                        </ul> 
                        <ul class="add_ellens">
                            <li><strong>Ellen's Place</strong></li>
                            <li>31,</li>
                            <li>Shady grove avenue,</li>
                            <li>Colombo 08,</li>
                            <li>Sri Lanka.</li><br>
                            <li>Tel.: +94 112 680 062</li>
                            <li>Fax: +94 112 860 063</li>
                            <li>E-mail:   info@ellensplace.lk</li>
                        </ul>

                        <?php
                            $rand = $cms->getPropertyDetails(2);
                        ?>                        
                        <ul class="add_rand">
                            <li><strong><?=$rand->property_name?></strong></li>
                            <?php 
                                $addressparts = explode(',', $rand->property_address);

                                foreach($addressparts as $part):
                            ?>
                                <li><?=$part?></li>
                            <?php endforeach; ?>
                            <br>
                            <?php if(!empty($rand->property_telephone)): ?>
                            <li>Tel.: <?=$rand->property_telephone?></li>
                            <?php endif; ?>
                            <?php if(!empty($rand->property_fax)): ?>
                            <li>Fax: <?=$rand->property_fax?></li>
                            <?php endif; ?>
                            <?php if(!empty($rand->property_email)): ?>
                            <li>E-mail: <?=$rand->property_email?></li>    
                            <?php endif; ?>
                        </ul>              
                        <div style="clear:both"></div> 


                        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqHHPTaSlfbdTnuv8kO4HrOw9_BpaehUg"></script>
                        <script>
                            var myCenter = new google.maps.LatLng(6.912712, 79.852655);

                            function initialize()
                            {
                                var mapProp = {
                                    center: myCenter,
                                    zoom: 18,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP
                                };

                                var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

                                var marker = new google.maps.Marker({
                                    position: myCenter,
                                });

                                marker.setMap(map);
                            }

                            google.maps.event.addDomListener(window, 'load', initialize);
                        </script>

                        <div id="googleMap"></div>
                    </div><!--  .wrapper  -->

                </div><!--  #main  -->

            </div><!--  #node-details  -->

            <footer id="footer" role="contentinfo">  
                <?php include 'trip-advisor.php'; ?> 

                <?php include 'includes/footer.php'; ?>

                </body>
                </html>
