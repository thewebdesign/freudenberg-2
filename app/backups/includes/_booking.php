<?php
if (isset($_POST['submit_fsaleisure']) && $_POST['site'] === 'fsaleisure') {
    $hotel_name = filter_var($_POST['hotels'], FILTER_SANITIZE_STRING);

    $arrival_date = filter_var($_POST['arrival_date'], FILTER_SANITIZE_STRING);
    list($arrival_year, $arrival_month, $arrival_day) = explode('-', $arrival_date);

    $departure_date = filter_var($_POST['departure_date'], FILTER_SANITIZE_STRING);
    list($departure_year, $departure_month, $departure_day) = explode('-', $departure_date);

    if (isset($hotel_name) && $hotel_name !== '-1'):
        if ($hotel_name === 'randholee'):
            $submit_URL = 'http://www.booking.com/hotel/lk/randholee-luxury-resort-kandy.en-gb.html?';
        elseif ($hotel_name === 'firs'):
            $submit_URL = 'http://www.booking.com/hotel/lk/the-firs.en-gb.html?';
        elseif ($hotel_name === 'ellens'):
            $submit_URL = 'http://www.booking.com/hotel/lk/ellen-39-s-place.en-gb.html?';
        endif;
    endif;

    $parameters = 'aid=330843&checkin_monthday=' . $arrival_day . '&checkin_year_month=' . $arrival_year . '-' . $arrival_month . '&checkout_monthday=' . $departure_day . '&checkout_year_month=' . $departure_year . '-' . $departure_month . '&dist=0&sb_price_type=total&type=total&';

    echo "<script data-cfasync='false' > window.location.replace('" . $submit_URL . $parameters . "') </script>";
    unset($_POST);
    unset($submit_URL);
    unset($parameters);
    exit;
}
?>

<div class="online_reservation">
    <div class="b_room">
        <div class="booking_room">
            <div class="reservation">
                <ul>
                    <h5>MAKE A RESERVATION&nbsp;<a id="booking_widget_open_close" class=""><i id="close" class="fa fa-times"></i></a></h5>
                    <div class="toggler">
                        <div id="effect" class="ui-widget-content ui-corner-all">
                            <form id="central_booking" action="" method="POST">
                                <li class="span1_of_1">
                                    <!--start section_room-->
                                    <div class="section_room">
                                        <select id="hotels" name="hotels" class="frm-field required">
                                            <option value="-1">Select a Hotel</option>
                                            <option value="randholee">Randholee Resort &#38; Spa</option>
                                            <option value="firs">The Firs</option>
                                            <option value="ellens">Ellen's Place</option>
                                        </select>
                                    </div>
                                </li>

                                <li  class="span1_of_1 left">
                                    <div class="book_date">
                                        <input class="date" id="arrival_date" name="arrival_date" type="text" value="CHECK IN" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                    this.value = '<?php echo date("Y-m-d") ?>';
                                                }">
                                    </div>
                                </li>
                                <li  class="span1_of_1 right">
                                    <div class="book_date">
                                        <input class="date" id="departure_date" name="departure_date" type="text" value="CHECK OUT" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                    this.value = '<?php echo date("Y-m-d", strtotime("tomorrow")) ?>';
                                                }">
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <input type="hidden" name="site" value="fsaleisure" />
                                        <input type="submit" name="submit_fsaleisure" value="CHECK AVAILABILITY" />
                                    </div>
                                </li>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </ul>

            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>