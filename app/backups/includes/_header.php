<?php require_once 'top.php'; ?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta content='width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0' name='viewport'>
    <link rel="shortcut icon" href="assets/img/favicon.ico">

    <link type="text/css" rel="stylesheet" href="assets/css/fonts.css">
    <link type="text/css" rel="stylesheet" href="assets/css/styles.css">
    <link type="text/css" rel="stylesheet" href="assets/css/main.css">
    <link type="text/css" rel="stylesheet" href="assets/css/responsive.css">
    <link type="text/css" rel="stylesheet" href="assets/css/accordian.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.css" />
    <link rel="stylesheet" href="assets/css/smoothslides.theme.css">


    <?php include 'meta.php'; ?>

</head>