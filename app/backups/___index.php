﻿<!DOCTYPE html> 
<html lang="en">  
    <?php include 'includes/header.php'; ?>  
    <body class="node-type-accommodation-list">

        <header id="header" >
            <?php include 'includes/navigation.php'; ?> 

        </header><!--  #header  -->
        <?php include 'includes/booking.php'; ?> 


        <div class="blur">  
            <div class="node--page_basic mode--full">    
                <aside>
                    <?php include 'includes/slider.php'; ?>
                </aside>  

                <div id="main" role="main">
                    <div class="wrapper">
                        <article>
                            <div class="ctatext-wrapper">
                                <div class="ctatext-text pad_top">
                                    <h1 class="hdr-seven">Freudenberg Leisure</h1>
                                    <div class="hdr-two">Upscale small hotel experiences</div>     
                                    <p style="text-align:justify; font-size:17px;">
                                        Freudenberg Leisure, with a chain of small upscale hotels in Colombo, the capital of Sri Lanka, Kandy, the UNESCO nominated world heritage city and Nuwara Eliya, the ‘city on the plain’ or ‘city of light’ 
                                        offers one-of-a-kind experiences. Come to us with your wish list, our hotels will fulfill all your needs.
                                    </p>
                                    <p style="text-align:justify; font-size:17px;">
                                        The Randholee Resort and spa is an ideal choice for a fun family holiday, honeymoon getaways or business retreat for discerning modern travelers.
                                        Fascinating, unruffled and peaceful, the Firs Nuwara Eliya offers the travelers, serenity and indulgence for a relaxing holiday.
                                        Ellen’s Place, named after Ellen Senanayake, a leader in Sri Lanka’s independence struggle, is a upscale hotel in Colombo with unique and delightful facilities of a home away from home.
                                    </p>
                                    <!--<div class="double-button">
                                      <a class="btn-underline" href="#">The Resort</a>
                                      <a class="btn-underline" href="#">Getting Here</a>
                                    </div>--><!--  .double-button  -->
                                </div><!--  .ctatext-text  -->          
                            </div><!--  .ctatext-wrapper  -->

                        </article>
                    </div><!--  .wrapper  -->
                </div><!--  #main  -->

                <aside>
                    <header>
                        <h1 class="hide-visual">Freudenberg Leisure Experiences</h1>
                    </header>  
                    <div class="experience-thumblist highlight-panels">
                        <ul>  
                            <div class="experience-thumblist highlight-panels">
                                <ul>          
                                    <li class="highlight" style="background: #ebebeb url('assets/images/romancee.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="hotels/randholee/honeymoon.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">       
                                                <h2 class="hdr-four">Romance &amp; Relaxation</h2>    
                                                <div class="hdr-two fadeitem"><em>Romance, Privacy and Opulence</em></div>      
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">Read More</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 


                                    <li class="highlight" style="background: #ebebeb url('assets/images/generations.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="hotels/randholee/promotions.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">       
                                                <h2 class="hdr-four">Randholee Promotions</h2>    
                                                <div class="hdr-two fadeitem"><em>Exciting Deals & Offers</em></div>      
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">Read More</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 


                                    <li class="highlight" style="background: #ebebeb url('assets/images/firs_promo.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="hotels/firs/promotions.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">       
                                                <h2 class="hdr-four">Firs Promotions</h2>    
                                                <div class="hdr-two fadeitem"><em>Great Savings and Promotions</em></div>      
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">Read More</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 


                                    <li class="highlight" style="background: #ebebeb url('assets/images/adventure.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="experience.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">       
                                                <h2 class="hdr-four">Experiences</h2>    
                                                <div class="hdr-two fadeitem"><em>Explore the pearl of the Indian Ocean</em></div>      
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">Read More</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 


                                    <li class="highlight" style="background: #ebebeb url('assets/images/ellens_promo.jpg') no-repeat 50% 50%; background-size: cover;">
                                        <a class="highlight-hotspot" href="hotels/ellens/promotions.php"></a>
                                        <div class="highlight-background"></div><!--  .highlight-background  -->
                                        <div class="highlight-content">
                                            <div class="highlight-content-inside">       
                                                <h2 class="hdr-four">Ellen's Promotions</h2>    
                                                <div class="hdr-two fadeitem"><em>Amazing discounts & Promotions</em></div>      
                                                <a class="btn-underline btn-underline--big-white fadeitem" href="#">Read More</a>
                                            </div><!--  .highlight-content-inside  -->
                                        </div><!--  .highlight-content  -->
                                    </li><!--  .highlight  --> 

                                </ul>
                            </div><!--  .experience-thumblist .highlight-panels  --> 
                        </ul>
                    </div><!--  .experience-thumblist .highlight-panels  -->
                </aside>  


                <aside>
                    <h1 class="hide-visual">Freudenberg Leisure Offerings</h1>
                    <div class="ctacard-wrapper">
                        <div class="ctacard-card equal-height">
                            <div class="inner-ratio">
                                <img class="img-responsive" src="assets/images/randholee/view.jpg" alt="Randholee">
                            </div><!--  .inner-ratio  -->
                            <div class="ctacard-text">    
                                <h2 class="hdr-four">Randholee Resort and Spa</h2>     
                                <p>A upscale hotel built atop a misty and magical mountain is surrounded by a panoramic view of the villages below. Visit this resort for a lovely overview of the beautiful city of Kandy if you are planning a romantic vacation or a honeymoon getaway to Sri Lanka.</p>    
                            </div><!--  .ctacard-text  -->    
                            <div class="ctacard-button">
                                <a href="hotels/randholee/index.php" class="btn-arrow">Find Out More</a>  </div><!--  .ctacard-button  -->      
                        </div><!--  .ctacard-card  -->


                        <div class="ctacard-card equal-height">
                            <div class="inner-ratio">
                                <img class="img-responsive" src="assets/images/ellens/sl1.jpg" alt="Ellen's Place">
                            </div><!--  .inner-ratio  -->    
                            <div class="ctacard-text">    
                                <h2 class="hdr-four">Ellen's Place</h2>    
                                <p>Set against an urban landscape of concrete, steel and glass, Ellens’ Place stands out with a unique silhouette and a lush green garden. Experience an aura of living, breathing lushness like no other at, Ellens’, your tropical resort in the capital city of Colombo.</p>    
                            </div><!--  .ctacard-text  -->    
                            <div class="ctacard-button">
                                <a href="hotels/ellens/index.php" class="btn-arrow">Find Out More</a>  </div><!--  .ctacard-button  -->      
                        </div><!--  .ctacard-card  -->


                        <div class="ctacard-card equal-height">
                            <div class="inner-ratio">
                                <img class="img-responsive" src="assets/images/firs.jpg" alt="The Firs">
                            </div><!--  .inner-ratio  -->    
                            <div class="ctacard-text">    
                                <h2 class="hdr-four">The Firs</h2>   
                                <p>Elegant and cozy, this exquisite heritage bungalow that hearkens back to the British colonial era in Nuwara Eliya refreshingly different. A combination of old world charm and new world hospitality brings you the best in world class hospitality.</p>
                            </div><!--  .ctacard-text  -->    
                            <div class="ctacard-button">
                                <a href="hotels/firs/index.php" class="btn-arrow">Find Out More</a>  </div><!--  .ctacard-button  -->      
                        </div><!--  .ctacard-card  -->    </div><!--  .ctacard-wrapper  -->

                </aside> 
            </div><!--  #node-details  -->  

            <?php include 'map_index.php'; ?>

            <footer id="footer">    

                <div style="clear:both"></div>
                <?php include 'trip-advisor.php'; ?> 
                <div style="clear:both"></div>
                <?php include 'includes/footer.php'; ?>

                </body>
                </html>