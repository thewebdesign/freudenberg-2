<div id="map_canvas" style="width:100%; height:500px;"></div> 

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
<script type="text/javascript">

    var map;
    var markers = [];

    initialize();

    function initialize() {
        var myOptions = {
            zoom: 10,
            scrollwheel: false,
            center: new google.maps.LatLng(7.155538, 80.367844),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        addMarker(new google.maps.LatLng(7.263146, 80.616914), "Randholee Luxury Resort, Kandy", "assets/img/map_pin_randholee.png");
        addMarker(new google.maps.LatLng(6.963518, 80.781524), "The Firs, Nuwara Eliya", "assets/img/map_pin_firs.png");
        addMarker(new google.maps.LatLng(6.911567, 79.881787), "Ellen's Place, Colombo", "assets/img/map_pin_ellens.png");

    }

    function addMarker(latlng, myTitle, myIcon) {
        markers.push(new google.maps.Marker({
            position: latlng,
            map: map,
            title: myTitle,
            icon: myIcon
        }));
    }

</script> 
