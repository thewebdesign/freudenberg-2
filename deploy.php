<?php
namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'freudenberg');

// Project repository
set('repository', 'git@bitbucket.org:thewebdesign/freudenberg-2.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);

set('shared_dirs', ['vendor']);


// Writable dirs by web server
set('writable_dirs', []);
set('allow_anonymous_stats', false);
set('ssh_multiplexing', true);


// Hosts
host('web-server-a')
    ->stage('production')
    ->configFile('~/.ssh/config')
    ->set('keep_releases', 2)
    ->identityFile('~/.ssh/id_rsa')
    ->forwardAgent(false)
    ->multiplexing(true)
    ->set('deploy_path', '~/source')
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no');


task('create_pub_symlink', function(){
    
    if (!has('previous_release')) {
        run('rm -rf ~/public_html');
        run('ln -s ~/source/current/app/ ~/public_html');
        writeln(" created symlink for public_html ");
    }

});

task('set_known_hosts', function(){
    
    if (!has('previous_release')) {
        run('ssh -o StrictHostKeyChecking=no git@bitbucket.org');
        writeln("added to known hosts");
    }
});

task('prod_config', function(){
    
    run('mv {{release_path}}/live-config/lve-config.php {{release_path}}/config/config.php');
    run('mv {{release_path}}/live-config/lve.htaccess {{release_path}}/app/.htaccess');
    writeln(" moved production configurations ");

});

task('set_php_alias', function(){
    
    if (!has('previous_release')) {
        run('echo -e "\nalias php=\'/usr/local/php71/bin/php\'" >> ~/.bash_profile');
        writeln(" set alias to cli php 7.1");
    }
});


desc('Deploy your project');
task(
    'deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:copy_dirs',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
    ]
);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

# after('deploy:update_code', 'prod_config');

before('deploy:unlock', 'create_pub_symlink');

before('deploy:vendors', 'set_php_alias');

before('deploy:update_code', 'set_known_hosts');
